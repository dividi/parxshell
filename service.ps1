. $PSScriptRoot\libs.ps1
. $PSScriptRoot\config.ps1
. $PSScriptRoot\class-service.ps1

try
{
	Log2 "[SERVICE] Started" "info"
	# Enable or disable Parxshell (client)
	$parx_launch = Set-ParxLaunch
	# Security audit
	Set-Audit
	
	# Create Parx Log 
	Set-ParxLog
	
	# Create Parx Folder in scheduled task
	Set-ParxFolder
	
	# Listen to security events
	Register-WinEvents -eventid 7001 -logname "System" -SourceIdentifier "Logon"
	Register-WinEvents -eventid 7002 -logname "System" -SourceIdentifier "Logoff"
	Register-WinEvents -eventid 4800 -logname "Security" -SourceIdentifier "Lock"
	Register-WinEvents -eventid 4801 -logname "Security" -SourceIdentifier "Unlock"
	Register-WinEvents -eventid 4778 -logname "Security" -SourceIdentifier "Switch"
	Register-WinEvents -eventid 4719 -logname "Security" -SourceIdentifier "Audit_change"
	Register-WinEvents -eventId 9999 -logname "ParxShell" -SourceIdentifier "Gui"
	
	# Check if Parxshell was already initiliazed, if not then it's a fresh install
	$fresh_install = Check-FreshInstall
	
	# Api token
	$global:token = Get-Token
	
	# Actual server ip found from config.ps1 (will be used to check against new server ip if one is found)
	$actual_server_ip = $address
	
	# Variable that will be used when checking for user task; when a user is logged in
	$global:is_logged = $false
	
	# Disable welcome screen for first logon
	Disable-Welcome
	
	
	
	# If service was just installed, check if any user is logged so that parx can be launched in its session id
	if ($fresh_install -eq $true)
	{
		Launch-Parxshell
	}
	
	# try to contact server with a ClearPass remediation
	$global:testserver = test-server
	$retryInterval = 10000 # Retry interval in milliseconds (e.g., 10000 = 10 seconds)
	$maxAttempts = 3 # Maximum number of retries	
	$retryCount = 0
	$attempt = 0
	while ($global:testserver -eq $null -and $retryCount -lt $maxAttempts)
	{
		$attempt++
		log2 "[SERVICE] !! GURU MEDITATION !! Problem while connecting with Parx server on $actual_server_ip, retrying : $attempt/$maxAttempts  " "warn"
		Start-Sleep -Milliseconds $retryInterval
		$global:testserver = test-server
		$retryCount++
	}
	
	if ($testserver -eq $null)
	{
		log2 "[SERVICE] !! GURU MEDITATION !! Server is not reachable on $actual_server_ip" "error"
		# Check wether host is a tablette or not and get a new Numser if available
		Check-Numser
		
	}
	else
	{
		log2 "[SERVICE] Connected on Parx server on : $actual_server_ip." "success"
		# Get and check for newer config file from server for host
		$conf, $conf_restart = Get-Config $actual_server_ip
		
		# Update variables from config.ps1 if new file is applied
		if ($conf -eq $true)
		{
			# Restart Parxshell service if server address (that was validated) changed
			if ($conf_restart -eq $true)
			{
				# We must invoke a failure in order to restart the service (from nssm). Other cmd (such as Restart-Service or sc stop&&sc start) would fail to start Parxshell service
				# after service stops
				log2 "[SERVICE] Parxshell server ip changed, restarting service..." "info"
				[System.Environment]::FailFast("Parxshell server ip changed, restarting service...")
			}
			# If server address wasn't changed, update only config.ps1 variables
			else
			{
				. $PSScriptRoot\config.ps1
			}
		}
		
		
		# Register host and update info to server
		$hostid, $hostOS = Get-HostInfo
		Register-Host $hostid $hostOs
		
		# Acl security for parxshell folder
		Check-ParxAcl
		
		# Launch machine task
		$service_restart = Run-Task
		# If a client update was found as a task and successfully applied, restart service
		if ($service_restart -eq $true)
		{
			# We must invoke a failure in order to restart the service (from nssm). Other cmd (such as Restart-Service or sc stop&&sc start) would fail to start Parxshell service
			# after service stops
			log2 "[UPDATE] Parxshell update completed, restarting service..." "info"
			[System.Environment]::FailFast("Parxshell update completed, restarting service...")
		}
		
		
		# If service refresh is set, listen for tasks to apply from the server
		if ($service_refresh -gt 4)
		{
			Set-ServiceRefresh -ServiceRefresh $service_refresh -SourceIdentifier "Check.Tasks"
			log2 "[SERVICE TEMP] Checking for new task"
			Register-EngineEvent -SourceIdentifier "Check.Tasks" -Action {
				$service_restart = Run-Task
				# If a client update was found as a task and successfully applied, restart service
				if ($service_restart -eq $true)
				{
					# We must invoke a failure so that nssm service restarts itself. Other cmd (such as Restart-Service or sc stop&&sc start) would fail to start Parxshell service
					# after service stops
					log2 "[UPDATE] Parxshell update completed, restarting service..." "info"
					[System.Environment]::FailFast("Parxshell update completed, restarting service...")
				}
			}
			# Wait for the events
			while ($true)
			{
				$event = Wait-Event -SourceIdentifier "Check.Tasks"
				$event | Remove-Event
			}
			
		}
		
		
		
		
	}
}
catch
{
	Log2 "SERVICE ERROR : $($_.Exception.Message) on line $($_.InvocationInfo.ScriptLineNumber) : $($_.InvocationInfo.Line)"
}


