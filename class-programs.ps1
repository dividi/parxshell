﻿# get the default programs list from the db
Function get-programs-list ($profileid)
{
	
	return restGET "$address/rest_file_assoc/$profileid"
}

# This function will be injected as encoded command to be run as the current user
# Remove Parx program assocations
Function Delete-Programs
{
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction stop
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
	
	function Set-Browser ($browsername, $first_logon)
	{
		log2 "[ASSOCIATIONS] Setting default browser to $browsername" "info"
		
		# Define an empty array to hold the browser objects
		$browsers = @()
		
		# Get the output of the SetDefaultBrowser command, that will list all installed browsers and their corresponding hives
		$output = C:\parxshell\bin\SetDefaultBrowser.exe
		
		# Now we have to parse this output into ps objects
		# Find the line number where the browser entries start
		$index = $output.IndexOf("Installed browsers:") + 1
		
		# Loop through each line in the output starting from the index
		$output[$index .. ($output.Length - 1)] | ForEach-Object {
			
			# Check if the line starts with "HKLM" or "HKCU"
			if ($_ -match "^HK(LM|CU)\s+(.+)$")
			{
				# Read the hive
				$hive = $output[$index]
				
				# Read the next line to get the name
				$name = $output[$index + 1]
				
				# Read the next line to get the path
				$path = $output[$index + 2]
				
				# Use regex to extract the path value from the line
				$pathPattern = "path:\s*(.+)"
				if ($path -match $pathPattern)
				{
					$path = $matches[1]
				}
				else
				{
					$path = ""
				}
				
				# Create a new browser object and add it to the array
				$browser = [PSCustomObject]@{
					Hive = $hive
					Name = $name.Replace("name:", "").Trim()
					Path = $path.Replace('"', "")
				}
				$browsers += $browser
			}
			
			# Increment the line index
			$index++
		}
		# Get the hive part of the powershell object corresponding to the defined default browser from server
		$defaultbrowser = ($browsers | Where-Object { $_.Name -like "*$browsername*" })[0].Hive
		
		# check if the browser hive name contains a space, as SetDefaultBrowser needs some "" if so
		if ($defaultbrowser.Substring(5).Contains(" "))
		{
			$hive = "$($defaultbrowser.Split(" ")[0]) `"$($defaultbrowser.Substring(5))`""
		}
		else
		{
			$hive = $defaultbrowser
		}
		
		# Set the default browser
		$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetDefaultBrowser.exe" -ArgumentList $hive
		# Set the default browser
		log2 "[ASSOCIATIONS] Default browser set to $browsername with Hive $hive" "info"
		
	}
	
	function Set-DefaultAssociations($default)
	{
		
		@($default.Audio, $default.Video, $default.Image, $default.PDF, $default.Office) | out-File "c:\users\$env:username\appdata\Roaming\Parx\default_assocs.txt" -Encoding utf8
		$default_file = "c:\users\$env:username\appdata\Roaming\Parx\default_assocs.txt"
		$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList "`"$default_file`"" -ErrorAction SilentlyContinue
		
		switch -Wildcard ($default.Browser)
		{
			"*firefox*" {
				Set-Browser "Firefox"
			}
			"*chrome*" {
				Set-Browser "Chrome"
			}
			"*edge*" {
				Set-Browser "Edge"
			}
			
		}
		
	}
	
	$debugmode = "DEBUG"
	$eventid = "EVENTID"
	$debug_level = "LEVEL"
	$first_logon = "FIRSTLOGON"
	
	try
	{
		$default = Get-Content "c:\users\$env:username\appdata\Roaming\Parx\cache.json" | ConvertFrom-Json		
		Set-DefaultAssociations $default		
	}
	catch
	{
		log2 "[ASSOCIATIONS] Error while re-setting defaults one : $($_.Exception.Message)"
	}
	finally
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Associations Done" -Category 1 -RawData 10, 20
		Remove-Item -Path "C:\Users\$env:username\appdata\roaming\parx" -Recurse -Force		
		exit		
	}
	
}

# This function will be injected as encoded command to be run as the current user
# Set default programs that are found in profile
Function Set-Programs
{
	# We must declare again the log2 function as it can't be reached from user session
	# Write to the log file
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction stop
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
	
	Function Set-Cache
	{
		# Set the cache for default programs rollback, ie the one that was set before Parx was launched for the first time on the user session
		$cache = @{ }
		# Browsers
		$regvalue = (Get-ItemProperty HKCU:\Software\Microsoft\windows\Shell\Associations\UrlAssociations\http\UserChoice).Progid
		# Search for the matched browser
		$cache['Browser'] = $regvalue
		
		# Grab all extensions assocations
		$associations = & "C:\parxshell\Bin\setuserfta.exe" get
		
		# PDF
		$pdf = $associations | Where-Object { $_ -like "*.pdf*" }
		$cache['PDF'] = $pdf
		
		# Audio
		$audio_extensions = @(
			".aac", ".adts", ".aif", ".aifc", ".aiff", ".amr", ".au", ".cda", ".flac", ".m3u",
			".m4a", ".m4p", ".mid", ".midi", ".mk3d", ".mka", ".MP2", ".mp3", ".ra", ".ram",
			".rmi", ".s3m", ".snd", ".voc", ".wav", ".wax", ".wm", ".wma", ".wmx", ".WPL", ".xm", ".zpl"
		)
		$audio = $associations | Where-Object { $_.Split(',')[0] -in $audio_extensions }
		$cache['Audio'] = $audio
		
		# Video
		$video_extensions = @(
			".3g2", ".3gp", ".3gp2", ".3gpp", ".asf", ".avi", ".m2t", ".m2ts", ".m4v",
			".mkv", ".mov", ".mp4", ".mp4v", ".mts", ".wm", ".wmv", ".amr", ".asx", ".cda", ".m1v", ".mp2v",
			".mpa", ".MPE", ".mpeg", ".mpg", ".mpv2", ".ra", ".ram", ".s3m", ".TS", ".TTS", ".voc", ".wvx", ".xm", ".zpl"
		)
		$video = $associations | Where-Object { $_.Split(',')[0] -in $video_extensions }
		$cache['Video'] = $video
		
		# Image
		$image_extensions = @(
			".avif", ".bmp", ".dib", ".gif", ".heic", ".heif", ".hif", ".ico", ".jfif", ".jpe",
			".jpeg", ".jpg", ".jxr", ".png", ".tif", ".tiff", ".wdp", ".cr2", ".crw", ".dcx", ".jif",
			".jpc", ".nef", ".pcx", ".pgm", ".pic", ".ppm", ".psd", ".raf", ".rle", ".rw2", ".xbm", ".xpm"
		)
		$image = $associations | Where-Object { $_.Split(',')[0] -in $image_extensions }
		$cache['Image'] = $image
		
		# Bureautique
		$office_extensions = @(
			".doc", ".docm", ".docx", ".dot", ".dotm", ".dotx", ".iqy", ".odt", ".pot", ".potm", ".potx",
			".pps", ".ppsx", ".ppt", ".pptm", ".pptx", ".odp", ".ods", ".pub", ".rtf", ".xls", ".xlsb",
			".xlsm", ".xlsx", ".xltm", ".xltx", ".csv", ".slk", ".xlw"
		)
		$Office = $associations | Where-Object { $_.Split(',')[0] -in $office_extensions }
		$cache['Office'] = $Office
		
		# Write cache to JSON file
		$json = $cache | ConvertTo-Json
		$json | Out-File -Encoding UTF8 -FilePath "c:\users\$env:username\appdata\Roaming\Parx\cache.json"
	
	}
	# Check if selected ProgId is installed on computer
	function Check-ProgId ($progid)
	{
		# Check if Parx selected default program is installed on the computere
		$Program = $progsid.GetEnumerator() | Where-Object { $_.Name -like "*$progid*" -or $_.Value -like "*$progid*" } | Select-Object -First 1
		
		if ($Program)
		{
			return $true
		}
		else
		{
			return $false
		}
	}
	
	# Grab all ProgIds that exists on the computer, for future check 
	function Get-ProgsId
	{
		$ftypeMap = @{ }
		cmd /c ftype | foreach {
			$FileType, $Executable = $_ -split '='
			$ftypeMap.Add($FileType, $Executable)
			
		}
		# Search for the Photos app registry key
		$photoAppKey = Get-ChildItem -Path "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\Repository\Packages" -Recurse |
		Where-Object { $_.Name -like "*Microsoft.Windows.Photo*" } |
		Select-Object -First 1
		
		if ($photoAppKey)
		{
			# Get the file associations from the Photos app registry subkey
			$photoAssocKey = Join-Path -Path $photoAppKey.PSPath -ChildPath "App\Capabilities\FileAssociations"
			Get-ItemProperty -Path $photoAssocKey |
			Select-Object -Property * |
			ForEach-Object {
				foreach ($entry in $_.PSObject.Properties)
				{
					$ftypeMap[$entry.Name] = $entry.Value
				}
			}
		}
		return $ftypeMap
	}
	
	function Set-Pdf ($pdfname)
	{
		switch ($pdfname)
		{
			"Acrobat Reader" {
				if (Check-ProgId "AcroExch.Document.DC")
				{
					$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList ".pdf AcroExch.Document.DC"
				}
			}
			"Firefox" {
				if (Check-ProgId "Firefox")
				{
					$firefox = ($progsid.GetEnumerator() | Sort-Object Name | Where-Object { $_.Name -like "*firefoxpdf*" -or $_.Value -like "*firefoxpdf*" } | Select-Object -First 1).key
					$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList ".pdf $firefox"
				}
			}
			"Chrome" {
				if (Check-ProgId "Chrome")
				{
					$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList ".pdf ChromeHTML"
				}
			}
			"Edge" {
				if (Check-ProgId "Edge")
				{
					$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList ".pdf MSEdgePDF"
				}
			}
			"Foxit Reader" {
				if (Check-ProgId "Foxit")
				{
					$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList ".pdf FoxitReader.Document"
				}
			}
		}
	}
	
	function Set-Media ($medias)
	{
		foreach ($media in $medias.GetEnumerator())
		{
			if ($media.Value -ne "ne_pas_modifier")
			{
				$ext_file = "C:\parxshell\Extensions\$($media.Name)_$($media.Value).txt"
				Get-Content -Path $ext_file -ErrorAction SilentlyContinue -First 1 |
				ForEach-Object  {
					$progid = $_.Split(",")[1].Trim()
					if (Check-ProgId $progid)
					{
						Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList "`"$ext_file`""
					}
					
				}
			}
		}
		
	}
	
	function Set-Bureautique ($medias)
	{
		foreach ($bureautique in $bureautiques.GetEnumerator())
		{
			$ext_file = "C:\parxshell\Extensions\$($bureautique.Name)_$($bureautique.Value).txt"
			Get-Content -Path $ext_file -ErrorAction SilentlyContinue -First 1 |
			ForEach-Object  {
				$progid = $_.Split(",")[1].Trim()
				if (Check-ProgId $progid)
				{
					Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetUserFTA.exe" -ArgumentList "`"$ext_file`""
				}
				
			}
		}
	}
	
	function Set-Browser ($browsername, $first_logon)
	{
		# Define an empty array to hold the browser objects
		$browsers = @()
		
		# Get the output of the SetDefaultBrowser command, that will list all installed browsers and their corresponding hives
		$output = C:\parxshell\bin\SetDefaultBrowser.exe
		
		# Now we have to parse this output into ps objects
		# Find the line number where the browser entries start
		$index = $output.IndexOf("Installed browsers:") + 1
		
		# Loop through each line in the output starting from the index
		$output[$index .. ($output.Length - 1)] | ForEach-Object {
			
			# Check if the line starts with "HKLM" or "HKCU"
			if ($_ -match "^HK(LM|CU)\s+(.+)$")
			{
				# Read the hive
				$hive = $output[$index]
				
				# Read the next line to get the name
				$name = $output[$index + 1]
				
				# Read the next line to get the path
				$path = $output[$index + 2]
				
				# Use regex to extract the path value from the line
				$pathPattern = "path:\s*(.+)"
				if ($path -match $pathPattern)
				{
					$path = $matches[1]
				}
				else
				{
					$path = ""
				}
				
				# Create a new browser object and add it to the array
				$browser = [PSCustomObject]@{
					Hive = $hive
					Name = $name.Replace("name:", "").Trim()
					Path = $path.Replace('"', "")
				}
				$browsers += $browser
			}
			
			# Increment the line index
			$index++
		}
		# Get the hive part of the powershell object corresponding to the defined default browser from server
		$defaultbrowser = ($browsers | Where-Object { $_.Name -like "*$browsername*" })[0].Hive
		
		# check if the browser hive name contains a space, as SetDefaultBrowser needs some "" if so
		if ($defaultbrowser.Substring(5).Contains(" "))
		{
			$hive = "$($defaultbrowser.Split(" ")[0]) `"$($defaultbrowser.Substring(5))`""
		}
		else
		{
			$hive = $defaultbrowser
		}
		
		# Set the default browser
		log2 "[PROGRAMS] Setting default browser to $browsername with Hive $hive" "info"
		# Set the default browser
		$null = Start-Process -NoNewWindow -FilePath "C:\parxshell\bin\SetDefaultBrowser.exe" -ArgumentList $hive
	}
	$debugmode = "DEBUG"
	$eventid = "EVENTID"
	$debug_level = "LEVEL"
	$first_logon = "FIRSTLOGON"
	if ($debugmode -eq $true)
	{
		$DebugPreference = "Continue"
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$host.ui.rawui.windowtitle = "Parxshell debug"
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
	}
	
	$json = Invoke-RestMethod -Uri "IPPARX/rest_file_assoc/PROFILPARX" -Headers @{ Accept = "application/json" }
	
	if ($json.Code -eq 0 -and $json.message -ne "null")
	{
		# Set the cache to grab default associations
		if ($first_logon -eq $true)
		{
			Set-Cache
		}
		# Gets all ProgID
		$global:progsid = Get-ProgsId
		$list = $json.message | ConvertFrom-JSON | ConvertFrom-JSON
		# Browsers
		if ($list.Browser -ne "ne_pas_modifier")
		{
			Set-Browser $list.Browser $first_logon
		}
		
		# PDF
		Set-Pdf $list.pdf
		# Audio, Video
		$medias = @{ Video = $list.video; Audio = $list.music; Image = $list.image }
		Set-Media $medias
		# Bureautique
		$bureautiques = @{ Bureautique = $list.office }
		Set-Bureautique $bureautiques
		
		
	}
	Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Programs Done" -Category 1 -RawData 10, 20
	exit
}

# Prepare the command to be used as Current user
function Manage-Programs
{
	[CmdletBinding()]
	Param (
		[switch]$Remove_all,
		[boolean]$Force,
		[boolean]$debugmode,
		[int]$debug_level,
		$sessionID
	)
	Try
	{
		If ($debug_level -eq 1)
		{
			$visible = $true
		}
		else
		{
			$visible = $false
		}
		
		# We have for the user session to exists to launch these functions within user session
		Do
		{
			Start-Sleep -Milliseconds 100
		}
		While ((Get-Process -Name Logonui -ErrorAction SilentlyContinue).SI -eq $sessionID)
		
		if ($Remove_all)
		{
			Log2 "[ASSOCIATIONS] Re-setting all associations to defaults one (ie. the one that were set before parx was launched for the first time" "warn"
			$def = (Get-ChildItem "Function:Delete-Programs").Definition
			$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid).Replace("FIRSTLOGON", $first_logon)
			$block = [scriptblock]::Create($def)
			invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -Nowait
		}
		else
		{
			$def = (Get-ChildItem "Function:Set-Programs").Definition
			$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid).Replace("FIRSTLOGON", $first_logon)
			$block = [scriptblock]::Create($def)
			invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -Nowait
		}
		
	}
	catch
	{
		$err = $_.Exception.Message
		log2 "[PROGRAMS] Load PROGRAMS : $err" "error"
		
	}
	
}


# Prepare default programs functions
Function set-programs-list
{
	param (
		$profileid,
		[boolean]$Debugmode,
		[int]$Debug_level,
		$sessionid,
		[switch]$unloadProfile
	)
	
	
	$json = get-programs-list $profileid
	
	if ($json.code -eq 0 -and $json.Message -ne "null")
	{
		Manage-Programs -Debugmode $Debugmode -debug_level $Debug_level -sessionID $sessionid
	}
	elseif ($json.code -ne 0 -or $unloadProfile)
	{
		Manage-Programs -Debugmode $Debugmode -debug_level $Debug_level -sessionID $sessionid -Remove_all
		
	}
	else
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Programs Done" -Category 1 -RawData 10, 20
		
	}
	
	
	
}



