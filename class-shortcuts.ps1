class Shortcut
{
	[string]$name
	[string]$target
	[string]$arguments
	[string]$description
	[string]$workingDir
	[string]$path
	[string]$username
	
	Shortcut([string]$link_name, [string]$link_target, [string]$link_args, [string]$link_description, [string]$link_workingDir, [string]$path, [string]$username)
	{
		$this.name = $link_name
		$this.target = $link_target
		$this.arguments = $link_args
		$this.description = $link_description
		$this.workingDir = $link_workingDir
		$this.path = $path
		$this.username = $username
		
	}
	[void] CheckUserName()
	{
		if ($this.name -like "*%username%")
		{
			$this.name = $this.name.Replace("%username%", $this.username)
		}
	}
	[void] CheckHomedir()
	{
		if ($this.target -like "*%homedir%")
		{
			log2 "[SHORTCUT] CHECKING HOMEDRIVE FOR $($this.target)" "success"
			
			$homeletter, $homedrive, $homeshare = Test-Homedrive -first_logon $false
			$this.target = $homeshare
			log2 "[SHORTCUT] HOMELETTER SHOULD BE $($homeletter) with HOMEDRIVE $($homedrive) on HOMESHARE $($homeshare)  FOR $($this.target)" "success"
			
		}
	}
}
# This function will be injected as encoded command to be run as the current user
# Create a Network shortcut with USER access rights
function Create-UserShortcut
{
	# We must declare again the log2 function as it can't be reached from user session
	# We must declare again the log2 function as it can't be reached from user session
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		$lockFile = $LogFile + ".lock"
		# log level must be less or equal than log events params
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction SilentlyContinue
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
	
	Function Create-NetworkShortcut
	{
		#.DESCRIPTION
		# Create a shortcut in desired location : SHNAME for SHTARGET in SHPATH
		[CmdletBinding()]
		Param (
			[switch]$Remove_all,
			[string]$link_name,
			[string]$link_target,
			[string]$link_args,
			[string]$link_description,
			[string]$link_workingDir,
			[string]$path
		)
		
		if ($debug_level -eq 1)
		{
			
			$desc = ((((Get-help create-shortcut).Description | out-string).replace("SHNAME", $link_name).Replace("SHPATH", $path).Replace("SHTARGET", $link_target))).TrimEnd()
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		# Check if shortcut target is valid
		try
		{
			$null = Get-Acl -path ($link_target -replace '"', "") -ErrorAction Stop
			$wshshell = New-Object -ComObject WScript.Shell
			$lnk = $wshshell.CreateShortcut($path + "\" + $link_name + ".lnk")
			$lnk.TargetPath = $link_target
			$lnk.Arguments = $link_args
			$lnk.Description = $link_description
			$lnk.WorkingDirectory = $link_workingDir
			$lnk.Save()
			
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[SHORTCUT] Shortcut $($shortcut.Name) for $($shortcut.Target) applied to $path" "success"
			}
			else
			{
				log2 "[SHORTCUT] Shortcut : $link_name was drawn" "success"
			}
		}
		catch [System.Management.Automation.ItemNotFoundException]
		{
			log2 "[SHORTCUT] Target ($link_target) for $link_name not found" "Error"
			Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Target ($link_target) for $link_name not found |" -Category 1 -RawData 10, 20
		}
		catch [System.UnauthorizedAccessException]
		{
			if ($path -eq "C:\users\public\desktop")
			{
				log2 "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path. Users can't write on the public desktop" "Error"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path |" -Category 1 -RawData 10, 20
			}
			else
			{
				log2 "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path. Check $username acl on the share." "Error"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path |" -Category 1 -RawData 10, 20
				
			}
		}
		catch
		{
			log2 "[SHORTCUT] Error $($_.Exception.Message) : Target ($link_target) for $link_name on $path" "Error"
			Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "Error $($_.Exception.Message) : Target ($link_target) for $link_name on $path |" -Category 1 -RawData 10, 20
		}
		
		
	}
	
	$userShortcuts = "SHORTCUTSUSER"
	$debugmode = "DEBUG"
	$debug_level = "LEVEL"
	$eventid = "EVENTID"
	$sessionid = "SESSIONID"
	# Set the debug console if debug mode is enabled
	if ($debugmode -eq $true)
	{
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
		
		$DebugPreference = "Continue"
	}
	
	
	Foreach ($shortcut in ($userShortcuts | ConvertFrom-Json))
	{
		Create-NetworkShortcut -link_name $shortcut.Name -link_target $shortcut.Target -link_args $shortcut.arguments.replace("'", "`"") -link_description $shortcut.description -link_workingDir $shortcut.workingDir -path $shortcut.path
	}
}
# get the shortcuts list from the db (call)
Function get-taskbar-list ($profileid)
{
	return restGET "$address/profile_taskbar_shortcuts/$profileid"
}

# get the shortcuts list from the db (call)
Function get-shortcuts-list ($profileid)
{
	return restGET "$address/profile_desktop_shortcuts/$profileid"
}

# remove a shortcut
function Del-shortcut ($name)
{
	#.DESCRIPTION
	# Parx shortcut found (SHNAME) but is not in the profile (PROFILENAME) anymore, deleting it
	if ($debug_level -eq 1)
	{
		$desc = (((Get-Help Del-shortcut).Description | Out-String).Replace("SHNAME", $name).Replace("PROFILENAME", $profilename)).TrimEnd()
		Write-Host $desc -ForegroundColor DarkBlue
		(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
	}
	remove-item $name
}
# create a shortcut on the burlingue
Function create-shortcut
{
	#.DESCRIPTION
	# Create a shortcut in desired location : SHNAME for SHTARGET in SHPATH
	[CmdletBinding()]
	Param (
		[string]$link_name,
		[string]$link_target,
		[string]$link_args,
		[string]$link_description,
		[string]$link_workingDir,
		[string]$path
	)
	
	if ($debug_level -eq 1)
	{
		
		$desc = ((((Get-help create-shortcut).Description | out-string).replace("SHNAME", $link_name).Replace("SHPATH", $path).Replace("SHTARGET", $link_target))).TrimEnd()
		Write-Host $desc -ForegroundColor DarkBlue
		(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
	}
	# Check if shortcut target is valid
	try
	{
		
		$null = Resolve-Path ($link_target -replace '"', "") -ErrorAction Stop
		$wshshell = New-Object -ComObject WScript.Shell
		$lnk = $wshshell.CreateShortcut($path + "\" + $link_name + ".lnk")
		$lnk.TargetPath = $link_target
		$lnk.Arguments = $link_args
		$lnk.Description = $link_description
		$lnk.WorkingDirectory = $link_workingDir
		$lnk.Save()
		
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[SHORTCUT] Shortcut $link_name for $link_target applied to $path" "success"
		}
		else
		{
			log2 "[SHORTCUT] Shortcut : $link_name was drawn" "success"
		}
	}
	catch [System.Management.Automation.ItemNotFoundException]
	{
		log2 "[SHORTCUT] Target ($link_target) for $link_name not found" "Error"
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Target ($link_target) for $link_name not found |" -Category 1 -RawData 10, 20
	}
	catch [System.UnauthorizedAccessException]
	{
		log2 "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path" "Error"
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Access denied for $username : Target ($link_target) for $link_name on $path |" -Category 1 -RawData 10, 20
	}
	catch
	{
		log2 "[SHORTCUT] Error $($_.Exception.Message) : Target ($link_target) for $link_name on $path" "Error"
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "[SHORTCUT] Error $($_.Exception.Message) : Target ($link_target) for $link_name on $path |" -Category 1 -RawData 10, 20
	}
}

# Reset desktop if profile contains no shortcut anymore
function clear-desktop
{
	#.DESCRIPTION
	# As there is none shorcut in the profile (PROFILENAME), deleting all parx shortcuts	
	[CmdletBinding()]
	Param ()
	if ($debug_level -eq 1)
	{
		$desc = (((Get-Help clear-desktop).Description | Out-String).Replace("PROFILENAME", $profilename)).TrimEnd()
		Write-Host $desc -ForegroundColor DarkBlue
		(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
	}
	$desktopKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
	$userKey = "Registry::\HKEY_USERS\$userMSID\Volatile Environment"
	$desktopPath = (Get-ItemProperty $desktopKey).Desktop
	
	if (!($desktopPath -like "*onedrive*"))
	{
		$desktop = "c:\Users\$username\desktop"
	}
	else
	{
		$desktop = "c:\Users\$username\desktop"
	}
	
	$sh = New-Object -COM WScript.Shell
	$shortcuts = get-childitem $desktop, "c:\users\public\desktop" -Filter "*.lnk"
	$shortcuts | ForEach-Object {
		if (($sh.CreateShortcut($_.fullname).Description) -like "*[PARX]*")
		{
			Del-shortcut $_.fullname
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[SHORTCUT] $($_.fullname) Deleted" "success"
			}
			
		}
	}
	if ($DebugPreference -eq 'Continue')
	{
		log2 "[SHORTCUTS] All shortcuts deleted" "task"
	}
}

# draw each shortcut on the burlingue
Function draw-shortcut-list
{
	#.DESCRIPTION
	# Set the action to do on a shortcut from the profile (PROFILENAME)
	[CmdletBinding()]
	Param (
		$profileid,
		[string]$debugpref,
		[string]$username,
		[string]$folder_cache,
		[switch]$switch_profile,
		$autolaunch,
		$sessionID,
		$noDesktop,
		[switch]$unloadProfile
	)
	
	if ($debug_level -eq 1)
	{
		$desc = (((Get-Help draw-shortcut-list).Description | Out-String).Replace("PROFILENAME", $profilename)).TrimEnd()
		Write-Host $desc -ForegroundColor DarkBlue
		(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
	}
	# We retrieve and process the DisableDesktop within the shortcut runspace, not the options one; as user shortcuts will need an unlock/relock 
	if ($noDesktop -eq $false)
	{
		disabledesktop(0)
	}
	else
	{
		disabledesktop(1)
	}
	
	# get shortcuts list
	$json = get-shortcuts-list $profileid
	$json_taskbar = get-taskbar-list $profileid
	if ($json.code -eq 0)
	{
		#$list = json-array $json.message
		$list = ConvertFrom-JSON $json.message
		$sh = New-Object -ComObject WScript.Shell
		
		# Array for shortcuts to be drew from within user session
		$userShortcuts = @()
		$pathShortcuts = @()
		# Check if desktop is synced with onedrive
		$desktopKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
		$userKey = "Registry::\HKEY_USERS\$userMSID\Volatile Environment"
		$desktopPath = (Get-ItemProperty $desktopKey).Desktop
		$userDesktop = "c:\Users\$username\desktop"
		
		foreach ($shortcut in $list)
		{
			
			# draw on the burlingue
			switch ($shortcut.location)
			{
				"0" {
					
					if (!($desktopPath -like "*onedrive*"))
					{
						$path = $userDesktop
					}
					else
					{
						$path = $desktopPath
					}
					
				}
				"1" {
					$desktop = "C:\Users\Public\desktop"
					$path = $desktop
				}
			}
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[SHORTCUT] Shortcut $($shortcut.Name) for $($shortcut.Target) in $path found, setting it" "info"
			}
			
			$description = $shortcut.description + "[PARX]"
			
			[Shortcut]$_shortcut = [Shortcut]::new($shortcut.name, $shortcut.Target, $shortcut.arguments, $description, $shortcut.workingDir, $path, $username)
			$_shortcut.CheckUserName(), $_shortcut.CheckHomedir()
			$visible = $false
			# Add full shortcut path to pathShortuts array, for further processing (clearing shortcuts that aren't anymore on the profile)
			$pathShortcuts += "$path\$($_shortcut.name).lnk"
			# Check if shortcut is new, or has been changed in profile
			if (!(Test-Path "$path\$($_shortcut.name).lnk"))
			{
				log2 "[SHORTCUT] Shortcut $($_shortcut.Name) for $($_shortcut.Target) is being drawn" "info"
				# Check if shortcut is a network share
				# If so add it to userShortcuts array for further process
				if (!($_shortcut.target).StartsWith("C:"))
				{
					log2 "[SHORTCUT] Shortcut $($_shortcut.Name) for $($_shortcut.Target) is a network shortcut, it will be processed within user session" "info"
					$userShortcuts += $_shortcut
					
				}
				# Else, just draw it from system
				else
				{
					create-shortcut -link_name $_shortcut.name -link_target $_shortcut.target -link_args $_shortcut.arguments -link_description $description -link_workingdir $_shortcut.workingDir -path $path
					
				}
			}
			else
			{
				# Check if target or argument has changed
				$shortcutPath = "$path\$($_shortcut.name).lnk"
				$com_shortcut = $sh.CreateShortcut($shortcutPath)
				# Existing shortcut target/argument in user/public deskto
				$oldTarget = $com_shortcut.TargetPath -replace '"', ""
				$oldArguments = $com_shortcut.Arguments -replace '"', ""
				# Profile shortcut target/argument
				$newTarget = $_shortcut.target -replace '"', ""
				$newArguments = $_shortcut.arguments -replace '"', ""
				# Check against old/new values
				$targetChanged = $oldTarget -ne $newTarget
				$argumentsChanged = $oldArguments -ne $newArguments
				
				if ($targetChanged -or $argumentsChanged)
				{
					if ($targetChanged)
					{
						log2 "[SHORTCUT] Shortcut $($_shortcut.Name) for $($_shortcut.Target) has been modified. Redrawing it. Old target has changed from $oldTarget to $newTarget; and/or old argument has changed from $oldArguments to $newArguments " "warn"
					}
					
					# Check if shortcut is a network share
					# If so add it to userShortcuts array for further process
					if (!($_shortcut.target).StartsWith("C:"))
					{
						$userShortcuts += $_shortcut
						
					}
					# Else, just draw it from system
					else
					{
						create-shortcut -link_name $_shortcut.name -link_target $_shortcut.target -link_args $_shortcut.arguments -link_description $description -link_workingdir $_shortcut.workingDir -path $path
						
					}
				}
				else
				{
					log2 "[SHORTCUT] Shortcut $($_shortcut.Name) for $($_shortcut.Target) already present in $path" "info"
				}
			}
			
			
			# Add in synchronised hashtable shortcuts that will be autolaunched		
			if ($shortcut.Startup -eq 1)
			{
				$soft = [pscustomobject]@{ Target = "$path\$($shortcut.name).lnk"; Process = $shortcut.target; Name = $shortcut.Name }
				$autolaunch.Soft += $soft
			}
		}
		
		
		# Process userShortcuts from user session if any
		if ($userShortcuts.Count -gt 0)
		{
			# Unlock desktop if profile option disabledesktop is enabled
			# Lock desktop again if profile option disabledesktop is enabled
			if ($noDesktop -eq $true)
			{
				disabledesktop(0)
				log2 "[SHORTCUT] Profile Options DisableDesktop is enabled, unlocking desktop" "Info"
			}			
			#Forging current user function to be invoked if shortcut is a network share
			$def = (Get-ChildItem "Function:Create-UserShortcut").Definition
			$def = $def.Replace("SHORTCUTSUSER", ($($userShortcuts | ConvertTo-Json -Compress).replace("`"", "'"))).Replace("SESSIONID", $sessionID).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			$block = [scriptblock]::Create($def)
			# We have for the user session to exists to launch these functions within user session
			Do
			{
				Start-Sleep -Milliseconds 100
			}
			While ((Get-Process -Name Logonui -ErrorAction SilentlyContinue).SI -eq $sessionID)
			Log2("[SHORTCUTS] Processing network shortcuts")
			invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -NonElevatedSession
			# Lock desktop again if profile option disabledesktop is enabled
			if ($noDesktop -eq $true)
			{
				disabledesktop(1)
				log2 "[SHORTCUT] Profile Options DisableDesktop is enabled, locking desktop" "Info"
			}
		}
		
		
		#Delete shortcuts removed from profile/for whose location (user/public) changed but still present on user/public desktops
		$shortcuts = get-childitem $userDesktop, "c:\users\public\desktop" -Filter "*.lnk"
		$shortcuts | ForEach-Object {
			$changedlocation = ($sh.CreateShortcut($_.fullname).Description -like "*[PARX]*" -and $_.FullName -notin ($pathShortcuts))
			$removedfromprofile = ($sh.CreateShortcut($_.fullname).Description -like "*[PARX]*" -and (($_.Name).replace(".lnk", "").Replace($username, "%username%") -notin ($list.Name)))
			if ($changedlocation -eq $true)
			{
				$name = ($_.Name).replace(".lnk", "")
				Del-shortcut $_.fullname
				if ($removedfromprofile -eq $true)
				{
					log2 "[SHORTCUT] $name was removed from profile, deleting done" "warn"
				}
				else
				{
					log2 "[SHORTCUT] $name was changed from User to Public desktop (or Public to User, can't tell), redraw done" "warn"
				}
			}
		}
		if ($switch_profile)
		{
			# Load newly chosen profile desktop
			Set-IconPosition -username $username -profileid $profileid -folder_cache $folder_cache -save_load "Load"
		}
		#		# Add Parxgui c# class if not already loaded
		#		if (!("Parxgui.Refresh_Explorer" -as [type]))
		#		{
		#			Add-Type -Namespace Parxgui -Name Refresh_Explorer -MemberDefinition $refresh
		#		}
		#		# Send a refresh message to explorer
		#		Refresh-Explorer
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[SHORTCUTS] All shortcuts were drawed" "task"
		}
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Shortcuts done" -Category 1 -RawData 10, 20
	}
	elseif ($json.code -ne 0 -or $unloadProfile)
	{
		log2 "[SHORTCUTS] No shortcuts for this profile, checking if [PARX] shortcuts are to be removed" "Warn"
		clear-desktop
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Shortcuts done" -Category 1 -RawData 10, 20
	}
	# Add taskbar autolaunch shortcuts to autolaunch hashtable only if desktop shortcut is not already in hashtable
	if ($json_taskbar.code -eq 0)
	{
		$list = ConvertFrom-JSON $json_taskbar.message
		foreach ($shortcut in $list)
		{
			# Add in synchronised hashtable shortcuts that will be autolaunched		
			if ($shortcut.Startup -eq 1 -and $shortcut.Target -notin $autolaunch.Soft.Target)
			{
				$path = "c:\users\$username\appdata\Roaming\microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar"
				$target = $shortcut.Target -replace '"', ""
				$desc = (Get-ChildItem -Path $target).VersionInfo.FileDescription
				if ($desc -ne "")
				{
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] Description for $target found : $desc |" -Category 1 -RawData 10, 20
					}
					$file = $desc + ".lnk"
					
					
				}
				# if no description, shortcut name is target.lnk (ex "C:\test\toto.exe" shortcut name will be "toto.lnk")
				else
				{
					$file = [io.path]::GetFileNameWithoutExtension($target)
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] No Description for $target was found, windows will use its file ($file) name instead. |" -Category 1 -RawData 10, 20
					}
					$file = $file + ".lnk"
					
				}
				$soft = [pscustomobject]@{ Target = "$path\$file"; Process = $shortcut.target; Name = $shortcut.Name }
				$autolaunch.Soft += $soft
			}
		}
		
	}
}

# save the shortcuts list from the burlingue to the db (with add new shortcuts + assoc)
Function save-shortcut-list ()
{
	
	# The url to call
	$url = "$address/save_shortcuts"
	# All the links of all desktops (private + public)
	$desktop = @()
	# list of links
	$shortcuts = get-childitem "c:\users\$username\desktop", "c:\users\public\desktop", "c:\Users\$username\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar" -Filter "*.lnk"
	foreach ($item in $shortcuts)
	{
		Switch ($item.FullName)
		{
			{ !($_.Contains("public")) } { $location = "0" }
			{ $_.Contains("public") } { $location = "1" }
			{ $_.Contains("TaskBar") } { $location = "3" }
			
		}
		# clear the array		
		$link = @()
		$sh = New-Object -COM WScript.Shell
		$targetPath = $sh.CreateShortcut($item.fullname).TargetPath
		$description = $sh.CreateShortcut($item.fullname).Description -replace "\[PARX\]", ""
		$arguments = $sh.CreateShortcut($item.fullname).Arguments
		$workingDir = $sh.CreateShortcut($item.fullname).WorkingDirectory
		$name = $item.name -replace ".lnk$", ""
		$link = @{ name = $name; target = $targetPath; description = $description; arguments = $arguments; workingDir = $workingDir; location = $location }
		try
		{
			$desktop += @($link)
		}
		catch
		{
			$err = $_.Exception.Message
			log2 "[SHORTCUTS] $err on $path" "error"
		}
		
	}
	
	# add public shortcuts to db	
	# The body to send to the server
	$body = @{
		profileId = $global:profileid
		desktop   = $desktop | ConvertTo-Json
	}
	return restPOST -uri $url -body $body -api_token $token
	
}

function Save-Desktop
{
	$json = save-shortcut-list
	$list = ConvertFrom-JSON $json.message
	
	foreach ($item in $list)
	{
		
		if ($item.code)
		{
			if ($item.code -eq 0)
			{
				log2 $item.message "success"
			}
			else
			{
				log2 $item.message "error"
			}
		}
		
	}
}