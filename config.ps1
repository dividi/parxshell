# Version du client
$version = "20230626"

# Active / désactive le lancement du client Parx (1:activé, 0:désactivé)
$enable_parx = 1

# Adresse du serveur Parx SANS SLASH à la fin (ex : http://10.231.45.110:8000/api)
$address = ""

# Adresse du proxy (TOUJOURS mettre http:// devant l'adresse du proxy. NE PAS renseigner sous SERCOL !)
$webproxy = ""

# Kick les users sans profil (1:activé, 0:désactivé)
$noneShallPass = 0

# Montre ou cache les menus sensibles dans le gui (1:montre, 0:cache)
$full_gui = 0

# Montre ou cache les messages de parxgui (Information profil chargé, erreurs sur printer, share, shortcut)
$show_notification = 0

# Refresh rate (en minutes) de la recherche de tâches à effectuer par le service (0 : désactivé, sinon 5 minimum. 1 à 4 ne seront pas pris en compte.)
$service_refresh = 0

# Chargement automatique du profil (ie. desktop, taskbar, options, programmes, imprimantes, partages, tâches du profil)
$global:autoload_options = 1
$global:autoload_printers = 1
$global:autoload_shares = 1
$global:autoload_desktop = 1
$global:autoload_taskbar = 1
$global:autoload_associations = 1

# admin de profils (séparé par des virgules, tokens acceptés)
# Les admins de profils ne chargent pas le profil à l'ouverture de session, ont accès au fullgui et peuvent sauvegarder/restaurer des profils depuis le GUI.
$profiles_admins = "adm.*"



