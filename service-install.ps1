param (
	[switch]$Check_Numser,
	[string]$Numser
)

# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator'))
{
	if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000)
	{
		$CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $PSBoundParameters.Check_Numser + "-Numser " + $PSBoundParameters.Numser
		Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine -WindowStyle Hidden
		#Exit
	}
}


# If parxshell.msi is launched with Check_Numser property, which is the default value, we either ask for the numser; or take it from the NUMSER property if defined
if ($PSBoundParameters.Check_Numser)
{
	# Config file path
	$scriptfile = "C:\parxshell\config.ps1"
	# msiexec /i parxshell
	# If Numser is 9999 (which is the default value from the msi Numser property), ask 
	if ($PSBoundParameters.Numser -eq "9999")
	{
		# For input box
		Add-Type -AssemblyName Microsoft.VisualBasic		
		# Get Primary DNS adress
		$_numser = (Get-NetAdapter | Where-Object { -not $_.Virtual -and $_.Status -eq 'up' }).ifIndex | Get-DnsClientServerAddress -AddressFamily IPv4 | Select-Object -ExpandProperty ServerAddresses
		# Check with user if Numser (Third dot decimal value)
		$check = [Microsoft.VisualBasic.Interaction]::InputBox("V�rifiez et corrigez votre NUMSER si besoin", "Adresse du serveur parx", "$($_numser[0].Split(".")[2])")
		# Set Parx server in config file accordingly
		(Get-Content $scriptFile) -replace '\$address = "(.*)"', "`$address = `"http://10.231.$check.110:8000/api""" | Set-Content $scriptFile
	}
	# msiexec /i parxshell NUMSER=xxx
	else
	{
		(Get-Content $scriptFile) -replace '\$address = "(.*)"', "`$address = `"http://10.231.$($PSBoundParameters.Numser).110:8000/api""" | Set-Content $scriptFile
	}
	
}



# Install Parxshell service
c:\parxshell\bin\nssm install ParxShell "powershell.exe" "-noprofile -nologo -WindowStyle Hidden -noexit -ExecutionPolicy Bypass -File c:\parxshell\service.ps1"
# Adds some service info and secure acl
$regKey = "Registry::\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ParxShell"
$value = "76492d1116743f0423413b16050a5345MgB8AHIAegBwAHoANgBwADkAMAA4AHIAdQBxADMAOABqAGEAMgBPAE0AdQBvAFEAPQA9AHwAOABlADQAZABhADIANQA1AGQANwBhADcAZgA0ADkAMwBhAGYAOAAxAGQANQA4AGEANgA1AGIAOQA3ADMAOAAwAGUAYgA0ADcAYgBhAGQANgA1AGIAOQAzAGUANAAwAGIAMgBkADcAZgBmAGEAYwA3ADUAOQBlAGMANwAzADkAYgBjADgAZgBlADgANgAwAGIAMQA2ADEAMwA5ADkAMgA1ADgANwAxAGUANwAxAGQAMgBmAGEAMAA4AGUAMwA4ADIAMgA4AGMAOQA1AGEAYQBjAGQAMQAxADAAMwA3ADQAZQAyAGUAMgAzAGMAOAA4ADMAMgAxAGYAZQA5ADUAYQA5ADkANQBlAGEANgBkADkANQBiADIAZgAxAGQANgA2ADkAMQAyADgAYgAyAGEAOQBlAGQAZgAyADAAZQA1ADQAMgAyADIAYQA0ADEAMgA5ADkAZAAyAGIAMwAyAGQAMwA4ADEAMwBlADkAMgBhADUANwA2ADMAZAA3ADkAZABlADcA"
$setprop = Set-ItemProperty -path $regKey -name "To" -type String -value $value -force | Out-null
$regKey = "HKLM:\SYSTEM\CurrentControlSet\Services\ParxShell"
$acl = Get-Acl -path $regKey
$rule = New-Object System.Security.AccessControl.RegistryAccessRule("Utilisateurs", "ReadKey", "ContainerInherit,ObjectInherit", "None", "Deny")
$acl.AddAccessRule($rule)
$acl | Set-Acl -Path $regKey

# Add Parx Reg to Software
if ((Test-Path -LiteralPath "HKLM:\SOFTWARE\Parxshell") -ne $true) { New-Item "HKLM:\SOFTWARE\Parxshell" -force -ea SilentlyContinue };
New-ItemProperty -LiteralPath 'HKLM:\SOFTWARE\Parxshell' -Name 'Initialized' -Value 0 -PropertyType DWord -Force -ea SilentlyContinue;
c:\parxshell\Bin\nssm set ParxShell Description "Service Parx v2"
# Start Parxshell
c:\parxshell\Bin\nssm start ParxShell
net start ParxShell




