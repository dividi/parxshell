# get the list of shares from the db
Function get-shares-list ($profileid)
{
	return restGET "$address/rest_shares/$profileid" -proxy
}

# This function will be injected as encoded command to be run as the current user
# delete ALL PARX shares if no shares are found in profile
function Delete-Shares
{
	function Delete-Share ($letter)
	{
		net use "${letter}" /delete /y
	}
	
	$force = "TRUE/FALSE"
	$debugmode = "DEBUG"
	$debug_level = "LEVEL"
	$eventid = "EVENTID"
	# Set the debug console if debug mode is enabled
	if ($debugmode -eq $true)
	{
		$DebugPreference = "Continue"
	}
	# Check for mounted share
	Foreach ($share in (Get-childItem -Path "Registry::\HKEY_CURRENT_USER\Network" | Get-ItemProperty))
	{
		# Check for PARX tag, so that only parx shares will be unmounted
		if ($share.'[PARX]' -eq 1 -or $force -eq $true)
		{
			$letter = $share.PsChildName + ":"
			Delete-Share $letter
			# Check if share was successfully unmounted
			if (!(Test-Path -path $share.PsChildName))
			{
				$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
				$LogMutex = New-Object System.Threading.Mutex($false, "LogMutex")
				$LogMutex.WaitOne() | out-null
				if ($DebugPreference -eq 'Continue')
				{
					Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[SHARES] $letter was unmounted. |" -Category 1 -RawData 10, 20
				}
				Add-Content -Path "c:\Parxshell\Logs\parx.log" "$now [SUCCESS] [SHARES] $letter was unmounted."
				$LogMutex.ReleaseMutex() | out-null
			}
		}
	}
	if ($force -eq $false)
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Share Done" -Category 1 -RawData 10, 20
		exit
	}
}

function Set-Log
{
	# We must declare again the log2 function as it can't be reached from user session
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		# log level must be less or equal than log events params
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction SilentlyContinue
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
}

# This function will be injected as encoded command to be run as the current user
# Set shares that are found in profile
Function Set-shares
{
	
	
	Function Test-RegistryValue
	{
		param ([string]$RegKeyPath,
			[string]$Value,
			[boolean]$getvalue)
		
		if ($getvalue -eq $false)
		{
			$ValueExist = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).$Value -ne $null
			Return $ValueExist
		}
		else
		{
			$RegValue = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).$Value
			Return $RegValue
		}
		If ($Reg_not_found)
		{
			Return $false
		}
	}
	function Mount-Share ($letter, $share)
	{
		#.DESCRIPTION
		# Mounting share
		if ($debug_level -eq 1)
		{
			(Get-Help Mount-share).Description
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		$null = net use /persistent:yes "${letter}:" $share
	}
	function Delete-Share ($letter)
	{
		#.DESCRIPTION
		# Deleting share
		if ($debug_level -eq 1)
		{
			(Get-Help Delete-Share).Description
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		$null = net use "${letter}:" /delete /y
	}
	$debugmode = "DEBUG"
	$debug_level = "LEVEL"
	$eventid = "EVENTID"
	# Set console mode if debug mode is enabled
	if ($debugmode -eq $true)
	{
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
		$DebugPreference = "Continue"		
	}
	
	$json = Invoke-RestMethod -uri "IPPARX/rest_shares/PROFILPARX" -Headers @{ Accept = "application/json" }
	if ($json.Code -eq 0)
	{
		$list = ConvertFrom-JSON $json.message
		$existing_shares = Get-childItem -Path "Registry::\HKEY_CURRENT_USER\Network"
		
		# If Parx share are already mounted, check if they are still present in the profile, unmount if not
		if ($existing_shares -ne $null)
		{
			foreach ($share in ($existing_shares | Where-Object { (Test-RegistryValue "Registry::\$($_.Name)" "[PARX]") -eq $true }))
			{
				$share_letter = $share.Name -replace '.*\\'
				# Unmount share
				if ($share_letter -notin $list.Pivot.Shareletter)
				{
					$msg = "[SHARES] Drive [${share_letter}:] was removed from profile, unmounting it"
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
					}
					log2 $msg "warn"
					Delete-Share $share_letter
					# Check if share was successfully unmounted
					if (!(Test-Path -Path $share.Name))
					{
						$msg = "$now [SHARES] $share was unmounted."
						log2 $msg"success"						
						if ($DebugPreference -eq 'Continue')
						{
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						}
					}
				}
			}
		}
		# Check for each share in profile 
		foreach ($share in $list)
		{
			$letter = $share.Pivot.shareLetter
			# Check if share target is valid
			
			try
			{
				$null = Resolve-Path ($share.Target -replace '"', "") -ErrorAction Stop
				$null = Get-ChildItem -Path ($share.Target -replace '"', "") -ErrorAction Stop
				$DriveKey = "Registry::\HKEY_CURRENT_USER\Network\$letter"
				$same_share = Test-RegistryValue $DriveKey "RemotePath" -getvalue $true
				$parx_share = (Get-childItem -Path $Changed_Drive_Key |
					Get-ItemProperty |
					Where-Object{
						$_.RemotePath -eq $share.Target
					}).PsChildName
				
				if ($same_share -and $same_share -ne $share.Target)
				{
					# Target changed, unmount share and mount it again with correct target
					$msg = "[SHARES] Target for Share [$($share.Target)] on drive [$letter] was modified, but letter is the same, deleting existing share and mounting it again with new target"
					log2 $msg "warn"
					Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
					Delete-Share $letter
					Start-Sleep -Milliseconds 100
					Mount-Share $letter $share.Target
					# Check if share is successfully mounted, and tag its registry key with PARX
					If ((Test-RegistryValue $DriveKey "RemotePath" -getvalue $true) -eq $share.Target)
					{
						$msg = "[SHARES] Mounting share [$($share.Target)] on drive [$letter]"
						log2 $msg "success"
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						
						#Set parx tag on drive, for further removal
						Set-ItemProperty -path $DriveKey -name "[PARX]" -type DWORD -value 1 -force | Out-null
						
					}
					else
					{
						$msg = "[SHARES] Share [$($share.Target)] on drive [$letter] is not available. Check wether target is the good one, or that the user $env:username has rights to access it."
						log2 $msg "error"
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType error -Message "$msg |" -Category 1 -RawData 10, 20
						
					}
				}
				# Share already mounted
				if ($same_share -and $same_share -eq $share.Target)
				{
					$msg = "[SHARES] Share [$($share.Target)] on drive [$letter] already mounted"
					log2 $msg "warn"
					Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
					
				}
				# Check whether letter from profile has changed, unmount share and mount it with correct letter
				foreach ($share in $parx_share)
				{
					#Letter changed
					if ($share -ne $letter)
					{
						$msg = "[SHARES] Letter for Share [$($share.Target)] on drive [$letter] was modified, but share is the same, deleting existing share and mounting it again with new letter"
						log2 $msg "warn"
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						Delete-Share $letter
						Start-Sleep -Milliseconds 50
						Mount-Share $letter $share.Target
						If ((Test-RegistryValue $DriveKey "RemotePath" -getvalue $true) -eq $share.Target)
						{
							$msg = "[SHARES] Mounting share [$($share.Target)] on drive [$letter]"
							log2 $msg "success"
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
							#Set parx tag on drive, for further removal
							Set-ItemProperty -path $DriveKey -name "[PARX]" -type DWORD -value 1 -force | Out-null
							
						}
						else
						{
							$msg = "[SHARES] Share [$($share.Target)] on drive [$letter] is not available. Check wether target is the good one, or that the user $env:username  has rights to access it."
							log2 $msg "error"
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
							
						}
					}
					
				}
				# New share
				if (!($same_share))
				{
					# Check if share target is valid
					
						$msg = "[SHARES] New share, mounting [$letter] for [$($share.Target)]"
						log2 $msg "info"
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						# Mount share
						Mount-Share $letter $share.Target
						# Check if share is successfully mounted
						If ((Test-RegistryValue $DriveKey "RemotePath" -getvalue $true) -eq $share.Target)
						{
							$msg = "[SHARES] Share [$($share.Target)] on drive [$letter] is mounted"
							log2 $msg "success"
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
							#Set parx tag on drive, for further removal
							Set-ItemProperty -path $DriveKey -name "[PARX]" -type DWORD -value 1 -force | Out-null
							
						}						
				}
			}
			# Target share isn't reachable
			catch [System.Management.Automation.ItemNotFoundException]
			{
				Delete-Share $letter
				$msg = "[SHARES] Share [$($share.Target)] on drive [$letter] is not found. Check if share target is a valid one."
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				log2 $msg "error"
				continue
			}
			catch [System.UnauthorizedAccessException]
			{
				Delete-Share $letter
				$msg = "[SHARES] User $env:username doesn't have rights to access share [$($share.Target)] on drive [$letter]."
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				log2 $msg "error"
				continue
			}
			catch
			{
				Delete-Share $letter
				$msg = "[SHARES] Error $($_.Exception.Message)"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				log2 $msg "error"
				continue
			}
		}
	}
	Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Share Done" -Category 1 -RawData 10, 20
	exit
}


# Prepare the command to be used as Current user
function Manage-Shares
{
	[CmdletBinding()]
	Param (
		[switch]$Remove_all,
		[boolean]$force,
		[boolean]$Debugmode,
		[int]$debug_level,
		$sessionID
		
	)
	Try
	{
		If ($debug_level -eq 1)
		{
			$visible = $true
		}
		else
		{
			$visible = $false
		}
		# We have for the session to exists to launch these functions within user session
		Do
		{
			Start-Sleep -Milliseconds 50
		}
		While ((Get-Process -Name Logonui -ErrorAction SilentlyContinue).SI -eq $sessionID)
		# If no share is in profile, remove all PARX Printers
		If ($remove_all)
		{
			# If First_logon since parx upgrade, we have to clear all printers to mount them again with PARX tag
			if ($Force -eq $true)
			{
				$def = (Get-ChildItem "Function:Set-Log", "Function:Delete-Shares", "Function:Set-Shares").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
			else
			{
				$def = (Get-ChildItem "Function:Set-Log","Function:Delete-Shares").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
			
		}
		# Mount shares
		else
		{
			$def = (Get-ChildItem "Function:Set-Log", "Function:Set-Shares").Definition
			$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("DEBUG", $debugmode).Replace("USERNAME", $username).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
		}
		$block = [scriptblock]::Create($def)
		invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -Nowait
	}
	catch
	{
		$err = $_.Exception.Message
		log2 "[SHARE] Load SHARE : $err" "error"
		
	}
}


# Prepare printers functions
Function set-shares-list
{
	param (
		$profileid,
		[boolean]$Debugmode,
		[int]$debug_level,
		$sessionid,
		[switch]$unloadProfile
	)
	
	# get the list o shares
	$json = get-shares-list $profileid
	# If first time since parx upgrade, remove all shares and mount them again with PARX tag
	if ($first_logon -eq $true)
	{
		log2 "[SHARES] First logon since parx upgrade. Remaking shares." "warn"
		Manage-Shares -remove_all -force $true -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
	}
	else
	{
		if ($json.code -eq 0)
		{
			Manage-Shares -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
		}
		elseif ($json.code -ne 0 -or $unloadProfile)
		{
			log2 "[SHARES] No share for this profile, checking for previously [PARX] mounted shares to be removed" "warn"
			Manage-Shares -remove_all -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
		}
	}
	
}

