﻿#################### GUI


# manage and display the GUI
Function initIHM ($profiles)
{
	# Custom dll for Dark Theme
	Add-Type -Path "$PSScriptRoot\Assemblies\DarkGui.dll"
		
	$Miliseconds = 50000
	$Title = "Parxshell"
	#The form
	[reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
	[System.Windows.Forms.Application]::EnableVisualStyles()
	# Create new Objects
	$global:objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon
	$global:objContextMenu = New-Object System.Windows.Forms.contextMenuStrip
	
	$colorTable = New-Object DarkGui.DarkColorTable	
	$renderer = New-Object DarkGui.DarkProfessionalRenderer
	$objContextMenu.Renderer = $renderer
	
	$global:pngError = [System.Drawing.Image]::FromFile("$PSScriptRoot\Resources\error.png")
	$global:pngNoError = [System.Drawing.Image]::FromFile("$PSScriptRoot\Resources\noError.png")
		
	# Control Visibility and state of things
	# Assign an Icon to the Notify Icon object
	$objNotifyIcon.Visible = $true
	$objNotifyIcon.Text = "ParxShell $version"
	$objNotifyIcon.ContextMenuStrip = $objContextMenu
	$objNotifyIcon.BalloonTipIcon = [System.Windows.Forms.ToolTipIcon]::None
	$objNotifyIcon.BalloonTipTitle = "$Title"
	$objNotifyIcon.Visible = $true
	
	#If profiles are set
	if ($profiles.code -eq 0)
	{
		$currentProfile = $profileid
		Update-ServerProfile
	}
	
	# No profiles set
	else
	{
		$refreshRate = 0
		$currentProfile = 0
	}
	# add profiles list 
	$loadProfilesList = profileList $profiles
	# Set parxgui icon and text depending on wether a profile is loaded and if there are any error during loading process
	if ($global:noprofile -eq $false -and $global:ServerError -eq $false)
	{
		$currentProfileName = (ConvertFrom-JSON (get-profile $currentProfile).message).name
		$Text = "Bienvenue sur le profil $profilename !"
		$objNotifyIcon.BalloonTipText = "$Text"
		$objNotifyIcon.Text = "Profil: $currentProfileName"
		$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
	}
	elseif ($global:noprofile -eq $true -and $global:ServerError -eq $false)
	{
		$Text = "Bienvenue, aucun profil n'est chargé !"
		$objNotifyIcon.BalloonTipText = "$Text"
		$objNotifyIcon.Text = "Profil: Aucun profil"
		$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_grey.ico"
	}
	elseif ($global:ServerError -eq $true)
	{
		$Text = "Bienvenue, le serveur n'est pas joignable !"
		$objNotifyIcon.BalloonTipText = "$Text"
		$objNotifyIcon.Text = "Serveur injoignable"
		$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_red.ico"
	}
	if ($isprofileadmin -eq 1)
	{
		$Text = "Bienvenue, mode admin de profil !"
		$objNotifyIcon.BalloonTipText = "$Text"
		$objNotifyIcon.Text = "Mode admin de profil"
		$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_blue.ico"
	}
	# Welcome notification, if chosen so in config file
	if ($show_notification -eq 1)
	{
		$objNotifyIcon.ShowBalloonTip($Miliseconds)
	}
	
	$dsearch = New-Object System.Windows.Forms.TextBox
	$dsearch.BackColor = 'Black'
	$dsearch.ForeColor = 'DodgerBlue'
	$dsearch.Location = New-Object System.Drawing.Point(13, 374)
	$dsearch.Name = 'textbox5'
	$dsearch.Size = New-Object System.Drawing.Size(200, 23)
	$dsearch.TabIndex = 10
	$dsearch.Multiline = $false
	$dsearch.Font = [System.Drawing.Font]::new('Segoe UI', '10')
	# Trap enterkey to send search event
	$dsearch_KeyDown = [System.Windows.Forms.KeyEventHandler]{
		if ($_.KeyCode -eq 'Enter')
		{
			$isearch.PerformClick()
			#Suppress sound from unexpected use of enter on keyPress/keyUp
			$_.SuppressKeyPress = $true
		}
	}
	$dsearch.add_KeyDown($dsearch_KeyDown)
	# Search / Filter from debug console
	$isearch = New-Object 'System.Windows.Forms.Button'
	$isearch.Location = New-Object System.Drawing.Point(223, 374)
	$isearch.Enabled = $false
	$isearch.Size = New-Object System.Drawing.Size(23, 23)
	$isearch.Image = [System.Drawing.Image]::FromFile("C:\Parxshell\Resources\search.ico")
	$isearch.FlatStyle = 'Flat'
	# Search function 
	$isearch.Add_Click({
			# Search
			if (($dsearch.Text).Length -gt 0)
			{
				Clear-host
				# Grab the output text buffer
				foreach ($line in ($debug_txt.ToString().Split('|').Replace([Environment]::NewLine, "") | Select-String -SimpleMatch -Pattern $dsearch.Text))
				{
					# Coloring the console depending on message level
					if ($line -like "*INFO*")
					{
						Write-Host "$line" -ForegroundColor Gray
					}
					elseif ($line -like "*SUCCESS*")
					{
						Write-Host "$line" -ForegroundColor Green
					}
					elseif ($line -like "*WARN*")
					{
						Write-Host "$line" -ForegroundColor Yellow
					}
					elseif ($line -like "*ERROR*")
					{
						Write-Host "$line" -ForegroundColor Red
					}
					elseif ($line -like "*REGISTRY*")
					{
						Write-Host "$line" -ForegroundColor Magenta
					}
					else
					{
						Write-Host "$line" -ForegroundColor White
					}
				}
			}
			# No search
			else
			{
				Clear-Host
				foreach ($line in ($debug_txt.ToString().Split('|')))
				{
					$line = $line.Replace([Environment]::NewLine, "")
					if ($line -like "*INFO*")
					{
						Write-Host $line -ForegroundColor Gray
					}
					elseif ($line -like "*SUCCESS*")
					{
						Write-Host $line -ForegroundColor Green
					}
					elseif ($line -like "*WARN*")
					{
						Write-Host $line -ForegroundColor Yellow
					}
					elseif ($line -like "*ERROR*")
					{
						Write-Host $line -ForegroundColor Red
					}
					elseif ($line -like "*REGISTRY*")
					{
						Write-Host $line -ForegroundColor Magenta
					}
					else
					{
						Write-Host $line -ForegroundColor White
					}
				}
				
			}
		})
	
	
	# Childform to control debug mode, new form is needed
	$ChildForm = New-Object system.Windows.Forms.Form
	$ChildForm.Size = new-object System.Drawing.Size @(300, 455)
	$ChildForm.BackColor = [System.Drawing.Color]::FromArgb(255, 44, 45, 42)
	$ChildForm.FormBorderStyle = 'FixedSingle'
	$ChildForm.ShowIcon = $False
	$ChildForm.StartPosition = "Manual"
	$ChildForm.Showintaskbar = $false
	$ChildForm.AutoScaleDimensions = New-Object System.Drawing.SizeF(6, 13)
	
	# Datatable to host all profiles for profile selection combobox
	$datatable = New-Object system.Data.DataTable
	$col1 = New-Object system.Data.DataColumn "Value", ([string])
	$col2 = New-Object system.Data.DataColumn "Text", ([string])
	$datatable.columns.add($col1)
	$datatable.columns.add($col2)
	# Debug controls creation
	$combobox1 = New-Object 'System.Windows.Forms.ComboBox'
	$radiobutton2 = New-Object 'System.Windows.Forms.RadioButton'
	$radiobutton1 = New-Object 'System.Windows.Forms.RadioButton'
	$button1 = New-Object 'System.Windows.Forms.Button'
	$button2 = New-Object 'System.Windows.Forms.Button'
	$button3 = New-Object 'System.Windows.Forms.Button'
	$button4 = New-Object 'System.Windows.Forms.Button'
	$button5 = New-Object 'System.Windows.Forms.Button'
	$button6 = New-Object 'System.Windows.Forms.Button'
	$button7 = New-Object 'System.Windows.Forms.Button'
	$button8 = New-Object 'System.Windows.Forms.Button'
	$button9 = New-Object 'System.Windows.Forms.Button'
	$button10 = New-Object 'System.Windows.Forms.Button'
	$textbox1 = New-Object 'System.Windows.Forms.TextBox'
	
	$childform.Controls.Add($radiobutton2)
	$childform.Controls.Add($radiobutton1)
	$childform.Controls.Add($button10)
	$childform.Controls.Add($button9)
	$childform.Controls.Add($button8)
	$childform.Controls.Add($button7)
	$childform.Controls.Add($button6)
	$childform.Controls.Add($button5)
	$childform.Controls.Add($button4)
	$childform.Controls.Add($button3)
	$childform.Controls.Add($button2)
	$childform.Controls.Add($button1)
	$childform.Controls.Add($textbox1)
	$childform.Controls.Add($combobox1)
	$ChildForm.Controls.Add($dsearch)
	$ChildForm.Controls.Add($isearch)
	# Debug controls properties
	$textbox1.ReadOnly = $true
	$textbox1.Location = New-Object System.Drawing.Point(12, 98)
	$textbox1.Size = New-Object System.Drawing.Size(230, 20)
	$textbox1.TabIndex = 2
	$textbox1.Text = "Choisir un profil du serveur :"
	$textbox1.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$textbox1.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	
	$combobox1.FormattingEnabled = $True
	$combobox1.Location = New-Object System.Drawing.Point(13, 124)
	$combobox1.Name = 'combobox1'
	$combobox1.Size = New-Object System.Drawing.Size(230, 23)
	$combobox1.TabIndex = 4
	$combobox1.Text = 'Changer de profil'
	$combobox1.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	
	# Only apply default value if at least one profile is available for host/user (event is fired at launch)
	$combobox1.Add_SelectedIndexChanged({
			if ($global:noprofile -eq $false)
			{
				$global:profileid = $combobox1.SelectedItem["Value"]
				$global:profilename = $combobox1.SelectedItem["Text"]
				$button2.Text = "Infos du profil chargé : $profilename"
				$button2.Enabled = $true
				$button3.Enabled = $true
				$button4.Enabled = $true
				$button5.Enabled = $true
				$button6.Enabled = $true
			}
			# Else, toggle noprofile variable for combobox to load chosen profile on next click
			else
			{
				$global:noprofile = $false
			}
			
			
		})
	
	# Simple debug mode button
	$radiobutton1.Location = New-Object System.Drawing.Point(13, 13)
	$radiobutton1.Fore
	$radiobutton1.Name = 'radiobutton1'
	$radiobutton1.Size = New-Object System.Drawing.Size(104, 24)
	$radiobutton1.TabIndex = 0
	$radiobutton1.TabStop = $True
	$radiobutton1.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$radiobutton1.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$radiobutton1.Text = 'Mode simple'
	$radiobutton1.UseVisualStyleBackColor = $True
	$radiobutton1.Add_CheckedChanged({
			if ($this.Checked -eq $true)
			{
				$global:debug_level = 0
				$host.Ui.WriteLine('Magenta', 'Black', "Mode débug simple. Uniquement les étapes et résultats de chaque opération seront affichés |")
			}
		})
	
	# Advanced debug mode button
	$radiobutton2.Location = New-Object System.Drawing.Point(123, 13)
	$radiobutton2.Name = 'radiobutton2'
	$radiobutton2.Size = New-Object System.Drawing.Size(104, 24)
	$radiobutton2.TabIndex = 1
	$radiobutton2.TabStop = $True
	$radiobutton2.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$radiobutton2.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$radiobutton2.Text = 'Mode avancé'
	$radiobutton2.UseVisualStyleBackColor = $True
	$radiobutton2.Add_CheckedChanged({
			if ($this.Checked -eq $true)
			{
				$global:debug_level = 1
				$host.Ui.WriteLine('Magenta', 'Black', "Mode débug avancé. Chacune des opérations vous proposera des options de diagnostic. |")
			}
	})
	
	# Client info button
	$button1.Location = New-Object System.Drawing.Point(13, 42)
	$button1.Name = 'button1'
	$button1.Size = New-Object System.Drawing.Size(230, 23)
	$button1.TabIndex = 0
	$button1.Text = 'Informations client'
	$button1.UseVisualStyleBackColor = $True
	$button1.FlatStyle = 'Flat'
	$button1.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button1.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button1.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Info
		})
	
	# Profile info button
	$button2.Location = New-Object System.Drawing.Point(13, 72)
	$button2.Name = 'button2'
	$button2.Size = New-Object System.Drawing.Size(230, 23)
	$button2.TabIndex = 1
	$button2.UseVisualStyleBackColor = $True
	$button2.FlatStyle = 'Flat'
	$button2.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button2.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button2.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Profile
		})
	
	# Load desktop button
	$button3.Location = New-Object System.Drawing.Point(12, 154)
	$button3.Name = 'button3'
	$button3.Size = New-Object System.Drawing.Size(230, 23)
	$button3.TabIndex = 5
	$button3.Text = 'Charger le bureau'
	$button3.UseVisualStyleBackColor = $True
	$button3.FlatStyle = 'Flat'
	$button3.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button3.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button3.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Shortcuts
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "SHORTCUTS"
		})
	
	# Load options button
	$button4.Location = New-Object System.Drawing.Point(13, 214)
	$button4.Name = 'button4'
	$button4.Size = New-Object System.Drawing.Size(230, 23)
	$button4.TabIndex = 6
	$button4.Text = 'Charger les options'
	$button4.UseVisualStyleBackColor = $True
	$button4.FlatStyle = 'Flat'
	$button4.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button4.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button4.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Options
		})
	
	# Load printers button
	$button5.Location = New-Object System.Drawing.Point(13, 244)
	$button5.Name = 'button5'
	$button5.Size = New-Object System.Drawing.Size(230, 23)
	$button5.TabIndex = 7
	$button5.Text = 'Charger les imprimantes'
	$button5.UseVisualStyleBackColor = $True
	$button5.FlatStyle = 'Flat'
	$button5.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button5.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button5.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Printers
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "PRINTERS"
		})
	
	# Load shares button
	$button6.Location = New-Object System.Drawing.Point(13, 274)
	$button6.Name = 'button6'
	$button6.Size = New-Object System.Drawing.Size(230, 23)
	$button6.TabIndex = 8
	$button6.Text = 'Charger les partages'
	$button6.UseVisualStyleBackColor = $True
	$button6.FlatStyle = 'Flat'
	$button6.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button6.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button6.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Shares
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "SHARES"
		})
	
	# Hidden button that will be invoked when launching debug mode from GUI
	$button7.Name = 'button7'
	$button7.TabIndex = 9
	$button7.Text = 'Activation du debug'
	$button7.Size = New-Object System.Drawing.Size(0, 0)
	$button7.Add_Click({
			# Here we launch the debug form and activate the special debug function
			$dText.Clear()
			$p = $rs.CreatePipeline({ [void]$dForm.ShowDialog() })
			$p.Input.Close()
			$p.InvokeAsync()
			. $PSScriptRoot\class-debug.ps1
			Enable-DebugMode -Debug
		})
	
	# Load all button
	$button8.Location = New-Object System.Drawing.Point(13, 304)
	$button8.Name = 'button6'
	$button8.Size = New-Object System.Drawing.Size(230, 23)
	$button8.TabIndex = 8
	$button8.Text = 'Tout charger (runspace)'
	$button8.UseVisualStyleBackColor = $True
	$button8.FlatStyle = 'Flat'
	$button8.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button8.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button8.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-All
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon			
		})
	
	# Load taskbar button
	$button9.Location = New-Object System.Drawing.Point(13, 184)
	$button9.Name = 'button6'
	$button9.Size = New-Object System.Drawing.Size(230, 23)
	$button9.TabIndex = 8
	$button9.Text = 'Charger la taskbar'
	$button9.UseVisualStyleBackColor = $True
	$button9.FlatStyle = 'Flat'
	$button9.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button9.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button9.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Taskbar
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "TASKBAR"
		})
	
	# Show last profile error button
	$button10.Location = New-Object System.Drawing.Point(13, 334)
	$button10.Name = 'button10'
	$button10.Size = New-Object System.Drawing.Size(230, 23)
	$button10.TabIndex = 8
	$button10.Text = 'Afficher les erreurs du profil'
	$button10.UseVisualStyleBackColor = $True
	$button10.FlatStyle = 'Flat'
	$button10.ForeColor = [System.Drawing.SystemColors]::ControlLight
	$button10.Font = [System.Drawing.Font]::new('Segoe UI', '8.25')
	$button10.Add_Click({
			. $PSScriptRoot\class-debug.ps1
			Debug-Error		
		})
	
	# Only enable Profile info button if a profile is loaded (ie default profile available for host/user)
	if (!([string]::IsNullOrWhitespace($profilename)))
	{
		$button2.Text = "Infos du profil chargé : $profilename"
	}
	else
	{
		$button2.Text = "Aucun profil chargé"
		$button3.Enabled = $false
		$button4.Enabled = $false
		$button5.Enabled = $false
		$button6.Enabled = $false
	}
	$ChildForm.Add_Closing({
			. $PSScriptRoot\class-debug.ps1
			Disable-DebugMode
			Set-ConsoleVisibility -show_hide 0
		})
	
	# Debug mode is ready, we will it only if needed, on a event click from main gui
	
	# init gui items
	$profileList = profileList $profiles $objContextMenu
	$saveMenu = saveMenu
	$loadMenu = loadMenu
	$logMenu = logMenu
	$infoMenu = infoMenu
	$exitMenu = exitMenu
	
	# add items if fullgui and if a profile is available    
	$objContextMenu.Items.Add($profileList) | Out-Null
	$objContextMenu.Items.Add($logMenu) | Out-Null
	if ($full_gui -eq 1 -and $profiles.code -eq 0) { $objContextMenu.Items.Add($saveMenu) | Out-Null }
	if ($profiles.code -eq 0) { $objContextMenu.Items.Add($loadMenu) | Out-Null }
	if ($full_gui -eq 1) { $objContextMenu.Items.Add($infoMenu) | Out-Null }
	if ($full_gui -eq 1) { $objContextMenu.Items.Add($exitMenu) | Out-Null }
	
	$colorTable.SetMenuForeColor($objContextMenu.Items, [System.Drawing.Color]::White)
	
	# Listen to printer, shares and taskbar error only if their autoloads are set
	if ($max_task -gt 0 -and $global:ServerError -eq $false)
	{
		Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon
	}
	$first_logon = $false
	# Cleaning
	#[System.GC]::Collect()	
	# Launch parxgui systray
	$appcontext = New-Object System.Windows.Forms.ApplicationContext
	$appcontext.Add_ThreadExit({
			Set-IconPosition -username $username -profileid $profileid -folder_cache $folder_cache -save_load "Save"
		})
	[void][System.Windows.Forms.Application]::Run($appcontext)
	
	
	
	# nothing can be written beyond this point : the showdialog get the process and keep it until it stop.
	# only write a "on exit" method
	
	
}



# exit Menu 
Function exitMenu ()
{
	
	# Create an Exit Menu Item
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "&Quitter"
	
	$menuItem.add_Click({
			log2 "See you space cowboy..." "warn"
			Get-CimInstance Win32_Process -Filter "name = 'powershell.exe'" | Select-Object  ProcessId, SessionID, CommandLine | Where-Object { $_.SessionId -eq $sessionid -and $_.CommandLine -notlike "*service.ps1*" } | ForEach-Object { Stop-Process -Id $_.ProcessId -force }
		})
	
	return $menuItem
}




# info (log, client, inventaire? logiciels?)
Function infoMenu ()
{
	
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "&Outils"
	

	
	$infoClient = New-Object System.Windows.Forms.ToolStripMenuItem
	$infoClient.Text = "Debug mode"
	
	$clearLog = New-Object System.Windows.Forms.ToolStripMenuItem
	$clearLog.Text = "Nettoyer Log"
	
	
	$reloadMenu = New-Object System.Windows.Forms.ToolStripMenuItem
	$reloadMenu.Text = "Redémarrer le client"
	
	#$menuItem.MenuItems.Add($refreshMenu) | Out-Null
	$menuItem.DropDownItems.Add($reloadMenu) | Out-Null
	$menuItem.DropDownItems.Add($clearLog) | Out-Null
	$menuItem.DropDownItems.Add($infoClient) | Out-Null
	
	$reloadMenu.Add_Click({
			Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID 9999 -EntryType Information -Message "$sessionid|$username|$userMSID" -Category 1 -RawData 10, 20
		})
	
	
	$clearLog.Add_Click({
			
			$box = new-object -comobject wscript.shell
			$intAnswer = $box.popup("Voulez vous vider le fichier des logs ?", 0, "Vider les logs", 4)
			If ($intAnswer -eq 6)
			{
				clear-log "C:\Parxshell\logs\parx.Log"
			}
		})
	
	
	
	$infoclient.add_Click({
			. c:\parxshell\class-debug.ps1
			Enable-DebugMode -Debug
			Clear-host
			Set-ConsoleVisibility -show_hide 1 -move -user $false
			$hash.host.Ui.WriteLine('Magenta', 'Black', "Bienvenue sur le mode debug de Parx ! |")			
			# Load debug combobox with all profiles available on parx server
			$all_profiles = restGET -uri "$address/all_profile_ids"
			foreach ($pro in $all_profiles.Message)
			{
				$name = ((get-profile $pro).Message | ConvertFrom-Json).Name
				$datarow1 = $datatable.NewRow()
				$datarow1.Value = $pro
				$datarow1.Text = $name
				$datatable.Rows.Add($datarow1)
				
			}
			
			$combobox1.ValueMember = "Value"
			$combobox1.DisplayMember = "Text"
			$combobox1.Datasource = $datatable
			$left = $where - 290
			$ChildForm.Location = New-Object System.Drawing.Point($left, 200)
			$childform.ShowDialog()
			
		})
	
	return $menuItem
}



# Log menu
Function logMenu ()
{
	
	
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "&Logs"
	
	$showLog = New-Object System.Windows.Forms.ToolStripMenuItem
	$showLog.Text = "Voir le fichier log"
	
	$showCurrentLog = New-Object System.Windows.Forms.ToolStripMenuItem
	$showCurrentLog.Text = "Voir le fichier log pour la session en cours"
	# define load desktop
	
	$showLog.Add_Click({
			try
			{
				$logfile = "C:\Parxshell\logs\parx.Log"
				Read-Log -logfile $logfile
				
			}
			catch
			{
				log2 "[GUI]" "Error while reading log $logfile : $($_.Exception.Message)"
			}
		})
	
	$showCurrentLog.Add_Click({
			try
			{
				$logfile = "C:\Parxshell\logs\current\session.Log"
				Read-Log -logfile $logfile
				
			}
			catch
			{
				log2 "[GUI]" "Error while reading log $logfile : $($_.Exception.Message)"
			}
		})
	
	$menuItem.DropDownItems.Add($showCurrentLog) | Out-Null
	$menuItem.DropDownItems.Add($showLog) | Out-Null		
	return $menuItem
}

# Load desktop, options, shares, printers
Function loadMenu ()
{
	
	
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "Charger"
	
	# define load functions
	$loadAll = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadAll.Text = "TOUT"
	
	$global:loadShortcut = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadShortcut.Text = "Bureau"
	
	$global:loadtaskbar = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadtaskbar.Text = "Taskbar"
	
	$global:loadOptions = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadOptions.Text = "Options"
		
	$global:loadShares = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadShares.Text = "Partages"
	
	$global:loadPrinters = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadPrinters.Text = "Imprimantes"
	
	$global:loadAssociations = New-Object System.Windows.Forms.ToolStripMenuItem
	$loadAssociations.Text = "Associations"
	
	
	$unloadAll = New-Object System.Windows.Forms.ToolStripMenuItem
	$unloadAll.Text = "Décharger le profil"
	
	$menuItem.DropDownItems.Add($loadAll) | Out-Null
	$menuItem.DropDownItems.Add($loadShortcut) | Out-Null
	$menuItem.DropDownItems.Add($loadtaskbar) | Out-Null	
	$menuItem.DropDownItems.Add($loadShares) | Out-Null
	$menuItem.DropDownItems.Add($loadPrinters) | Out-Null
	$menuItem.DropDownItems.Add($loadOptions) | Out-Null
	$menuItem.DropDownItems.Add($loadAssociations) | Out-Null
	if ($full_gui -eq 1)
	{
		$menuItem.DropDownItems.Add($unloadAll) | Out-Null
	}
	
	$unloadAll.Add_Click({
			foreach ($item in $objContextMenu.MenuItems)
			{
				$item.Enabled = $false
			}
			
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
			log2 "[GUI] Unloading profile for $username" "info"
			UnLoad-All
			log2 "[GUI] Profile unloaded for $username" "info"
			foreach ($item in $objContextMenu.MenuItems)
			{
				$item.Enabled = $true
			}
		})
		
	$loadAll.Add_Click({
			foreach ($item in $objContextMenu.MenuItems)
			{
				$item.Enabled = $false
			}
			
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
			log2 "[GUI] Reloading profile for $username" "info"
			Load-All
			log2 "[GUI] Profile reloaded for $username" "info"
			foreach ($item in $objContextMenu.MenuItems)
			{
				$item.Enabled = $true
			}
			# Welcome notification, if chosen so in config file
			if ($show_notification -eq 1)
			{
				$objNotifyIcon.BalloonTipText = "$Text"
				$objNotifyIcon.ShowBalloonTip($Miliseconds)
			}
			$objNotifyIcon.Text = "Profil : $profilename chargé !"
			
			
		})
	
	$loadShortcut.Add_Click({
			
			# Load shortcuts on user/public desktop
			$global:DebugPreference = 'SilentlyContinue'
			log2 "[GUI] Load Desktop" "info"
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"			
			$global:eventid = Get-Random -Minimum 1 -Maximum 9998
			draw-shortcut-list -profileid $profileid -autolaunch $autolaunch -debugpref $DebugPreference -folder_cache $folder_cache -username $username -switch_profile:$switch_profile -sessionid $sessionid -noDesktop $noDesktop
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "SHORTCUT"
			Autolaunch $autolaunch
			
		})
	
	$loadtaskbar.Add_Click({
			
			# Load taskbar on user desktop
			$global:DebugPreference = 'SilentlyContinue'
			log2 "[GUI] Load Taskbar" "info"
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
			$global:eventid = Get-Random -Minimum 1 -Maximum 9998
			set-taskbar-list $profileid -sessionid $sessionid -Debugmode $false
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "TASKBAR"
			
		})
	
	$loadOptions.Add_Click({
			# Load options
			$global:DebugPreference = 'SilentlyContinue'
			log2 "[GUI] Load Options" "info"
			$profile_cache = "C:\Users\$userName\appdata\roaming\Parx\$profileid.json"
			$upgrade_option, $options_cache = Set-ProfileCache $profileid $profile_cache
			$autoloads, $variables = Set-ParxVariables $switch_profile
			set-options-cache $options_cache -DebugPref $DebugPreference $false
			
		})
	
	$loadShares.Add_Click({
			#Load shares
			$global:DebugPreference = 'SilentlyContinue'
			log2 "[GUI] Load Shares" "info"
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
			$global:eventid = Get-Random -Minimum 1 -Maximum 9998
			set-shares-list $profileid -sessionid $sessionid -Debugmode $false
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "SHARES"
			
			
		})
	
	$loadPrinters.Add_Click({
			# Load Printers
			$global:DebugPreference = 'SilentlyContinue'
			log2 "[GUI] Load Printers" "info"
			$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
			$global:eventid = Get-Random -Minimum 1 -Maximum 9998
			set-printers-list $profileid -sessionid $sessionid -Debugmode $false
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "PRINTERS"
		})
	
	
	$loadAssociations.Add_Click({
			log2 "[GUI] Load Associations" "info"
			$global:eventid = Get-Random -Minimum 1 -Maximum 9998
			set-programs-list $profileid -debugmode $debugmode -debug_level $debug_level -sessionid $sessionid
			Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -Single -module "ASSOCIATIONS"
		})
	
	return $menuItem
}


# Save desktop, add host
Function saveMenu ()
{
	
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "&Sauve"
	
	
	# define load desktop
	$saveDesk = New-Object System.Windows.Forms.ToolStripMenuItem
	$saveDesk.Text = "Bureau"
	
	$menuItem.DropDownItems.Add($saveDesk) | Out-Null
	
	
	# actions
	$saveDesk.Add_Click({
			
			$box = new-object -comobject wscript.shell
			$intAnswer = $box.popup("Voulez vous sauvegarder les raccourcis du bureau ?", 0, "Sauvegarde bureau", 4)
			If ($intAnswer -eq 6)
			{
				Save-Desktop
			}
		})
	
	return $menuItem
}




# choose a profile in a list
Function profileList ($profiles, $objContextMenu)
{
	
	
	$menuItem = New-Object System.Windows.Forms.ToolStripMenuItem
	$menuItem.Text = "Profils"
	$menuItem.Name = "Profils"
	$refreshMenu = New-Object System.Windows.Forms.ToolStripMenuItem
	$refreshMenu.Text = "Rafraichir"
	
	$menuItem.DropDownItems.Add($refreshMenu) | Out-Null
	
	$refreshMenu.Add_Click({
			$profiles = get-profiles $env:COMPUTERNAME $userName
			$profileList = profileList $profiles
			$objContextMenu.Items.removeAt(0)
			$menuProfiles = $objContextMenu.Items.Insert(0, $profileList) | Out-Null
			$colorTable.SetMenuForeColor($objContextMenu.Items, [System.Drawing.Color]::White)			
		})
	
	if ($profiles.code -eq 0)
	{
		
		$list = ConvertFrom-JSON $profiles.message
		
		foreach ($profile in $list)
		{
			if ($profile.name)
			{
				
				# define load desktop
				$item = New-Object System.Windows.Forms.ToolStripMenuItem
				$item.Text = $profile.name
				$item.Name = $profile.id
				
				$menuItem.DropDownItems.Add($item) | Out-Null
				
				# Select the default profile
				if ($item.Name -eq $profileid) { $item.checked = $true }
				
				# actions
				$item.Add_Click({
						$clickedItem = $this												
						$parentControl = $clickedItem.Owner
						if ($this.Name -ne $profileid)
						{
							foreach ($item in $objContextMenu.Items)
							{
								$item.Enabled = $false
							}
							foreach ($itm in $parentControl.Items) { $itm.checked = $false }
							$this.checked = $true
							$profileName = $this.Text
							$objNotifyIcon.Text = "ParxShell v$version `rProfil: $profileName"
							$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_orange.ico"
							log2 "[GUI] Reloading profile for $username" "info"
							# Save actual desktop icon position
							Set-IconPosition -username $username -profileid $profileid -folder_cache $folder_cache -save_load "Save"
							# new active profile in client							
							$global:profileid = $this.Name
							# As we are switching profile, toggle the switch_profile variable to true so that options will be forced to reapply
							$global:switch_profile = $true
							$global:first_logon = $false
							Load-All -switch
							log2 "[GUI] Profile reloaded for $username" "info"
							$switch_profile = $false
							foreach ($item in $objContextMenu.Items)
							{
								$item.Enabled = $true
							}
							# Welcome notification, if chosen so in config file
							if ($show_notification -eq 1)
							{
								$objNotifyIcon.BalloonTipText = "$Text"
								$objNotifyIcon.ShowBalloonTip($Miliseconds)
							}
						}
						
						
					})
				
			}
		}
		
	}
	else
	{
		
		# define load desktop
		$item = New-Object System.Windows.Forms.ToolStripMenuItem
		$item.Text = "Pas de Profil"
		
		$menuItem.DropDownItems.Add($item) | Out-Null
	}
	
	
	
	return $menuItem
}

# Function for load all button from GUI, and switch profile from GUI
function Load-All
{
	[CmdletBinding()]
	Param (
		[switch]$switch
	)
	# We must enable all autoloads 
	$global:autoload_options = 1
	$global:autoload_printers = 1
	$global:autoload_shares = 1
	$global:autoload_desktop = 1
	$global:autoload_taskbar = 1
	$global:autoload_associations = 1
	# Disable debug mode if it was previously enabled from console
	$global:DebugPreference = 'SilentlyContinue'
	# Change logontype so that explorer is refreshed when options are applied
	$logontype = "Switch"
	# If switching profile (multiples profiles available), set chosen profile in cache in order to load it by default at next logon
	if ($switch)
	{
		$folder_cache = "C:\Users\$userName\appdata\roaming\Parx\"
		$profileid | Set-Content "$folder_cache\last_profile.txt"
		$profileName | Add-Content "$folder_cache\last_profile.txt"
		Update-ServerProfile
	}
	# Cache profile parameters
	$profile_cache = "C:\Users\$userName\appdata\roaming\Parx\$profileid.json"
	$upgrade_option, $options_cache = Set-ProfileCache $profileid $profile_cache
	set-options-cache $options_cache	
	$autoloads, $variables = Set-ParxVariables $switch_profile $first_logon $noDesktop
	$max_task = ($autoloads.Values | Measure-Object -Sum).Sum
	New-Runspace -Threads $max_task -Autoloads $autoloads -Autolaunch $autolaunch -variables $variables -profileid $profileid
	Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon
	Autolaunch $autolaunch
	
}

# Function for unloading a profile 
function UnLoad-All
{
	# We must enable all autoloads 
	$global:autoload_options = 1
	$global:autoload_printers = 1
	$global:autoload_shares = 1
	$global:autoload_desktop = 1
	$global:autoload_taskbar = 1
	$global:autoload_associations = 1
	# Disable debug mode if it was previously enabled from console
	$global:DebugPreference = 'SilentlyContinue'
	# Change logontype so that explorer is refreshed when options are applied
	# Cache profile parameters, profileid 9999 is a blank profile that will be used to unload all parx modifications
	$options_cache = get-content -Path "$PSScriptRoot\Resources\default.json"| ConvertFrom-Json
	# If not switching profile, only apply new options that can be found on server
	set-options-cache $options_cache $false $true
	$autoloads, $variables = Set-ParxVariables $switch_profile $false $false
	$max_task = ($autoloads.Values | Measure-Object -Sum).Sum
	New-Runspace -Threads $max_task -Autoloads $autoloads -Autolaunch $autolaunch -variables $variables -unloadProfile
	Get-ParxError -Eventid $eventid -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -clear	
}