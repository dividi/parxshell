﻿# get the printers list from the db
Function get-printers-list ($profileid)
{
	
	return restGET "$address/rest_printers/$profileid"
}

# This function will be injected as encoded command to be run as the current user
#Delete ALL PARX printers if no printers are in profile
function Delete-Printers
{
	$debugmode = "DEBUG"
	$debug_level = "LEVEL"
	$force = "TRUE/FALSE"
	$eventid = "EVENTID"
	
	# Set the debug console if debug mode is enabled
	if ($debugmode -eq $true)
	{
		$DebugPreference = "Continue"
		
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$host.ui.rawui.windowtitle = "Parxshell debug"			
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
	}
	# Delete
	function Del-Printer ($connection_name)
	{
		#.DESCRIPTION
		# Printer isn't the profile anymore, we're going to delete it
	if ($debug_level -eq 1)
		{
		(Get-Help Del-Printer).Description
		(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
	}
	Remove-Printer -Name $connection_name
	}
	# Check for mounted printers
	if (Test-Path "Registry::\HKEY_CURRENT_USER\Printers\Connections")
	{
		Foreach ($printers in (Get-childItem -Path "Registry::\HKEY_CURRENT_USER\Printers\Connections" | Get-ItemProperty))
		{
			# Check for PARX tag, so that only parx printers will be deleted
			if ($printers.'[PARX]' -eq 1 -or $force -eq $true)
			{
				$connection_name = ($printers.PSChildName).Replace(',', '\')
				Del-Printer $connection_name
				# Check if printer was successfully deleted
				if (!(get-printer -Name $connection_name -ErrorAction SilentlyContinue))
				{
					$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
					$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
					# log level must be less or equal than log events params
					try
					{
						# Wait until the mutex is acquired
						$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
						# Write to the log file
						Add-Content -Path "c:\Parxshell\Logs\parx.log" "$now [SUCCESS] [PRINTERS] $connection_name was uninstalled."
						if ($DebugPreference -eq 'Continue')
						{
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[PRINTERS] [SUCCESS] $connection_name was uninstalled. |" -Category 1 -RawData 10, 20
						}
					}
					finally
					{
						# Release the mutex
						$mutex.ReleaseMutex()
						$mutex.Dispose()
					}
					
					
				}
			}
		}
	}
	if ($force -eq $false)
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Printer Done" -Category 1 -RawData 10, 20
		exit
	}
	
}

# This function will be injected as encoded command to be run as the current user
# Set printers that are found in profile
Function Set-Printers
{
	# We must declare again the log2 function as it can't be reached from user session
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"		
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		# log level must be less or equal than log events params
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction SilentlyContinue
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
	
	# Mount printers
	Function Mount-Printer ($target, $name, $default)
	{
		#.DESCRIPTION
		# New printer is found, we're going to install it and check if installation was successful		
		# For delayed printers (offline), get target as name
		if (!($name))
		{
			$name = $target
		}
		
		log2 "[PRINTERS] $name not installed, mounting" "info"		
		# Mount printer and set default if needed
		try
		{
			$ws_net = New-Object -COM WScript.Network
			$ws_net.AddWindowsPrinterConnection("$target")
		}
		catch  {
			if ($_.Exception.Message -like "*valide*")
			{
				# Printer doesn't exist on the printer server	
				$msg = "[PRINTERS] $($printer.Name) ($target) not found on $server_ip, check printer parameters on profile !"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				log2 $msg "error"
				continue
				
			}
			elseif ($_.Exception.Message -like "*DENIED*")
			{
				# User doesn't have rights to connect to the printer server	
				$msg = "[PRINTERS] $env:username don't have rights to access $($printer.Name) ($target), check printer parameters on server if you want to grant access to the user !"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				log2 $msg "error"
				continue
			}
			else
			{
				$msg = "[PRINTERS] $name (($target) wasn't installed, error $($_.Exception.Message), check the profile printer parameters."
				log2 $msg "error"
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
				continue
			}
			
		}
		
		if ($default -eq 1)
		{
			# Enable Parx default printer management
			if ((Test-Path -LiteralPath "HKCU:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows") -ne $true) { New-Item "HKCU:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows" -force -ea SilentlyContinue };
			New-ItemProperty -LiteralPath 'HKCU:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows' -Name 'LegacyDefaultPrinterMode' -Value 1 -PropertyType DWord -Force -ea SilentlyContinue;
			(New-Object -ComObject WScript.Network).SetDefaultPrinter($target)
			log2 "[PRINTERS] $name was set as default printer" "success"
		}
		# Check if printer was successfully mounted, and tag its registry key with PARX, for further deletion
		$tag = (Get-Printer | Where-Object { $_.Sharename -eq ($target -replace '.*\\') })
		if ($tag)
		{
			$printer_key = "Registry::\HKEY_CURRENT_USER\Printers\Connections\$(($tag.Name).Replace('\', ','))"			
			if ((Test-Path -LiteralPath "$printer_key") -ne $true) { New-Item "$printer_key" -force -ea SilentlyContinue };		
			$msg = "[PRINTERS] $name is installed."
			log2 $msg "success"
			if ($DebugPreference -eq 'Continue')
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
			}
			New-ItemProperty -path $printer_key -name "[PARX]" -type DWORD -value 1 -force | Out-null
			New-ItemProperty -path $printer_key -name "ShareName" -value $target -force | Out-null
			
		}	
	}
	# Delete
	function Del-Printer ($connection_name)
	{
		#.DESCRIPTION
		# Printer isn't the profile anymore, we're going to delete it
		Remove-Printer -Name $connection_name
	}
	
	$debugmode = "DEBUG"
	$eventid = "EVENTID"
	$debug_level = "LEVEL"
	if ($debugmode -eq $true)
	{
		$DebugPreference = "Continue"
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$host.ui.rawui.windowtitle = "Parxshell debug"
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
	}
	
	# Array for offline printers
	$delay_offline = @()
	$json = Invoke-RestMethod -Uri "IPPARX/rest_printers/PROFILPARX" -Headers @{ Accept = "application/json" }	
	
	if ($json.Code -eq 0)
	{
		$list = ConvertFrom-JSON $json.message
		# Remove printers that aren't in the profile anymore, checking its PARX key to remove only PARX printerse
		if (Test-Path "Registry::\HKEY_CURRENT_USER\Printers\Connections")
		{
			# We're going to compare the sharename of already mounted printers with the ones in the profile
			Foreach ($printers in (Get-childItem -Path "Registry::\HKEY_CURRENT_USER\Printers\Connections" | Get-ItemProperty))
			{
				$connection_name = ($printers.PSChildName).Replace(',', '\')
				# Printer not anymore in profile, unmount it
				if ($printers.ShareName -notin ($list.Target) -and $printers.'[PARX]' -eq 1)
				{
					$msg = "[PRINTERS] $connection_name was removed from profile. Deleting printer."
					log2 $msg "warn"
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
					}
					Del-Printer $connection_name
					# Check if printer was successfully unmounted
					if (!(get-printer -Name $connection_name -ErrorAction SilentlyContinue))
					{
						$msg = "[PRINTERS] $connection_name was uninstalled."
						log2  "success"
						if ($DebugPreference -eq 'Continue')
						{
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						}
					}
				}
			}
		}
		
		# Process printers in profile
		foreach ($printer in $list)
		{
			$server_ip = [string]::join("\\", ($printer.target).Split("\")[1 .. 2])
			$share_name = $printer.Target -replace '.*\\'
			# If printer exists on the printer server
			if ((Get-Printer -ComputerName "$server_ip" | Where-Object { $_.ShareName -eq $share_name }) -ne $null)
			{
				
				# Check if printer is already mounted or not
				if ($share_name -notin ( get-printer | Select-object Name, ShareName).ShareName)
				{
					# Add offline printers in array to be processed after online ones (prevent script slowdown)
					if ((Get-printer -ComputerName $server_ip | Where-Object { $_.ShareName -eq $share_name }).PrinterStatus -eq "Offline")
					{
						$msg = "[PRINTERS] $($printer.Name) is offline, delaying mount"
						log2 $msg "warn"
						if ($DebugPreference -eq 'Continue')
						{
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
						}
						$delay_offline += "$($printer.Target)"
						continue
					}
					# Mount online printers
					else
					{
						Mount-Printer "$($printer.Target)" $printer.Name $printer.pivot.default
					}
					
				}
				else
				{
					$msg = "[PRINTERS] $($printer.Name) already installed."
					log2 $msg "info"
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
					}
				}
			}
			# Try to mount printers not returned by Get-Printers, an exception will be thrown and logged
			else
			{
				Mount-Printer "$($printer.Target)" $printer.Name $printer.pivot.default
			}
		}
		
		# Process offline printers
		If ($delay_offline.Count -ne 0)
		{
			log2 "[PRINTERS] Processing delayed mounts" "info"			
			Foreach ($printer in $delay_offline)
			{
				Mount-Printer "$printer"
			}
		}
		
	}
	Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Printer Done" -Category 1 -RawData 10, 20
	exit
}

# Prepare the command to be used as Current user
function Manage-Printers
{
	[CmdletBinding()]
	Param (
		[switch]$Remove_all,
		[boolean]$Force,
		[boolean]$debugmode,
		[int]$debug_level,
		$sessionID
	)
	Try
	{
		If ($debug_level -eq 1)
		{
			$visible = $true
		}
		else
		{
			$visible = $false
		}
		
		# We have for the user session to exists to launch these functions within user session
		Do
		{
			Start-Sleep -Milliseconds 100
		}
		While ((Get-Process -Name Logonui -ErrorAction SilentlyContinue).SI -eq $sessionID)
	
		# If no printers is in profile, remove all PARX Printers
		If ($remove_all)
		{
			# If First_logon since parx upgrade, we have to clear all printers to mount them again with PARX tag
			if ($Force -eq $true)
			{
				$def = (Get-ChildItem "Function:Delete-Printers", "Function:Set-Printers").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
			else
			{
				$def = (Get-ChildItem "Function:Delete-Printers").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
			
		}
		# Mount printers
		else
		{
			$def = (Get-ChildItem "Function:Set-Printers").Definition
			$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
		}
		$block = [scriptblock]::Create($def)		
		invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -Nowait
		
		
	}
	catch
	{
		$err = $_.Exception.Message
		log2 "[PRINTERS] Load PRINTER : $err" "error"
		
	}
	
}


# Prepare printers functions
Function set-printers-list {
	param (
		$profileid,
		[boolean]$Debugmode,
		[int]$Debug_level,
		$sessionid,
		[switch]$unloadProfile
	)
	# If first time since parx upgrade, remove all printers and mount them again with PARX tag
	if ($first_logon -eq $true)
	{
		log2 "[PRINTERS] First logon since parx upgrade. Remaking printers." "warn"
		Manage-Printers -remove_all -force $true -Debugmode $Debugmode -debug_level $Debug_level -sessionID $sessionid
	}
	else
	{
		$json = get-printers-list $profileid
		
		if ($json.code -eq 0)
		{
			Manage-Printers -Debugmode $Debugmode -debug_level $Debug_level -sessionID $sessionid 
		}
		elseif ($json.code -ne 0 -or $unloadProfile)
		{
			log2 "[PRINTERS] No printer for this profile, checking if [PARX] printers are to be removed" "warn"
			Manage-Printers -remove_all -Debugmode $Debugmode -debug_level $Debug_level -sessionID $sessionid
		} 
	}
	
}

