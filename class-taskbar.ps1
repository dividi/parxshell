﻿# get the shortcuts list from the db (call)
Function get-taskbar-list ($profileid)
{
	return restGET "$address/profile_taskbar_shortcuts/$profileid"
}

function Set-Log
{
	# We must declare again the log2 function as it can't be reached from user session
	Function log2 ($value, $type)
	{
		$logfile = "C:\parxshell\Logs\parx.log"
		$currentSession = "C:\parxshell\Logs\Current\session.log"
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
		$lockFile = $LogFile + ".lock"
		# log level must be less or equal than log events params
		try
		{
			# Wait until the mutex is acquired
			$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
			# Write to the log file
			Add-Content $logfile, $currentSession "$now [$($type.ToUpper())] $value" -Force -Encoding UTF8 -ErrorAction SilentlyContinue
		}
		finally
		{
			# Release the mutex
			$mutex.ReleaseMutex()
			$mutex.Dispose()
		}
	}
}

# This function will be injected as encoded command to be run as the current user
# Delete PARX taskbar shortcut if profile has no taskbar shortcut
function Delete-Taskbar
{
	function Unpin ($path, $name)
	{
		#.DESCRIPTION
		# As no taskbar item is in the porfil, unpining all parx items one by one
		if ($DebugPreference -eq 'Continue')
		{
			(Get-Help Unpin).Description | out-Host
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		(New-Object -ComObject shell.application).Namespace($path).parsename("$name").verbs() | Where-Object { $_.Name.replace('&', '') -match 'Désépingler de la barre des tâches' } | ForEach-Object { $_.DoIt(); $exec = $true }
		
	}
	
	$debugmode = "DEBUG"
	$debug_level = "LEVEL"
	$force = "TRUE/FALSE"
	$eventid = "EVENTID"	
	# Set the console if debug mode is enabled
	if ($debugmode -eq $true)
	{
		if ($debug_level -eq 1)
		{
			[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
			$null = . C:\parxshell\class-c.ps1
			$null = . C:\parxshell\class-debug.ps1
			$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
			$null = Set-ConsoleEdit -set $true
			$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
		}
		$DebugPreference = "Continue"
	}
	# Array for existing shortcut target
	$path = "c:\users\$env:USERNAME\appdata\Roaming\microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar"
	$pinned_shortcuts = Get-Childitem -path $path
	# Remove shortcuts that aren't in the profile anymore (or are disabled), checking its PARX alternate data stream to remove only PARX pinned shortcuts	
	if ($DebugPreference -eq 'Continue')
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] Checking for PARX taskbar shortcut to be deleted |" -Category 1 -RawData 10, 20
	}
	Foreach ($shortcut in $pinned_shortcuts)
	{
		# If PARX flag is found, unpin
		if ((Get-content -Path $shortcut.FullName -Stream PARX -ErrorAction SilentlyContinue) -eq "[PARX]")
		{
			$name = $shortcut.Name
			if ($DebugPreference -eq 'Continue')
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] PARX taskbar shortcut found ($name), deleting it |" -Category 1 -RawData 10, 20
			}
			# Unpin
			Unpin $path $name
			$msg = "[TASKBAR] $name deleted"
			log2 $msg "success"
			if ($DebugPreference -eq 'Continue')
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
			}
		}
	}
	if ($force -eq $false)
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Taskbar Done" -Category 1 -RawData 10, 20
		log2 "[TASKBAR] Taskbar done" "info"
		exit
	}
}

# For first logon after big parx update, we need to flag existing pinned items that are present on profile with a PARX tag
function Mark-Taskbar
{
	function Mark-Taskbar
	{
		# Set Parx tag on file alternate data stream
		function Set-ADS ($path, $file)
		{
			#.DESCRIPTION
			# We're going to set a [PARX] tag in the file alternate data stream
			
			set-content "$path\$file" -Stream PARX -Value "[PARX]"
		}
		
		
		$debugmode = "DEBUG"
		$debug_level = "LEVEL"
		$force = "TRUE/FALSE"
		$eventid = "EVENTID"
		
		if ($debugmode -eq $true)
		{
			if ($debug_level -eq 1)
			{
				[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")
				$null = . C:\parxshell\class-c.ps1
				$null = . C:\parxshell\class-debug.ps1
				$null = Set-ConsoleVisibility -show_hide 1 -move -user $true
				$null = Set-ConsoleEdit -set $true
				$host.Ui.WriteLine('White', 'Black', "Debug console from user $env:username session ")
			}
			$DebugPreference = "Continue"
		}
		# Path to user pinned item
		$path = "c:\users\$env:USERNAME\appdata\Roaming\microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar"
		$json = Invoke-RestMethod -Uri "IPPARX/profile_taskbar_shortcuts/PROFILPARX" -Headers @{ Accept = "application/json" }
		
		# Remove windows store 	
		((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() |
			?{ $_.Name -eq "Microsoft Store" }).Verbs() | ?{ $_.Name.replace('&', '') -match 'Désépingler de la barre des tâches' } | %{ $_.DoIt() }
		
		
		# Get taskbar shortcut
		$list = ConvertFrom-Json $json.Message
		$msg = "[TASKBAR] [TASK] First logon since parx update, checking for existing pinned item which are in profile to set a [PARX] tag on them"
		log2 $msg "info"
		# Get all pinned shortcut in user env profile
		$sh = New-Object -COM WScript.Shell
		$targets = Get-ChildItem -Path $path | ForEach-Object {
			# We must get exe target from user existing pinned item
			$target = $sh.CreateShortcut($_.fullname).TargetPath
			# If existing pinned item is in profile, flag [PARX]
			if ($target -in $list.Target)
			{
				# Grab the file description from exe, as Windows uses it to create pinned item shorcut name (*.lnk)
				$desc = (Get-ChildItem -Path $target).VersionInfo.FileDescription
				# If description exists, shortcut name is description.lnk
				if ($desc -ne "")
				{
					$file = $desc + ".lnk"
				}
				# if no description, shortcut name is target.lnk (ex "C:\test\toto.exe" shortcut name will be "toto.lnk")
				else
				{
					$file = [io.path]::GetFileNameWithoutExtension($target)
				}
				# Set [PARX] flag in alternate data stream
				Set-ADS $path $file
			}
		}
	}
	
	log2 "[TASKBAR] Setting parx tag on existing taskbar shortcut done" "warn"
}

# This function will be injected as encoded command to be run as the current user
Function Set-Taskbar
{
	function Set-Pin ($pin)
	{
		#.DESCRIPTION
		# Pinned item target is valid, pinutil.exe will pin it to user taskbar with provided arguments showed below
		if ($debug_level -eq 1)
		{
			(Get-Help Set-Pin).Description | out-Host
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		Start-Process -WindowStyle Hidden -FilePath "C:\parxshell\bin\PinUtil.exe" -ArgumentList $pin -Wait
	}
	function Set-Arguments ($file, $target, $arguments)
	{
		try
		{
			$shell = New-Object -ComObject WScript.Shell
			$shortcut = $shell.CreateShortcut($file)
			$shortcut.TargetPath = $target
			$shortcut.Arguments = $arguments
			$shortcut.Save()
			Log2 "[TASKBAND] Arguments on $file for $target with $arguments set" "info"
		}
		catch
		{
			log2 "ERROR TASK $($_.Exception.Message)" "error"
		}
	}
	
	function Unpin ($path, $name)
	{
		#.DESCRIPTION
		# Item isn't in the profile anymore, unpinning it
		if ($debug_level -eq 1)
		{
			(Get-Help Unpin).Description | out-Host
			(Get-PsCallstack)[0] | Select-Object Command, Location, InvocationInfo | Debug-Script
		}
		try
		{
			(New-Object -ComObject shell.application).Namespace($path).parsename("$name").verbs() | Where-Object { $_.Name.replace('&', '') -match 'Désépingler de la barre des tâches' } | ForEach-Object { $_.DoIt(); $exec = $true }
		}
		catch { }
		
	}
	
	$debugmode = "DEBUG"
	$eventid = "EVENTID"
	
	# Array for existing shortcut target
	$json = Invoke-RestMethod -Uri "IPPARX/profile_taskbar_shortcuts/PROFILPARX" -Headers @{ Accept = "application/json" }
	$path = "c:\users\$env:USERNAME\appdata\Roaming\microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar"
	
	if ($json.Code -eq 0)
	{
		# Mark Taskbar if first_logon
		if (get-command Mark-Taskbar -ErrorAction SilentlyContinue)
		{
			Mark-Taskbar
		}
		
		$list = ConvertFrom-JSON $json.message
		$sh = New-Object -COM WScript.Shell
		$existing_shortcuts = Get-Childitem -path $path
		# Remove shortcuts that aren't in the profile anymore (or are disabled), checking its PARX alternate data stream to remove only PARX pinned shortcuts
		$msg = "[TASKBAR] Checking for taskbar shortcut to be removed if not anymore in profile"
		log2 $msg "info"
		
		if ($DebugPreference -eq 'Continue')
		{
			Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
		}
		Foreach ($shortcut in $existing_shortcuts)
		{
			# Check if file alternate data stream is set to [PARX]
			if ((Get-content -Path $shortcut.FullName -Stream PARX -ErrorAction SilentlyContinue) -eq "[PARX]")
			{
				# Grab shortcut target to check if its target is in profile taskbar shortcuts
				$target = $sh.CreateShortcut($shortcut.fullname).TargetPath
				if ($target -notin $list.Target)
				{
					$msg = "[TASKBAR] $($shortcut.Name) was removed from profile, deleting it"
					log2 $msg "info"
					
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message $msg -Category 1 -RawData 10, 20
					}
					$name = $shortcut.Name
					# Unpin
					Unpin $path $name
					if ($DebugPreference -eq 'Continue')
					{
						Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [SUCCESS] $($shortcut.Name) removed |" -Category 1 -RawData 10, 20
					}
				}
			}
		}
		
		# Sets parx shortcuts
		$msg = "[TASKBAR] Checking for new shortcut to pin"
		log2 $msg "info"
		
		if ($DebugPreference -eq 'Continue')
		{
			Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "$msg |" -Category 1 -RawData 10, 20
		}
		$pinned_shortcuts = Get-Childitem -path $path
		$pinned_target = @()
		# We must get all existing taskbar shortcuts target before all
		Foreach ($shortcut in $pinned_shortcuts)
		{
			if ((Get-content -Path $shortcut.FullName -Stream PARX -ErrorAction SilentlyContinue) -eq "[PARX]")
			{
				$pinned_target += $sh.CreateShortcut($shortcut.fullname).TargetPath
			}
			
		}
		# Process profile taskbar shortcut
		Foreach ($shortcut in $list)
		{
			$target = $shortcut.Target -replace '"', ""
			# If profil shortcut isn't already pinned, pin it
			if ($target -notin $pinned_target)
			{
				$msg = "[TASKBAR] $target found, pinning it"
				log2 $msg "info"
				$args = @"
TaskBar "$target"
"@
				Try
				{
					# Check if shortcut is a network program, as they can't be pinned
					if (($target).StartsWith("\\"))
					{
						log2 "[TASKBAR] Can't pin $($shortcut.name) for $target, as network shortcut can't be pinned, skipping. You should remove it from profile or change it for a desktop shortcut." "warn"
						continue
					}
					# Check if shortcut target is valid
					If (Test-Path -Path $target)
					{
						# Grab target description, for pinned shortcut are automatically named by windows depending they have a description or not
						$desc = (Get-ChildItem -Path $target).VersionInfo.FileDescription
						if ($desc -ne "")
						{
							if ($DebugPreference -eq 'Continue')
							{
								Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] Description for $target found : $desc |" -Category 1 -RawData 10, 20
							}
							$file = $desc + ".lnk"
							
							
						}
						# if no description, shortcut name is target.lnk (ex "C:\test\toto.exe" shortcut name will be "toto.lnk")
						else
						{
							$file = [io.path]::GetFileNameWithoutExtension($target)
							if ($DebugPreference -eq 'Continue')
							{
								Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[TASKBAR] [INFO] No Description for $target was found, windows will use its file ($file) name instead. |" -Category 1 -RawData 10, 20
							}
							
							
						}
						# If target is already present, but without a PARX flag, unpin it and mark it as PARX item						
						# Unpin						
						Unpin $path $file
						Remove-Item -Path "$path\$file" -Force -ErrorAction SilentlyContinue
						# We must invoke pin util within user session						
						Set-Pin $args
						# Set-Pin needed the $file as set before, further actions will need the extensions, so we check for its absence and add it if needed
						if ($file -notlike "*.lnk*")
						{
							$file = $file + ".lnk"
						}
						# If shortcut has some arguments, we need to add them now 
						if (![string]::IsNullOrEmpty($shortcut.arguments))
						{
							Set-Arguments "$path\$file" $shortcut.target $shortcut.arguments
						}
																	
									
						# Set [PARX] flag in alternate data stream						
						set-content "$path\$file" -Stream PARX -Value "[PARX]"
						
					}
					# Shortcut target not valid
					else
					{
						$msg = "[TASKBAR] [ERROR] Target $target for Shortcut $($shortcut.Name) not found"
						log2 $msg "error"
						if ($DebugPreference -eq 'Continue')
						{
							Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Error -Message "$msg |" -Category 1 -RawData 10, 20
						}
					}
				}
				Catch
				{
					$msg = "[TASKBAR] [ERROR] : $($_.Exception.Message)"
					log2 $msg "error"
					
				}
			}
			# Shortcut already pinned
			else
			{
				$msg = "[TASKBAR] $target already pinned"
				log2 $msg "info"
				
			}
		}
		$msg = "[TASKBAR] Taskbar done"
		Log2 $msg "info"
	}
	Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Warning -Message "Taskbar done" -Category 1 -RawData 10, 20
	exit
	
}

# Prepare the command to be used as Current user
function Manage-Taskbar
{
	[CmdletBinding()]
	Param (
		[switch]$Remove_all,
		[boolean]$Force,
		[boolean]$debugmode,
		[int]$debug_level,
		$sessionID
	)
	Try
	{
		If ($debug_level -eq 1)
		{
			$visible = $true
		}
		else
		{
			$visible = $false
		}
		# We have for the session to exists to launch these functions within user session
		Do
		{
			Start-Sleep -Milliseconds 100
		}
		While ((Get-Process -Name Logonui -ErrorAction SilentlyContinue).SI -eq $sessionID)
		
		# If no taskbar shortcut is in profile, remove all PARX taskbar shorcuts
		If ($remove_all)
		{
			# If First_logon since parx upgrade, we have to clear all existing taskbar shorcut and mount new item with PARX tag
			if ($Force -eq $true)
			{
				$def = (Get-ChildItem "Function:Set-Log", "Function:Mark-Taskbar", "Function:Set-Taskbar").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
			else
			{
				$def = (Get-ChildItem "Function:Set-Log", "Function:Delete-Taskbar").Definition
				$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("TRUE/FALSE", $force).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
			}
		}
		# Set taskbar shortcut
		else
		{
			$def = (Get-ChildItem "Function:Set-Log", "Function:Set-Taskbar").Definition
			$def = $def.Replace("IPPARX", $address).Replace("PROFILPARX", $profileid).Replace("DEBUG", $debugmode).Replace("LEVEL", $debug_level).Replace("EVENTID", $eventid)
		}
		$block = [scriptblock]::Create($def)
		invoke-ascurrent_user -UseWindowsPowerShell -Visible:$visible -scriptblock $block -Nowait
	}
	catch
	{
		$err = $_.Exception.Message
		log2 "[TASKBAR] Load taskbar : $err" "error"
	}
}


# Prepare taskbar functions
Function set-taskbar-list
{
	param (
		$profileid,
		[boolean]$Debugmode,
		[int]$debug_level,
		$sessionid,
		[switch]$unloadProfile
	)
	$json = get-taskbar-list ($profileid)
	
	# At least one shortcut in profile
	if ($json.code -eq 0 -or $first_logon -eq $true)
	{
		# If first time since parx upgrade, flag all existing pinned item with [PARX] in alternate data stream, if they exists on parx profile
		if ($first_logon -eq $true)
		{
			log2 "[TASKBAR] First logon since parx upgrade. Flagging all existing pinned items with [PARX] if they exists on profile." "warn"
			Manage-Taskbar -remove_all -force $true -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
		}
		else
		{
			Manage-Taskbar -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
		}
	}
	# No shortcut in profile
	elseif ($json.code -ne 0 -or $unloadProfile)
	{
		log2 "[TASBKAR] No taskbar shortcut for this profile, checking if [PARX] taskbar shortcuts are to be removed" "warn"
		Manage-Taskbar -remove_all -Debugmode $Debugmode -debug_level $debug_level -sessionID $sessionid
	}
}


