﻿# creates a Debug-Parx function dynamically
function Enable-DebugMode
{
	param (
		[switch]$Debug,
		[switch]$Monitor,
		[switch]$All
	)
	# Enable debug mode and functions
	if ($debug)
	{
		Set-HashConsole
		# Setting debugpreference to continue, it will show all "Write-Debug" from parxshell in console mode
		$global:DebugPreference = "Continue"
		# Toggling all autoloads to 1, because it's debug mode and we want it to load a profile wether autoloads are set or not
		$global:autoload_options = 1
		$global:autoload_printers = 1
		$global:autoload_shares = 1
		$global:autoload_desktop = 1
		$global:autoload_taskbar = 1
		$global:autoload_programs = 1
		# Name of the function that will be created when debug mode is enabled
		$Name = 'Debug-Parx'
		# Code that will be added in the function
		$Code = {
			PARAM (
				[parameter(Position = 0, Mandatory = $true, HelpMessage = "Require String name as value ", ValueFromPipeline = $True)]
				[String][ValidateNotNullOrEmpty()]
				$text
			)
			Try
			{
				# Here we lock the host ui object (console), to prevent synchronous call from runspaces 
				$LogMutex = New-Object System.Threading.Mutex($false, "LogMutex")
				$LogMutex.WaitOne() | out-null
				
				
				# Coloring based on message level
				if ($text -like "*ERROR*")
				{
					$hash.host.Ui.WriteLine('Red', 'Black', $text)
				}
				elseif ($text -like "*SUCCESS*")
				{
					$hash.host.Ui.WriteLine('Green', 'Black', $text)
				}
				elseif ($text -like "*INFO*")
				{
					$hash.host.Ui.WriteLine('White', 'Black', $text)
				}
				elseif ($text -like "*WARN*")
				{
					$hash.host.Ui.WriteLine('Yellow', 'Black', $text)
				}
				elseif ($text -like "*TASK*")
				{
					$hash.host.Ui.WriteLine('Gray', 'Black', $text)
				}
				else
				{
					$hash.host.Ui.WriteLine('White', 'Black', $text)
					
				}
				# Unlock console object
				$LogMutex.ReleaseMutex() | out-null
			}
			
			Catch
			{
				Write-Host  $_.Exception.message
			}
		}
		
		# create new function in function: drive and set scope to "global:"			
		if (!(Get-Content -Path function:\$Name -ErrorAction SilentlyContinue))
		{
			$null = New-Item -Path function: -Name "global:$Name" -Value $Code
		}
	}
	# Enable event watcher for functions printer taskbar and share, as they are launched from within user session
	if ($monitor)
	{
		$Name = 'Debug-UserEvent'
		$Code = {
			param
			(
				[switch]$all,
				[switch]$single,
				[int]$max_task
			)
			Try
			{
				# Forging the watcher props, we will listen the Parxshell event log
				$logname = "Parxshell"
				$select = "*[System[(EventID=$eventid)]]"
				$query = [System.Diagnostics.Eventing.Reader.EventLogQuery]::new("Parxshell", [System.Diagnostics.Eventing.Reader.PathType]::LogName, $select)
				$watcher = [System.Diagnostics.Eventing.Reader.EventLogWatcher]::new($query)
				$watcher.Enabled = $true
				Register-ObjectEvent -InputObject $watcher -EventName 'EventRecordWritten' -SourceIdentifier "Parxgui"
				# Settings initial wait variables 
				$waiting_all = $true
				$wait = $true
				$done = $false
				# Variable that will increment each time a part of the profile is loaded
				$global:finished = 0
				# Wether runspaces are launched, or single task
				if (!($single))
				{
					$listen = $max_task
				}
				else
				{
					$listen = 1
				}
				
				do
				{
					$event = Wait-Event
					$pb = $event.SourceEventArgs.EventRecord.properties[0].Value
					# Logic for the action on event
					# Parxgui sends a "Share done / Printer done" message when these parts of the profile are loaded, only gets messages that come before
					if ($pb -notlike "*Done*")
					{
						Debug-Parx -Text $pb
						
					}
					if ($pb -like "*Done*")
					{
						$global:finished++
					}
					
					# Logic for waiting for runspaces to be finished
					# "All" means all runspaces that are opened, new-runspace function (libs.ps1) will send a "FINISH" event when all runspaces are closed
					if ($all)
					{
						if ($pb -like "*FINISH*")
						{
							$waiting_all = $false
						}
					}
					# Single task, no need to wait for runspaces
					else
					{
						$waiting_all = $false
					}
					# We compare how many part of the profile are done loading with the total number of parts to monitor
					if ($finished -lt $listen)
					{
						$wait = $true
					}
					else
					{
						$wait = $false
					}
					# Profile has finished loading, closing the while loop
					if ($wait -eq $false -and $waiting_all -eq $false)
					{
						$done = $true
					}
					$event | Remove-Event
					Start-Sleep -Milliseconds 100
					
				}
				while ($done -eq $false)
				# Cleaning up
				
				Unregister-Event -SourceIdentifier "Parxgui"
				$watcher.Dispose()
			}
			Catch
			{
				Write-Host $_.Exception.Message
			}
			
		}
		# create new function in function: drive and set scope to "global:"			
		if (!(Get-Content -Path function:\$Name -ErrorAction SilentlyContinue))
		{
			$null = New-Item -Path function: -Name "global:$Name" -Value $Code
		}
	}
	
}

# To be able to write to console from runspace in debug mode
function Set-HashConsole
{
	$global:hash = [hashtable]::synchronized(@{ })
	$host.ui.rawui.windowtitle = "Parxshell debug"
	$global:hash.host = $host
}

# Enable or disable console window quick edit (to prevent script from pausing)
function Set-ConsoleEdit
{
	Param (
		$set
	)
	# We don't want the debug text from this function
	$DebugPreference = 'SilentlyContinue'
	$Objsh = New-Object -ComObject WScript.Shell
	add-type -TypeDefinition $pause -Language CSharp
	# Ensure console has focus
	$hWnd = [ParxDebug.Console]::GetConsoleWindow()
	[ParxDebug.Console]::SetForegroundWindow($hwnd)
	# Ensure console isn't pause by previous user selection, send F5 key 
	$Objsh.SendKeys("{F5}")
	# Enable / disable quick edit mode
	[DisableConsoleQuickEdit]::SetQuickEdit($set)
	$DebugPreference = 'Continue'
}

# Disable close button on debug console (because it closes parxgui process at the same time)
function Disable-CloseConsole ($hWnd)
{
	# Get debug console menu
	$hMenu = [ParxDebug.Console]::GetSystemMenu($hwnd, 0)	
	# Set debug console Style to toolwindow (so that it doesn't appear in the taskbar)
	$WS_EX_TOOLWINDOW = 0x00000080L
	$GWL_EXSTYLE = -20
	$SC_CLOSE = 0xF060
	$MF_DISABLED = 0x00000002L
	[ParxDebug.Console]::SetWindowLongPtr($hwnd, $GWL_EXSTYLE, $WS_EX_TOOLWINDOW) | Out-Null
	# Disable X Button
	[ParxDebug.Console]::EnableMenuItem($hMenu, $SC_CLOSE, $MF_DISABLED) | Out-Null
}


# Move debug console to topright, and fill height
function Move-Console
{
	Param (
		$hWnd,
		[switch]$user
	)
	# Grab current video card resolution
	[int]$screen_width = ([System.Windows.Forms.Screen]::PrimaryScreen).WorkingArea.Width
	# Set console position (topmost right minus current console width)		
	$Rectangle = [ParxDebug.Console+RECT]::New()
	$Return = [ParxDebug.Console]::GetWindowRect($hWnd, [ref]$Rectangle)
	$Width = $Rectangle.Right - $Rectangle.Left
	$Height = $Rectangle.Bottom - $Rectangle.Top
	if ($user)
	{
		$global:where = 0
		$Width = $Width / 1.63
	}
	else
	{
		$global:where = $screen_width - $Width		
	}
	# Set window position		
	[ParxDebug.Console]::MoveWindow($hWnd, $where, 0, $Width, $Height, $true)
}

# Show console window on debug mode
Function Set-ConsoleVisibility
{
	Param (
		$show_hide,
		[switch]$move,
		[boolean]$user
	)
	# We don't want the debug text to be written in the console for this function
	$DebugPreference = 'SilentlyContinue'
	# Add type if not found
	if (!("ParxDebug.Console" -as [type]))
	{
		Add-Type -MemberDefinition $console -Namespace ParxDebug -Name Console -Language CSharp
	}
	# Set console background
	$Host.UI.RawUI.BackgroundColor = "Black"
	Clear-Host	
	# Set console size	
	$currWidth = $host.UI.RawUI.WindowSize.Width
	$maxHeight = $host.UI.RawUI.MaxPhysicalWindowSize.Height
	$host.UI.RawUI.WindowSize = New-Object System.Management.Automation.Host.size($currWidth, $maxHeight)
	# Find powershell process
	$hWnd = [ParxDebug.Console]::GetConsoleWindow()
	if ($move)
	{
		Move-Console -hWnd $hWnd -user:$user
	}
	#Set some style and disable X button
	Disable-CloseConsole $hWnd
	# Show the hidden console
	[ParxDebug.Console]::ShowWindow($hWnd, $show_hide)
	$DebugPreference = 'Continue'
	
}

# Grab all console text on debug mode
function Get-ConsoleText
{
	$global:textBuilderConsole = New-Object System.Text.StringBuilder
	$textBuilderLine = New-Object System.Text.StringBuilder
	
	# Get console buffer content
	$bufferWidth = $host.UI.RawUI.BufferSize.Width
	$bufferHeight = $host.UI.RawUI.CursorPosition.Y
	$rec = New-Object System.Management.Automation.Host.Rectangle(0, 0, ($bufferWidth), $bufferHeight)
	$buffer = $host.UI.RawUI.GetBufferContents($rec)
	
	# Here we parse each line in console buffer
	for ($i = 0; $i -lt $bufferHeight; $i++)
	{
		for ($j = 0; $j -lt $bufferWidth; $j++)
		{
			$cell = $buffer[$i, $j]
			$null = $textBuilderLine.Append($cell.Character)
		}
		
		# Global content so that we can reset search filter to initial state
		$null = $textBuilderConsole.AppendLine($textBuilderLine.ToString().TrimEnd())
		# Reset line state for next line
		$textBuilderLine = New-Object System.Text.StringBuilder
	}
	Return $textBuilderConsole
}


function Disable-DebugMode
{
	# Setting debugpreference to ignore, all write-debug will be ignored in parxshell
	$global:DebugPreference = 'SilentlyContinue'
	
	# Toggling all autoloads to 0
	$global:autoload_options = 0
	$global:autoload_printers = 0
	$global:autoload_shares = 0
	$global:autoload_desktop = 0
	$global:autoload_taskbar = 0
	$global:autoload_programs = 0
	
}

# Display options list (info)
Function list-options ($profileid)
{
	
	# get options list
	$json = get-options-list $profileid
	
	if ($json.code -eq 0)
	{
		
		$list = ConvertFrom-JSON $json.message
		
		foreach ($option in $list)
		{
			$hash.host.Ui.WriteLine('White', 'Black', "$($option.Name) : $($option.pivot.ParameterValue)")
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('Red', 'Black', "$($json.message)")
	}
}

# Get profile informations to be written in debug console
function Get-ProfileInfo ($profileId)
{
	$hash.host.Ui.WriteLine('Green', 'Black', " `r")
	$hash.host.Ui.WriteLine('Green', 'Black', "*********************** `r")
	$hash.host.Ui.WriteLine('Green', 'Black', "PROFIL : $($profilename) `r")
	$hash.host.Ui.WriteLine('Green', 'Black', "*********************** `r")
	
	$hash.host.Ui.WriteLine('Green', 'Black', " `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Raccourcis : `r")
	list-shortcuts $profileid
	
	$hash.host.Ui.WriteLine('Green', 'Black', " `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Taskbar : `r")
	list-taskbar $profileid
	
	$hash.host.Ui.WriteLine('Green', 'Black', " `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Options : `r")
	list-options $profileid
	
	$hash.host.Ui.WriteLine('yellow', 'Black', " `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Partages : `r")
	list-shares $profileid
	
	$hash.host.Ui.WriteLine('yellow', 'Black', " `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Imprimantes : `r")
	list-printers $profileid
	
}

function Get-HardwareInfo
{
	# Get some ram info
	$cn = (Get-Item "hklm:\software\microsoft\windows\currentversion\group policy\state\machine").GetValue("Distinguished-Name") | ForEach-Object { $_.Substring(0, $_.IndexOf(',') + 1 + $_.Substring($_.IndexOf(',') + 1).IndexOf(',')) } -outvariable salle
	$salle = $salle.Split(",")[1].Replace("OU=", "")
	$slotram = Get-WmiObject Win32_PhysicalMemoryArray
	$totalram = Get-WMIObject Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum
	
	# Get some proc info
	$proc = Get-CimInstance -Class CIM_Processor -ErrorAction Stop | Select-Object Name, CurrentClockSpeed, NumberOfCores, NumberOfLogicalProcessors
	
	# Get some network info
	$adapters = Get-NetAdapter | Select-Object Name, LinkSpeed, Status, MacAddress, InterfaceDescription
	
	# Get some Os and free ram info
	$install = Get-CimInstance Win32_OperatingSystem | Select-Object InstallDate, FreePhysicalMemory
	
	# Get some computer info
	$computer = Get-WMIObject -class Win32_ComputerSystem | Select-Object Manufacturer, Model
	
	# Get some bios info
	$bios = Get-CimInstance -ClassName Win32_BIOS | Select-Object Manufacturer, SMBIOSBIOSVersion, ReleaseDate
	$bios_date = $bios.ReleaseDate.ToString("dd/MM/yyyy")
	
	# Display it	
	$hash.host.Ui.WriteLine('yellow', 'Black', "HOSTNAME :`t`t`t $env:computername`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "FABRICANT :`t`t`t $($computer.Manufacturer)`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "MODELE :`t`t`t $($computer.Model)`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "OS NAME :`t`t`t $hostOS `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "OS DATE D'INSTALLATION :`t $(($install.InstallDate).ToString('f')) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Salle :`t`t`t`t $salle `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "BIOS : `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "FABRICANT : `t`t`t $($bios.Manufacturer) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "VERSION : `t`t`t $($bios.SMBIOSBIOSVersion) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "DATE : `t`t`t`t $($bios_date) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "PROCESSEUR :`t`t`t $($proc.Name) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "VITESSE ACTUELLE :`t`t $($proc.CurrentClockSpeed)GHz `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "NOMBRE DE COEURS :`t`t $($proc.NumberOfCores) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "NOMBRE DE THREADS :`t`t $($proc.NumberOfLogicalProcessors) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Ram installée :`t`t`t $($totalram.sum/1024/1024/1024)GB `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Nombre de barrettes :`t`t $($totalram.count) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Nombre de slot :`t`t $($slotram.MemoryDevices) `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "Ram disponible :`t`t $([math]::Round($install.FreePhysicalMemory/1mb, 2))GB `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "CARTE(S) RESEAU : `r")
	foreach ($adapter in $adapters)
	{
		$hash.host.Ui.WriteLine('yellow', 'Black', "CARTE :`t`t`t`t $($adapter.InterfaceDescription)`r")
		$hash.host.Ui.WriteLine('yellow', 'Black', "RESEAU :`t`t`t $($adapter.Name)`r")
		$hash.host.Ui.WriteLine('yellow', 'Black', "STATUT :`t`t`t $($adapter.Status)`r")
		if ($adapter.Status -ne "Disconnected")
		{
			$hash.host.Ui.WriteLine('yellow', 'Black', "VITESSE :`t`t`t $($adapter.LinkSpeed)`r")
		}
		if ($adapter.Count -gt 1)
		{
			$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
		}
	}
}

# Get parx informations to be written in debug console
function Get-ParxInfo
{
	$psv = $Host.Version
	if (get-service ParxShell -ErrorAction SilentlyContinue) { $etat_srv = (get-service ParxShell).status }
	else { $etat_srv = "Introuvable" }
	$hostid, $hostOS = Get-HostInfo
	$profiles = Get-Profiles $env:COMPUTERNAME $username
	$hash.host.Ui.WriteLine('Blue', 'Black', "
	  ____   _    ____  __  __    _          _ _ 
	 |  _ \ / \  |  _ \ \ \/ /___| |__   ___| | |
	 | |_) / _ \ | |_) | \  // __| '_ \ / _ \ | |
	 |  __/ ___ \|  _ <  /  \\__ \ | | |  __/ | |
	 |_| /_/   \_\_| \_\/_/\_\___/_| |_|\___|_|_|
 ")
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	$hash.host.Ui.WriteLine('Green', 'Black', "INFORMATIONS GENERALES :")
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "POWERSHELL :`t`t`t $psv `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "VERSION CLIENT :`t`t $version `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "ADRESSE SERVEUR :`t`t $address `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "ETAT SERVICE :`t`t`t $etat_srv`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "REFRESH RATE SERVICE :`t`t $service_refresh mn `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "USERNAME :`t`t`t $userName`r")
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	$hash.host.Ui.WriteLine('Green', 'Black', "INFORMATIONS ORDINATEUR :")
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	Get-HardwareInfo
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	$hash.host.Ui.WriteLine('Green', 'Black', "INFORMATIONS CONFIGURATION CLIENT :")
	$hash.host.Ui.WriteLine('Green', 'Black', "*****************************************************************`r")
	
	$hash.host.Ui.WriteLine('yellow', 'Black', "MONTRER LES NOTIFICATIONS :`t $show_notification `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "AUTOLOADS : `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "DESKTOP :`t`t`t $autoload_desktop `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "TASKBAR :`t`t`t $autoload_taskbar `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "OPTIONS :`t`t`t $autoload_options `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "SHARES :`t`t`t $autoload_shares `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "PRINTERS :`t`t`t $autoload_printers `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "PROGRAMMES :`t`t`t $autoload_programs `r")
	$hash.host.Ui.WriteLine('yellow', 'Black', "-----------------------------------------------------------------`r")
	
	if ($global:profilename -ne "")
	{
		$hash.host.Ui.WriteLine('yellow', 'Black', "PROFIL ACTUEL :`t`t`t $global:profilename `r")
	}
	else
	{
		$hash.host.Ui.WriteLine('yellow', 'Black', "PROFIL ACTUEL :`t`t`t Aucun profil n'est appliqué sur l'hôte, utilisateur, ou hôte/utilisateur `r")
	}
	
	$hash.host.Ui.WriteLine('yellow', 'Black', "PROFILS DISPONIBLES pour $env:Computername, $username : `r")
	
	if ($profiles.Code -eq 0)
	{
		$list = ConvertFrom-Json $profiles.Message
		foreach ($pro in $list)
		{
			switch ($pro.MatchType)
			{
				"0" {
					$match = "hôte + utilisateur"
				}
				"1" {
					$match = "utilisateur"
				}
				"2" {
					$match = "hôte"
				}
				
			}
			$hash.host.Ui.WriteLine('yellow', 'Black', "$($pro.Name), id $($pro.Id), s'applique sur $match")
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('yellow', 'Black', "Aucun profil n'est appliqué sur l'hôte, utilisateur, ou hôte/utilisateur")
	}
}


# get the shortcuts list from the db (page info)
Function list-shortcuts ($profileid)
{
	
	# getting the shortcuts list
	$json = get-shortcuts-list $profileid
	
	if ($json.code -eq 0)
	{
		
		$list = ConvertFrom-JSON $json.message
		
		foreach ($shortcut in $list)
		{
			if ($shortcut.name)
			{
				$hash.host.Ui.WriteLine('White', 'Black', "$($shortcut.name)")
			}
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('Red', 'Black', "$($json.message)")
	}
}

# get the tasbkar list from the db (page info)
Function list-taskbar ($profileid)
{
	
	# getting the shortcuts list
	$json = get-taskbar-list $profileid
	
	if ($json.code -eq 0)
	{
		
		$list = ConvertFrom-JSON $json.message
		
		foreach ($shortcut in $list)
		{
			if ($shortcut.name)
			{
				$hash.host.Ui.WriteLine('White', 'Black', "$($shortcut.name)")
			}
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('Red', 'Black', "$($json.message)")
	}
}

# show the list o' shares (in info)
Function list-shares ($profileid)
{
	
	# Getting option list
	$json = get-shares-list $profileid
	
	if ($json.code -eq 0)
	{
		#$list = json-array $json.message
		$list = ConvertFrom-JSON $json.message
		
		foreach ($share in $list)
		{
			$name = $share.name
			$target = $share.target
			$letter = $share.pivot.shareLetter
			
			# Token
			if ($target -match "%USER%")
			{
				$target = $target -replace "%USER%", "$userName"
			}
			
			if ($name)
			{
				$hash.host.Ui.WriteLine('White', 'Black', "$name ($target) on drive [$letter]")
			}
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('Red', 'Black', "$($json.message)")
	}
}

# show the list o' printers
Function list-printers ($profileid)
{
	# Getting printers list
	$json = get-printers-list $profileid
	
	if ($json.code -eq 0)
	{
		$list = json-array $json.message
		
		foreach ($printer in $list)
		{
			$name = $printer.name
			$target = $printer.target
			$default = $printer.pivot.default
			
			if ($default -eq 1) { $default_text = " (default)" }
			else { $default_text = "" }
			
			if ($name)
			{
				$hash.host.Ui.WriteLine('White', 'Black', "$name sur $target $default_text")
			}
		}
	}
	else
	{
		$hash.host.Ui.WriteLine('Red', 'Black', "$($json.Message)")
	}
	
}

# Debug shortcut function
function Debug-Shortcuts
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement du bureau pour le profil : $global:profilename, id : $global:profileid |")
	$global:eventid = Get-Random -Minimum 1 -Maximum 9998
	draw-shortcut-list $global:profileid -folder_cache $folder_cache -autolaunch $autolaunch -username $username -sessionid $sessionid
	# Enabling debug controls
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement du bureau terminé pour le profil : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
	# If shortcuts for which autolaunch is set are found, launch them
	Autolaunch $autolaunch
}

function Debug-Options
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des options pour le profil : $global:profilename, id : $global:profileid |")
	$profile_cache = "C:\Users\$userName\appdata\roaming\Parx\$global:profileid.json"
	$upgrade_option, $options_cache = Set-ProfileCache $global:profileid $profile_cache
	$autoloads, $variables = Set-ParxVariables $true
	set-options-cache -options_cache $options_cache -switch_profile $true -first_logon $false
	# Enabling debug controls	
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des options terminé pour le profil : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
	
}

function Debug-Printers
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	# As printers are launched from within user session, we need to debug them from event log
	Enable-DebugMode -Monitor
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des imprimantes pour le profil : $global:profilename, id : $global:profileid |")
	$global:eventid = Get-Random -Minimum 1 -Maximum 9998
	set-printers-list $profileid -Debugmode $true -debug_level $debug_level -sessionid $sessionid
	# Monitor event log so that we can write informations to the console
	Debug-UserEvent -single
	# Enabling debug controls	
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des imprimantes terminé pour le profil : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
}

Function Debug-Shares
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	# As shares are launched from within user session, we need to debug them from event log
	Enable-DebugMode -Monitor
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des partages pour le profil : $global:profilename, id : $global:profileid |")
	$global:eventid = Get-Random -Minimum 1 -Maximum 9998
	set-shares-list $profileid -Debugmode $true -debug_level $debug_level -sessionid $sessionid
	# Monitor event log so that we can write informations to the console
	Debug-UserEvent -single
	# Enabling debug controls	
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement des partages terminé pour le profil : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
}

Function Debug-Taskbar
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	# As taskbar is launched from within user session, we need to debug them from event log
	Enable-DebugMode -Monitor
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement de la taskbar pour le profil : $global:profilename, id : $global:profileid |")
	$global:eventid = Get-Random -Minimum 1 -Maximum 9998
	set-taskbar-list $profileid -Debugmode $true -debug_level $debug_level -sessionid $sessionid
	# Monitor event log so that we can write informations to the console
	Debug-UserEvent -single
	# Enabling debug controls	
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement de la taskbar terminé pour le profil : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
}

function Debug-All
{
	# Logic for the Gui
	# Disabling debug controls
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	# Disabling console quick edit (so that script isn't interrupted on selection)
	Set-ConsoleEdit -set $true
	# As taskbar, share, printers are launched from within user session, we need to debug them from event log
	Enable-DebugMode -Monitor
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement du profil : $global:profilename, id : $global:profileid |")
	# We have to set all autoloads to 1, because we're debugging !
	$global:autoload_options = 1
	$global:autoload_printers = 1
	$global:autoload_shares = 1
	$global:autoload_desktop = 1
	$global:autoload_taskbar = 1
	$global:autoload_associations = 1
	# We have to tell parx that a new profile is being loaded, so that it will apply all settings again
	$global:switch_profile = $true
	$global:first_logon = $false
	$folder_cache = "C:\Users\$userName\appdata\roaming\Parx\"
	$profileid | Set-Content "$folder_cache\last_profile.txt"
	$profileName | Add-Content "$folder_cache\last_profile.txt"
	$profile_cache = "C:\Users\$userName\appdata\roaming\Parx\$profileid.json"
	$upgrade_option, $options_cache = Set-ProfileCache $profileid $profile_cache
	$autoloads, $variables = Set-ParxVariables $switch_profile $first_logon
	Write-EventLog -LogName "Parxshell" -Source "Gui" -eventid $eventid -EntryType Information -Message "[CLIENT] Reloading profile for $username |" -Category 1 -RawData 10, 20
	$max_task = ($autoloads.Values | Measure-Object -Sum).Sum
	New-Runspace -Threads $max_task -Autoloads $autoloads -variables $variables -profileid $profileid
	$switch_profile = $false
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	$hash.host.Ui.WriteLine('Green', 'Black', "Chargement du profil terminé : $global:profilename, id : $global:profileid |")
	# Setting text cache for searching through the logs
	$global:debug_txt = Get-ConsoleText
	$isearch.Enabled = $true
	# Enabling console quick edit
	Set-ConsoleEdit -set $false
}

# Function to get all script variables, minus global ones
function get-uservariables ($callstack)
{
	$choice = $null
	get-variable | where-object {
		(@(
				"FormatEnumerationLimit",
				"MaximumAliasCount",
				"MaximumDriveCount",
				"MaximumErrorCount",
				"MaximumFunctionCount",
				"MaximumVariableCount",
				"PGHome",
				"PGSE",
				"PGUICulture",
				"PGVersionTable",
				"PROFILE",
				"PSSessionOption"
			) -notcontains $_.name) -and `
		(([psobject].Assembly.GetType('System.Management.Automation.SpecialVariables').GetFields('NonPublic,Static') | Where-Object FieldType -eq ([string]) | ForEach-Object GetValue $null)) -notcontains $_.name -and $_.Name -eq "address" -or $_.Name -eq "profileName" -or $_.Name -eq "name" -or $_.Name -eq "shortcut" -or $_.Name -eq "list" -or $_.Name -eq "path" -or $_.Name -eq "username" -or $_.Name -eq "userMSID"
	} | ForEach-Object {
		$type = (($_.Value.GetType() | Select-Object UnderlyingSystemType).UnderlyingSystemType).Name
		if ($type -eq "Object[]" -or $type -eq "PsCustomObject")
		{
			$value = ($($_.Value) | Format-List | Out-String).ToString()
		}
		else
		{
			$value = "$([string]$_.Value)" + " |"
		}
		[pscustomobject]@{ Name = $_.Name; Value = $($value | Out-String) }
	} | Format-list | Out-Host
}

# Show parx and host info
function Debug-Info
{
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	Set-ConsoleEdit -set $true
	Get-ParxInfo
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	Set-ConsoleEdit -set $false
}
# Show profile info
function Debug-Profile
{
	$childform.Enabled = $false
	$dsearch.Enabled = $false
	Set-ConsoleEdit -set $true	
	Get-ProfileInfo $profileid
	$childform.Enabled = $true
	$dsearch.Enabled = $true
	Set-ConsoleEdit -set $false
}
# Get profiles errors
function Debug-Error
{
	$hash.host.Ui.WriteLine('Yellow', 'Black', "Erreurs du profil :")
	foreach ($err in ($errors.Split('|').Replace("|", "")))
	{
		$hash.host.Ui.WriteLine('Red', 'Black', $err)
	}
}
# Show current function in debug console
function List-Callstack ($callstack)
{
	$callstack | Foreach-Object {
		$splat = [ordered]@{
			Location = $_.Location
			function = $_.Command
			Command  = $_.InvocationInfo.Line.TrimStart()
			Variables = $_.InvocationInfo.BoundParameters | Out-String
		}
	}
	Return $splat
}
# Choices for the advanced debug mode
function Debug-Script 
{
	[CmdletBinding()]
	param ([Parameter (ValueFromPipeline)]
		$callstack
	)
	$splat = List-Callstack $callstack
	$choice = $null
	$choice = Read-Host "Press v to see variables in memory, f to see function content, or any other key to debug function call ($($callstack.Command))"
	if ($choice -eq "V")
	{
		get-uservariables $callstack
		$choice = Read-Host "Press F to see function content, or any other key to debug function call ($($callstack.Command))"
		if ($choice -eq "F")
		{
			$c = Get-Content function:$($callstack.Command)
			Write-Host $c -ForegroundColor Blue
			Read-Host "Press any key to process function call ($($callstack.Command)"
			$splat | Format-List | Out-Host
		}
	}
	else
	{
		
		if ($choice -eq "F")
		{
			$c = get-content function:$($callstack.Command)
			Write-Host $c -ForegroundColor Blue
			$choice = Read-Host "Press V to see variables in memory, or any other key to debug function call ($($callstack.Command))"
			if ($choice -eq "V")
			{
				get-uservariables $callstack
				Read-Host "Press any key to debug function call ($($callstack.Command))"
			}
		}
		$splat | Format-List | Out-Host
	}
	
}