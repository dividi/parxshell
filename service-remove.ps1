
# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator'))
{
	if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000)
	{
		$CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " 
		Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine -WindowStyle Hidden
		Exit
	}
}

	c:\parxshell\Bin\nssm stop ParxShell
	c:\parxshell\Bin\nssm remove ParxShell confirm
Start-Process Powershell.exe -Verb Runas -WindowStyle Hidden -ArgumentList "-noprofile -command &{if ((Test-Path -LiteralPath 'HKLM:\SOFTWARE\Parxshell') -eq `$true) { Remove-Item 'HKLM:\SOFTWARE\Parxshell' -force -ea SilentlyContinue};Get-CimInstance Win32_Process | Select-Object Name, ProcessId, CommandLine | Where-Object { `$_.Name -eq 'powershell.exe' -and `$_.CommandLine -notlike '*Test-Path*' } | ForEach-Object { Stop-Process -Id `$_.ProcessId -force };while (Get-CimInstance Win32_Process | Select-Object  Name,ProcessId, CommandLine | Where-Object { `$_.CommandLine -like '*parxgui.ps1*' -and `$_.CommandLine -notlike '*Test-Path*' }){Start-Sleep -Milliseconds 50};Remove-Item -Path 'c:\ParxShell' -Recurse -Force}"
	# Remove Parx Reg 
	

	 