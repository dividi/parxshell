﻿# Impersonnate Current user
function Invoke-AsCurrent_User
{
	[CmdletBinding()]
	param (
		[Parameter(Mandatory = $true)]
		[scriptblock]$ScriptBlock,
		[Parameter(Mandatory = $false)]
		[switch]$NoWait,
		[Parameter(Mandatory = $false)]
		[switch]$UseWindowsPowerShell,
		[Parameter(Mandatory = $false)]
		[switch]$UseMicrosoftPowerShell,
		[Parameter(Mandatory = $false)]
		[switch]$NonElevatedSession,
		[Parameter(Mandatory = $false)]
		[switch]$Visible,
		[Parameter(Mandatory = $false)]
		[switch]$CacheToDisk,
		[Parameter(Mandatory = $false)]
		[switch]$CaptureOutput
	)
	if (!("RunAsUser.ProcessExtensions" -as [type]))
	{
		Add-Type -TypeDefinition $source -Language CSharp
	}
	if ($CacheToDisk)
	{
		$ScriptGuid = new-guid
		$null = New-item "c:\users\$username\appdata\roaming\parx\temp\$($ScriptGuid).ps1" -Value $ScriptBlock -Force
		$pwshcommand = " -noprofile -Nologo -ExecutionPolicy Bypass -file `"c:\users\$username\appdata\roaming\parx\temp\$($ScriptGuid).ps1`""
	}
	else
	{
		$encodedcommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($ScriptBlock))
		$pwshcommand = "-noprofile -ExecutionPolicy Bypass -Nologo -EncodedCommand $($encodedcommand)"
	}
	$OSLevel = (Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").CurrentVersion
	if ($OSLevel -lt "6.2") { $MaxLength = 8190 }
	else { $MaxLength = 32767 }
	if ($encodedcommand.length -gt $MaxLength -and $CacheToDisk -eq $false)
	{
		Write-Error -Message "The encoded script is longer than the command line parameter limit. Switching to cachetodisk"
		$CacheToDisk = $true
		$ScriptGuid = new-guid
		$null = New-item "c:\users\$username\appdata\roaming\parx\temp\$($ScriptGuid).ps1" -Value $ScriptBlock -Force
		$pwshcommand = " -noprofile -Nologo -ExecutionPolicy Bypass -file `"c:\users\$username\appdata\roaming\parx\temp\$($ScriptGuid).ps1`""
	}
	if ($UseMicrosoftPowerShell -and -not (Test-Path -Path "$env:ProgramFiles\PowerShell\7\pwsh.exe"))
	{
		Write-Error -Message "Not able to find Microsoft PowerShell v7 (pwsh.exe). Ensure that it is installed on this system"
		return
	}
	
	try
	{
		# Use the same PowerShell executable as the one that invoked the function, Unless -UseWindowsPowerShell or -UseMicrosoftPowerShell is defined.
		$pwshPath = if ($UseWindowsPowerShell) { "$($ENV:windir)\system32\WindowsPowerShell\v1.0\powershell.exe" }
		elseif ($UseMicrosoftPowerShell) { "$($env:ProgramFiles)\PowerShell\7\pwsh.exe" }
		else { (Get-Process -Id $pid).Path }
		
		if ($NoWait) { $ProcWaitTime = 1 }
		else { $ProcWaitTime = -1 }
		if ($NonElevatedSession) { $RunAsAdmin = $false }
		else { $RunAsAdmin = $true }
		[RunAsUser.ProcessExtensions]::StartProcessAsCurrentUser(
			$pwshPath, "`"$pwshPath`" $pwshcommand",
			(Split-Path $pwshPath -Parent), $Visible, $ProcWaitTime, $RunAsAdmin, $CaptureOutput)
		if ($CacheToDisk) { $null = remove-item "c:\users\$username\appdata\roaming\parx\temp\$($ScriptGuid).ps1" -Force }
	}
	catch
	{
		Write-Error -Message "Could not execute as currently logged on user: $($_.Exception.Message)" -Exception $_.Exception
		return
	}
	
}

# Launch Parxshell into user session in case of fresh install
function Launch-Parxshell
{
	Try
	{
		# Launch Parxshell under active session if one is found
		$logontype = "Restart"
		$username = ((Get-CimInstance -ClassName Win32_ComputerSystem).Username) -replace '.*?\\(.*)', '$1'
		# Get SID from username
		$sid = (New-Object System.Security.Principal.NTAccount($username)).Translate([System.Security.Principal.SecurityIdentifier]).value
		# Get Session ID from explorer based on active username
		$sessionid = (Get-Process -name "explorer" -IncludeUserName | Select-Object -Property UserName, SI -First 1 | where-object { $_ -like "*$username*" }).SI
		# If an user is already logged in an active session, launch parx into its session 
		log2 "[INSTALL] As Parx was just installed, trying to launch Parxgui for user $username with sessionid $sessionid on sid $sid" "Info"
		$args = @"
		-nowait -session:$sessionid "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype -from_service" 
"@
		Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args
	}
	catch
	{
		# We don't care, no user session is active, parx will be launched whenever someone logs in		
	}
}

# Grab all user groups from AD
function Get-ADUserGroups
{
	param (
		[Parameter(Mandatory = $true)]
		[string]$Username
	)
	
	$root = New-Object System.DirectoryServices.DirectoryEntry("LDAP://RootDSE")
	$defaultNamingContext = $root.Properties["defaultNamingContext"].Value
	
	$searcher = New-Object System.DirectoryServices.DirectorySearcher
	$searcher.SearchRoot = New-Object System.DirectoryServices.DirectoryEntry("LDAP://$defaultNamingContext")
	$searcher.Filter = "(&(objectCategory=User)(samAccountName=$Username))"
	$searcher.PropertiesToLoad.AddRange(@("memberOf"))
	
	try
	{
		$result = $searcher.FindOne()
		
		if ($result -ne $null)
		{
			$groups = $result.Properties["memberOf"] | ForEach-Object {
				$_.ToString().Split(",")[0].Substring(3)
			}
			
			return $groups
		}
	}
	catch
	{
		log2 "[CLIENT] An error occurred while retrieving AD groups: $_" "error"
	}
	
	return $null
}

# Function to manage icons from user session when switching profiles
function Manage-IconPositionSave
{
	
	. C:\parxshell\class-c.ps1
	# Save icon position on desktop
	function Save-IconPosition
	{
		[CmdletBinding()]
		Param (
			[string]$username,
			[string]$folder_cache,
			[string]$profileid
		)
		
		if (!("ParxIcons.Program" -as [type]))
		{
			Add-Type -TypeDefinition $icons -Language CSharp -ReferencedAssemblies "c:\parxshell\Assemblies\UIAutomationClient.dll", "c:\parxshell\Assemblies\System.Xml.dll", "c:\parxshell\Assemblies\System.Xml.Linq.dll"
		}
		[ParxIcons.Program]::SavePositions("$folder_cache\$profileid.xml", $false)
	}
	Save-IconPosition -username "USERNAME" -folder_cache "FOLDER_CACHE" -profileid "PROFILEID"
}

# Function to manage icons from user session when switching profiles
function Manage-IconPositionLoad
{
	. C:\parxshell\class-c.ps1
	# Restore icon position on desktop
	function Restore-IconPosition
	{
		[CmdletBinding()]
		Param (
			[string]$username,
			[string]$folder_cache,
			[string]$profileid
		)
		if (!("ParxIcons.Program" -as [type]))
		{
			Add-Type -TypeDefinition $icons -Language CSharp -ReferencedAssemblies "c:\parxshell\Assemblies\UIAutomationClient.dll", "c:\parxshell\Assemblies\System.Xml.dll", "c:\parxshell\Assemblies\System.Xml.Linq.dll"
		}
		[ParxIcons.Program]::RestorePositions("$folder_cache\$profileid.xml", $false)
	}
	Restore-IconPosition -username "USERNAME" -folder_cache "FOLDER_CACHE" -profileid "PROFILEID"
}


# Save or Load icon position when profile shortcut are loaded (in case of multiple profile)
# Icons positions will be saved or restored
function Set-IconPosition
{
	[CmdletBinding()]
	Param (
		[string]$username,
		[string]$folder_cache,
		[string]$profileid,
		[string]$save_load
	)
	if ($save_load -eq "Save")
	{
		$def = (Get-ChildItem "Function:Manage-IconPositionSave").Definition
		$def = $def.Replace("USERNAME", "$username").Replace("FOLDER_CACHE", "$folder_cache").Replace("PROFILEID", "$profileid")
		$block = [scriptblock]::Create($def)
		invoke-ascurrent_user -UseWindowsPowerShell -scriptblock $block
	}
	else
	{
		$def = (Get-ChildItem "Function:Manage-IconPositionLoad").Definition
		$def = $def.Replace("USERNAME", "$username").Replace("FOLDER_CACHE", "$folder_cache").Replace("PROFILEID", "$profileid")
		$block = [scriptblock]::Create($def)
		invoke-ascurrent_user -UseWindowsPowerShell -scriptblock $block -NoWait
		
	}
}

# Runspace magic
function New-RunSpace
{
	[CmdletBinding()]
	Param (
		[int]$Threads,
		[String]$scriptPath = $PSScriptRoot,
		[int]$profileid,
		[string]$Command,
		$Autoloads,
		[hashtable]$variables,
		[hashtable]$autolaunch,
		[pscustomobject]$options_list,
		[switch]$unloadProfile = $false
	)
	
	#Rajout du chemin racine de parxshell ds les paramètres par défaut
	$PSBoundParameters.scriptPath = $scriptPath
	#Création d'un état initial du runspace (qui comprendra les variables indiquées ainsi que les fonctions du module parx
	$InitialSessionState = [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
	if ($DebugPreference -eq "Continue")
	{
		$debugmode = $true
		$autoloads.Insert(0, "Debug", 1)
		log2 "[CLIENT] RUNSPACE : démarrage" "success"
	}
	New-object System.Management.Automation.Runspaces.SessionStateVariableEntry -ArgumentList "Hash", $Hash, $Null | ForEach-Object { $InitialSessionState.Variables.Add($_) }
	#Variables injectées
	foreach ($var in $PSBoundParameters.GetEnumerator())
	{
		if ($var.Key -ne "Autoloads" -and $var.Key -ne "variables")
		{
			Write-Verbose "Ajout de la variable $($var.Key) avec la valeur $($var.Value)"
			New-object System.Management.Automation.Runspaces.SessionStateVariableEntry -ArgumentList "$($var.Key)", $($var.Value), $Null | ForEach-Object { $InitialSessionState.Variables.Add($_) }
		}
	}
	foreach ($var in $variables.GetEnumerator())
	{
		if ($($var.Key) -ne "Source" -and $($var.Key) -notlike "*wp*" -and $($var.Key) -notlike "*refresh*")
		{
			Write-Verbose "Ajout de la variable $($var.Key) avec la valeur $($var.Value)"
			
		}
		New-object System.Management.Automation.Runspaces.SessionStateVariableEntry -ArgumentList "$($var.Name)", $($var.Value), $Null | ForEach-Object { $InitialSessionState.Variables.Add($_) }
	}
	#Fonctions injectées
	Get-ChildItem function:/ | Where-Object { $_.Source -like "" } | ForEach-Object {
		#Write-Verbose "Ajout de la fonction $($_.Name) au runspace"
		$functionDefinition = Get-Content "Function:\$($_.Name)"
		$sessionStateFunction = New-Object System.Management.Automation.Runspaces.SessionStateFunctionEntry -ArgumentList $_.Name, $functionDefinition
		$InitialSessionState.Commands.Add($sessionStateFunction)
	}
	
	
	#Création d'un pool de runspace pour lancement en concurrence des tâches à effectuer
	$pool = [RunspaceFactory]::CreateRunspacePool(1, $Threads, $InitialSessionState, $Host)
	$pool.ApartmentState = "MTA"
	$pool.Open()
	$runspaces = @()
	
	#Scriptblock qui sera injecté dans chaque runspace
	$scriptblock = {
		
		param (
			[string]$task_name,
			[string]$DebugPreference,
			[boolean]$debugmode
		)
		Switch ($task_name)
		{
			"Debug"
			{
				. $PSScriptRoot\class-debug.ps1
				Debug-UserEvent -max_task ($Threads - 1) -all
				
			}
			"autoload_desktop"
			{
				log2 "[CLIENT] RUNSPACE : Loading desktop" "success"
				draw-shortcut-list -profileid $profileid -autolaunch $autolaunch -debugpref $DebugPreference -folder_cache $folder_cache -username $username -switch_profile:$switch_profile -sessionid $sessionid -noDesktop $noDesktop -unloadProfile:$unloadProfile
				
			}
			"autoload_taskbar"
			{
				log2 "[CLIENT] RUNSPACE : Loading taskbar" "success"
				set-taskbar-list $profileid -debugmode $debugmode -sessionid $sessionid
			}
			"autoload_options"
			{
				# Only do options if they were changed on server, on first_logon (new user profile on host), or when switching profile from GUI
				if ($upgrade_option -eq $true -or $first_logon -eq $true -or $switch_profile -eq $true)
				{
					log2 "[CLIENT] RUNSPACE : Loading options" "success"
					set-options-cache -options_cache $options_cache -switch_profile $switch_profile -first_logon $first_logon -DebugPref $DebugPreference -unloadProfile:$unloadProfile
				}
				else
				{
					log2 "[CLIENT] RUNSPACE :  No options changed since last logon, checking wallpaper only." "success"
					set-options-cache -wall_only -DebugPref $DebugPreference
				}
				
			}
			"autoload_shares"
			{
				log2 "[CLIENT] RUNSPACE : Loading shares" "success"
				set-shares-list $profileid -debugmode $debugmode -sessionid $sessionid -unloadProfile:$unloadProfile
			}
			"autoload_printers"
			{
				log2 "[CLIENT] RUNSPACE : Loading printers" "success"
				set-printers-list $profileid -debugmode $debugmode -debug_level $debug_level -sessionid $sessionid -unloadProfile:$unloadProfile
			}
			"autoload_associations"
			{
				log2 "[CLIENT] RUNSPACE : Loading default programs" "success"
				set-programs-list $profileid -debugmode $debugmode -debug_level $debug_level -sessionid $sessionid -unloadProfile:$unloadProfile
			}
		}
	}
	#Pour chaque autoload 1, lance un runspace	
	$autoloads.GetEnumerator() | ForEach-Object {
		if ($_.Value -eq 1)
		{
			Start-Sleep -Milliseconds 100
			$runspace = [powershell]::Create()
			# Ajout du scriptblock et des arguments au runspace			
			[void]$runspace.AddScript($scriptblock).AddArgument($_.Name).AddArgument($DebugPreference).AddArgument($debugmode)
			# pointage du runspace à un pool
			$runspace.RunspacePool = $pool
			$runspaces += [PSCustomObject]@{ RunSpace = $runspace; Status = $runspace.BeginInvoke() }
			
		}
	}
	
	#Array de collections des retours de chaque runspace
	$results = @()
	$x = 0
	while ($runspaces.Status -ne $null)
	{
		Start-Sleep -Milliseconds 1000
		$completed = $runspaces | Where-Object { $_.Status.IsCompleted -eq $true }
		$completed | ForEach-Object {
			# We increment x value, so that debug runspace will close itself when other runspaces are closed when receveing the event message finish
			$x++
			if ($x -eq ($Threads - 1) -and $DebugPreference -eq "Continue")
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] RUNSPACE : FINISH |" -Category 1 -RawData 10, 20
			}
			$result = $_.RunSpace.EndInvoke($_.Status)
			$results += $result
			$_.Status = $null
		}
	}
	#Nettoyage
	<#if ($profileid -eq "9999")
	{
		Remove-Item -Path "C:\Users\$username\appdata\roaming\parx" -Recurse -Force
	}#>
	$pool.Close()
	$pool.Dispose()
}

# Set profile file cache on user session for further uses
function Set-ProfileCache
{
	[OutputType([System.Object[]])]
	[CmdletBinding()]
	Param (
		[int]$profile_id,
		[String]$profile_cache
	)
	
	# Profile options revision
	$revision = ((get-profile $profile_id).Message | ConvertFrom-Json).Revision
	if ($DebugPreference -eq "Continue")
	{
		Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] Setting profile cache |" -Category 1 -RawData 10, 20
	}
	# If first logon, set options in cache file, in user session.
	$ExistingUser = Test-Path -Path $profile_cache
	if ($ExistingUser)
	{
		$emptyOptions = (Get-Content -Path $profile_cache).Length -eq 0
	}
	
	If (!($ExistingUser) -or ($emptyOptions -eq $true))
	{
		New-Item -Path $profile_cache -Force | out-Null
		New-Item -Path $last_profile_cache -Force | Out-Null
		$upgrade_option = $true
		$options_cache = get-profile_options $profile_id
		if ($DebugPreference -eq "Continue")
		{
			Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] First logon for $username, cache file for profile $profilename is set on $profile_cache |" -Category 1 -RawData 10, 20
		}
			if (!($ExistingUser))
			{
				log2 "[CLIENT] First logon for $username with $profilename, cache file for profile $profilename is set on $profile_cache" "info"
		}
		if ([string]::IsNullOrEmpty($options_cache))
		{
				log2 "[OPTIONS] There's no options in the profile." "info"
		}
	}
	# If options were changed on server (different revision), upgrade the cache file and set flag for option runspace
	else
	{
		$options_cache = get-content -Path $profile_cache | ConvertFrom-Json
		if ($options_cache[0].Value -ne $revision)
		{
			if ($DebugPreference -eq "Continue")
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] Profile options changed since last logon, upgrading client profile |" -Category 1 -RawData 10, 20
			}
			log2 "[CLIENT] Profile options changed since last logon, upgrading client profile" "info"
			$upgrade_option = $true
			$options_cache = upgrade_cache $profile_id
		}
		else
		{
			if ($DebugPreference -eq "Continue")
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] Profile options did not change since last logon |" -Category 1 -RawData 10, 20
			}
			log2 "[CLIENT] Profile options did not change since last logon" "info"
			$upgrade_option = $false
			$options_cache = upgrade_cache $profile_id
			
		}
	}
	
	$profile_id | Set-Content $last_profile_cache -Force
	$profilename | Add-Content $last_profile_cache -Force
	
	Return $upgrade_option, $options_cache
}

# Get last profile id from cache if user changed from default profile from other one (in case of multiple profiles)
function Get-Profileid ($profiles)
{
	if ($profiles.Code -eq 0)
	{
		if (!(Test-Path $last_profile_cache))
		{
			# Actual profile id
			# default profile (the first by default)
			$global:profileid, $profilename = defaultProfile $profiles
			
		}
		else
		{
			$profile_info = Get-Content $last_profile_cache
			$global:profileid = $profile_info[0]
			$profilename = $profile_info[1]
			
			Try
			{
				$json = (get-profile $profileid).Message | ConvertFrom-Json
				if ($json.Name -ne $profilename)
				{
					log2 "[CLIENT] !! GURU MEDITATION !! Last profile $profilename with id $profileid that was chosen by $username has changed name. Switching to default profile from profile list." "error"
					$global:profileid, $profilename = defaultProfile $profiles
				}
				elseif ($profileid -notin ($profiles.Message | ConvertFrom-Json).Id)
				{
					log2 "[CLIENT] !! GURU MEDITATION !! Last profile $profilename with id $profileid that was chosen $username was removed for $env:COMPUTERNAME, $username. Switching to default profile from profile list." "error"
					$global:profileid, $profilename = defaultProfile $profiles
				}
				else
				{
					log2 "[CLIENT] Setting last profile [$profilename on id $profileid] that was chosen by $username." "info"
					
				}
			}
			Catch
			{
				log2 "[CLIENT] !! GURU MEDITATION !! Last profile $profilename with id $profileid that was chosen by $username was removed from parx server. Switching to default profile from profile list." "error"
				$global:profileid, $profilename = defaultProfile $profiles
			}
			
		}
		# Profile id cache
		$profile_cache = "C:\Users\$userName\appdata\roaming\Parx\$profileid.json"
		Return $profileid, $profilename, $profile_cache
	}
	Else
	{
		if ($first_logon -eq $false)
		{
			$profile_info = Get-Content $last_profile_cache
			$global:profileid = $profile_info[0]
			$profilename = $profile_info[1]
		}		
		$global:noprofile = $true
		
	}
}

# Function for shortcut autolaunch
function AutoLaunch ($autolaunch)
{
	if ($autolaunch.Soft.Count -gt 0)
	{
		$block = @"
"@
		foreach ($item in $autolaunch.Soft.GetEnumerator())
		{
			$process = [io.path]::GetFileNameWithoutExtension($item.Process)
			# Check if software isn't already launched
			if ($process -notin ((Get-Process -Name "*$process*").Name | Where-Object {$_.SI -ne $sessionid}))
			{
				Log2 "[AUTOLAUNCH] Shortcut $($item.Name) is not launched, autolaunching it"
				$block = $block + "`r`nStart-Process -FilePath '$($item.Target)'"
			}
			else
			{
				Log2 "[AUTOLAUNCH] Shortcut $($item.Name) is already launched, skipping it"				
			}			
		}
		
		$scriptblock = [scriptblock]::Create($block)
		invoke-ascurrent_user -UseWindowsPowerShell -scriptblock $scriptblock -NoWait
		$autolaunch.Soft = @()
	}
}

# Set parx variables that will be used in each runspace
function Set-ParxVariables ($switch_profile, $first_logon, $noDesktop)
{
	# Synchronise an array that will get shortcuts for which autolaunch is set, to execute them after profile is loaded
	$global:autolaunch = [hashtable]::synchronized(@{ })
	$autolaunch.Soft = @()
	# Create a random event id for windows (parx) event logging, so that error tracking knows which one to monitor (excluding event 9999 as it's a restart order for service)
	$global:eventid = Get-Random -Minimum 1 -Maximum 9998
	# Splat of ordered autoloads that will be injected in the runspace pool
	$global:autoloads = [ordered]@{
		autoload_options  = $autoload_options		
		autoload_taskbar  = $autoload_taskbar
		autoload_shares   = $autoload_shares
		autoload_printers = $autoload_printers
		autoload_desktop	  = $autoload_desktop
		autoload_associations = $autoload_associations
	}
	# Splat of variables that will be injected in the runspace pool
	$global:variables = @{
		hostname	   = $env:computername
		userMSID	   = $userMSID
		username	   = $userName
		address	       = $address
		sessionid	   = $sessionid
		Source		   = $source
		Wp			   = $wp
		Eventid	       = $eventid
		refresh	       = $refresh
		Profile_cache  = $profile_cache
		Folder_cache   = $folder_cache
		noDesktop 	   = $noDesktop
		Switch_profile = $switch_profile
		options_cache  = $options_cache
		upgrade_option = $upgrade_option
		first_logon    = $first_logon
		debugmode	   = $global:debugmode
		debuglevel     = $global:debug_level
		logontype	   = $logontype
	}
	Return $autoloads, $variables
}
# Set the user options cache
function Set-Cache ($hash_options, $profile_cache)
{
	$hash_options | ConvertTo-Json | Set-Content $profile_cache -Force -Encoding UTF8 -ErrorAction stop
}

# Gets token for api call
function Get-Token
{
	$regkeypath = "Registry::\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ParxShell"
	$RegValue = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).To
	$key = (3, 4, 2, 3, 56, 34, 254, 222, 1, 1, 2, 23, 42, 54, 33, 233, 1, 34, 2, 7, 6, 5, 35, 43)
	$Decrypt = $RegValue | ConvertTo-SecureString -Key $key
	$Code = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($Decrypt))
	Return $code
}

# NOT USED ANYMORE Check if new value for option is found (to prevent reapplying same values over and over, hence a restart explorer )
<#function Check-Done
{
	[CmdletBinding()]
	Param (
		$name,
		$value,
		$options_cache)
	
	If ($name -eq "NONE")
	{
		log2 "[CLIENT] First logon for $username" 0 1 "info"
		$false
	}
	else
	{
		Write-Host "should check $name for $value"
		if (($options_cache | Where-Object { $_.Name -eq $name }).Value -ne $value)
		{
			$false
		}
		else
		{
			$true
		}
	}
	
}#>


# the embeded POST rest function
Function restPOST
{
	[CmdletBinding()]
	Param (
		[string]$uri,
		$body,
		[string]$api_token
	)
	
	if ($webproxy -eq "")
	{
		try
		{
			if ($api_token -ne "")
			{
				return Invoke-RestMethod -Method Post -Uri $uri -Body $body -Headers @{ Accept = "application/json"; Authorization = "Bearer $api_token" }
			}
			else
			{
				return Invoke-RestMethod -Method Post -Uri $uri -Body $body
				
			}
		}
		catch
		{
			$msg = $_.Exception.Message
			Log2 "[API] Error on api call : $msg" "error"
		}
		
	}
	else
	{
		try
		{
			if ($api_token -ne "")
			{
				return Invoke-RestMethod -Method Post -Uri $uri -body $body -proxy $webproxy -Headers @{ Accept = "application/json"; Authorization = "Bearer $api_token" }
			}
			else
			{
				return Invoke-RestMethod -Method Post -Uri $uri -body $body -proxy $webproxy
				
			}
		}
		catch
		{
			$msg = $_.Exception.Message
			Log2 "[API] Error on api call : $msg" "error"
		}
	}
}



# the embeded GET rest function
Function restGET ($uri)
{
	
	if ($webproxy -eq "")
	{
		try
		{
			return Invoke-RestMethod -uri $uri -Headers @{ Accept = "application/json" } -TimeoutSec 30
		}
		catch
		{
			return $null
		}
		
	}
	else
	{
		try
		{
			return Invoke-RestMethod -uri $uri -proxy $webproxy -Headers @{ Accept = "application/json" } -TimeoutSec 30
		}
		catch
		{
			return $null
		}
	}
}

# Update last profile on server
Function Update-ServerProfile
{
		$url = "$address/hosts/update-last-profile-name"
		$body = @{
		hostName		  = $env:computername
		last_profile_name = $profilename
	}
	
	return restPOST -uri $url -body $body -api_token $token
}

# get options list from server
Function get-options-list ($profileid)
{
	return restGET "$address/rest_options/$profileid"
}

# get json and return array
Function json-array ($json)
{
	try
	{
		if ($json -ne "")
		{
			[System.Reflection.Assembly]::LoadWithPartialName("System.Web.Extensions");
			$ser = New-Object System.Web.Script.Serialization.JavaScriptSerializer;
			$list = $ser.DeserializeObject($json);
			return $list;
		}
		else
		{
			return $null;
		}
	}
	catch
	{
		$ErrorMessage = $_.Exception.Message
		log2 "$ErrorMessage ($FailedItem)" "error"
	}
	
}



# test server
Function test-server ()
{
	return restGET "$address/rest"
}



# select the profile id from the db
Function get-profiles ([string]$env:computername, [string]$userLogin)
{
	return restGET "$address/rest_profiles/$env:computername/$userLogin"
}

# get the profile by its id
Function get-profile ($profileid)
{
	return restGET "$address/rest_profile/$profileid"
}

# get profile options for cache if user logs for the first time on host
function get-profile_options ($profileid)
{
	$json = get-options-list $profileid
	if ($json.code -eq 0)
	{
		$list = ConvertFrom-JSON $json.message
		$hash_options = @()
		$hash_options += @{ Name = "Revision"; Value = $revision; Done = "1" }
		foreach ($option in $list)
		{
			if ($option.name -eq "disableDesktop" -and $option.pivot.parameterValue -eq 1)
			{
				$global:noDesktop = $true
			}
			$hash_options += @{ Name = $option.name; Value = $option.pivot.parameterValue; Done = "0" }
		}
		Set-Cache $hash_options $profile_cache
		$hash_options = $hash_options | ConvertTo-Json | ConvertFrom-Json
		Return $hash_options
	}
	else
	{
		$global:noDesktop = $false
	}
}

# If options are changed on server (revision upgraded), check which one has changed and set its Done value to 0 to be processed again in option runspace
function upgrade_cache ($profileid)
{
	
	$json = get-options-list $profileid
	if ($json.code -eq 0)
	{
		$list = ConvertFrom-JSON $json.message
		$hash_options = @()
		$hash_options += @{ Name = "Revision"; Value = $revision; Done = "1" }
		foreach ($option in $list)
		{
			$hash_options += @{ Name = $option.name; Value = $option.pivot.parameterValue; Done = "1" }
			if ($option.name -eq "disableDesktop" -and $option.pivot.parameterValue -eq 1)
			{
				$global:noDesktop = $true
			}
		}
		$hash_options = $hash_options | ConvertTo-Json | ConvertFrom-Json
		compare-object -referenceobject $options_cache -differenceobject $hash_options -Property value -PassThru | Where-Object { $_.SideIndicator -eq '=>' } | ForEach-Object {
			
			$old = $options_cache[($options_cache.Name.IndexOf("$($_.Name)"))]
			if ($DebugPreference -eq "Continue")
			{
				Write-EventLog -LogName "Parxshell" -Source "Gui" -EventID $eventid -EntryType Information -Message "[CLIENT] $($_.Name) has changed from old value $($old.Value) to new value $($_.Value) |" -Category 1 -RawData 10, 20
			}
			log2 "[CLIENT] $($_.Name) has changed from old value $($old.Value) to new value $($_.Value)" "warn"
			$old.Done = "0"
			$old.Value = $_.Value
			
		}
		$options_cache | Select-Object Name, Value, Done | ConvertTo-Json | Set-Content $profile_cache -Force -Encoding UTF8 -ErrorAction stop
		$options_cache = $options_cache | ConvertTo-Json | ConvertFrom-Json
		Return $options_cache
	}
}
# Host info that will be send to server
Function Get-HostInfo
{
	$hostId = (get-hostid $env:computername).message
	$wmiOS = Get-WmiObject -ComputerName "." -Class Win32_OperatingSystem;
	if ($wmiOS.ServicePackMajorVersion -eq 0) { $sp = "" }
	else { $sp = " SP" + $wmiOS.ServicePackMajorVersion }
	$hostOS = $wmiOS.Caption + $sp
	Return $hostId, $hostOS
}

# select the host id from the db
Function get-hostid
{
	return restGET "$address/rest_hostid/$env:computername"
}

# select the default profile (the first in the list)
Function defaultProfile ($profiles)
{
	
	if ($profiles.code -eq 0)
	{
		
		#$list = json-array $profiles.message
		$list = ConvertFrom-JSON $profiles.message
		
		foreach ($profile in $list)
		{
			if ($profile.id)
			{
				return $profile.id, $profile.Name
			}
		}
	}
	else
	{
		return 0
	}
	
}

# Acl on file/folder for sercol prof / eleve / admin
function Set_Acl
{
	[CmdletBinding()]
	Param (
		[String]$folder,
		[hashtable]$rights,
		[String]$Type,
		[boolean]$Allow = $false,
		[Boolean]$Deny = $false,
		[int]$x = 0,
		[int]$y = 0,
		[String]$param_deny = "",
		[String]$param_allow = "",
		[switch]$inherit,
		[switch]$User
	)
	# Inheritance flags
	if ($inherit)
	{
		$propagation = "ContainerInherit,ObjectInherit"
	}
	else
	{
		$propagation = "None,None"
	}
	try
	{
		# Set Acl on parameters set in function call		
		#Get the folder existing acl
		$acl = Get-Acl -Path $folder
		#Remove any existing rules (that are not inherited, hence the ones previously set), to prevent Allow and Deny and the same time
		$acl.Access |
		Where-Object { $_.IdentityReference -eq "SERCOL\$username" -or $_.IdentityReference -eq "SERCOL\Professeur" -or $_.IdentityReference -eq "SERCOL\Eleve" -or $_.IdentityReference -eq "SERCOL\Personnels administratifs" } |
		Foreach-Object { $acl.RemoveAccessRule($_) } |
		Out-Null
		#Get the rights 
		foreach ($right in $rights.GetEnumerator())
		{
			if ($right.Value -eq "Deny")
			{
				#Here we iterate x / y  to know wether a single right or not is set (need the "," if multiple rights)
				$x++
				$deny = $true
				if ($x -eq 1)
				{
					$param_deny = $param_deny + $right.Name
					
				}
				else
				{
					$param_deny = $param_deny + "," + $right.Name
				}
			}
			else
			{
				$y++
				$allow = $true
				if ($y -eq 1)
				{
					$param_allow = $param_allow + $right.Name
					
				}
				else
				{
					$param_allow = $param_allow + "," + $right.Name
				}
			}
		}
		#If deny / allow, get type (file or folder) to set the right filesystem accessrule
		If ($deny -eq $true)
		{
			If ($type -eq "File")
			{
				if ($user)
				{
					$single = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\$username", "$param_deny", "Deny")
				}
				else
				{
					$prof = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Professeur", "$param_deny", "Deny")
					$eleve = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Eleve", "$param_deny", "Deny")
					$admin = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Personnels administratifs", "$param_deny", "Deny")
				}
			}
			else
			{
				if ($user)
				{
					$single = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\$username", "$param_deny", "$propagation", "None", "Deny")
				}
				else
				{
					$prof = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Professeur", "$param_deny", "$propagation", "None", "Deny")
					$eleve = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Eleve", "$param_deny", "$propagation", "None", "Deny")
					$admin = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Personnels administratifs", "$param_deny", "$propagation", "None", "Deny")
				}
			}
			if ($user)
			{
				$acl.SetAccessRule($single)
			}
			else
			{
				$acl.SetAccessRule($prof)
				$acl.SetAccessRule($eleve)
				$acl.SetAccessRule($admin)
			}
		}
		if ($allow -eq $true)
		{
			if ($type -eq "File")
			{
				if ($user)
				{
					$single = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\$username", "$param_allow", "Allow")
					
				}
				else
				{
					$prof = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Professeur", "$param_allow", "Allow")
					$eleve = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Eleve", "$param_allow", "Allow")
					$admin = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Personnels administratifs", "$param_allow", "Allow")
				}
				
			}
			else
			{
				if ($user)
				{
					$single = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\$username", "$param_allow", "$propagation", "None", "Allow")
				}
				else
				{
					$prof = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Professeur", "$param_allow", "$propagation", "None", "Allow")
					$eleve = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Eleve", "$param_allow", "$propagation", "None", "Allow")
					$admin = New-Object System.Security.AccessControl.FileSystemAccessRule("SERCOL\Personnels administratifs", "$param_allow", "$propagation", "None", "Allow")
				}
			}
			if ($User)
			{
				$acl.SetAccessRule($single)
			}
			else
			{
				#Set the rules on the acl
				$acl.SetAccessRule($prof)
				$acl.SetAccessRule($eleve)
				$acl.SetAccessRule($admin)
			}
		}
		#Set the acl on the folder/file	
		Set-Acl -path $folder -AclObject $acl -ErrorAction SilentlyContinue
		
	}
	catch
	{
		log2 "[SERVICE] ACL not set on $PSScriptRoot, error : $($_.Exception.Message)" "error"
		
	}
}

# Write debug logs on the host ui console
function Write-Debug ($hash, $fontcolor, $msg)
{
	$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")
	try
	{
		# Wait until the mutex is acquired
		$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
		# Write to the log file
		$hash.host.Ui.WriteLine($fontcolor, 'Black', "$msg |")
		
	}
	finally
	{
		# Release the mutex
		$mutex.ReleaseMutex()
		$mutex.Dispose()
	}
}

function Clear-CurrentLog()
{
	Set-Content -Path "C:\parxshell\Logs\Current\session.log" ""
}



# Read log file using CmTrace (prevents user from deleting logs as well as better reading)
function Read-Log()
{
	[CmdletBinding()]
	param (
		[Parameter(Mandatory = $true)]
		[string]$logfile
	)
	$scriptblock = [ScriptBlock]::create("Start-Process -FilePath 'C:\parxshell\Bin\CMTrace.exe' $logfile")
	# We invoke as current user so that notepad doesn't have SYSTEM rights eheh
	Invoke-AsCurrent_User -ScriptBlock $scriptblock
}

# log (date + event) $level 0,1,2, $output to show log on output screen
Function log2
{
	[CmdletBinding()]
	Param (
		$value,
		$type,
		$first
	)
	$logfile = "C:\parxshell\Logs\parx.log"
	$currentSession = "C:\parxshell\Logs\Current\session.log"
	
	if ($first)
	{
		if (Test-Path $logfile)
		{
			# Split log file into archive if size exceeds 100kb
			if ((Get-Item $logfile).length -gt 100kb)
			{
				$filenamedate = get-date -Format 'dd-MM-yy HH.mm.ss'
				$archivelog = ($PSScriptRoot + "\" + "Logs" + "\" + "Archives" + "\" + "Parx" + '.' + $filenamedate + '.archive').Replace('/', '-')
				copy-item $logfile -Destination $archivelog
				Remove-Item $logfile -force
				Set_Acl -Folder $PSScriptRoot -logs -inherit				
			}
		}
	}
	# output to debug console only
	if ($DebugPreference -eq 'Continue')
	{
		# Coloring depending on information level
		# LogMutex means we block the console object while we're writing on it, to prevent synchronous calls from runspaces
		switch ($type)
		{
			
			"success" {
				$index = $value.IndexOf(']') + 1
				$value = $value.Insert($index, " [SUCCESS]")
				Write-Debug $hash 'Green' $value
			}
			"error" {
				$index = $value.IndexOf(']') + 1
				$value = $value.Insert($index, " [ERROR]")
				Write-Debug $hash 'Red' $value
			}
			"warn" {
				$index = $value.IndexOf(']') + 1
				$value = $value.Insert($index, " [WARN]")
				Write-Debug $hash 'Yellow' $value
			}
			"task" {
				$index = $value.IndexOf(']') + 1
				$value = $value.Insert($index, " [TASK]")
				Write-Debug $hash 'Gray' $value
			}
			"registry" {
				$index = $value.IndexOf(']') + 1
				$value = $value.Insert($index, " [REGISTRY]")
				Write-Debug $hash 'Magenta' $value
			}
			"start" {
					$index = $value.IndexOf(']') + 1
					$value = $value.Insert($index, " [INFO]")
					Write-Debug $hash 'DarkGreen' $value
				}
				default {
					$index = $value.IndexOf(']') + 1
					$value = $value.Insert($index, " [INFO]")
					Write-Debug $hash 'White' $value
				}
				
			}
		}
	
	# output to parxlog.txt
	else
	{
		switch ($type)
		{
			"success" { $msg = "[SUCCESS] $value" }
			"error" { $msg = "[ERROR] $value" }
			"warn" { $msg = " [WARN] $value" }
			"task" { $msg = " [TASK] $value" }
			default { $msg = "[INFO] $value" }
		}
		# current data+time
		$now = (Get-Date).ToString('dd/MM/yyyy HH:mm:ss')
		
			$mutex = New-Object System.Threading.Mutex($false, "Global\LogMutex")			
			try
			{
				# Wait until the mutex is acquired
				$mutex.WaitOne([TimeSpan]::FromSeconds(5)) | Out-Null
				# Write to the log file
				Add-Content $logfile, $currentSession "$now $msg" -Force -Encoding UTF8 -ErrorAction SilentlyContinue
				
		
			}
			finally
			{
				# Release the mutex
				$mutex.ReleaseMutex()
				$mutex.Dispose()
			}
		
	}
}

# clear log file
Function clear-log ()
{
	Set-Content $logfile "" -Force -Encoding UTF8
	log2 "[CLIENT] Clear Log file" "info"
}

# Update the parx icon according to the errors/warning from log event
function UpdateSystemTrayIcon
{
	[CmdletBinding()]
	Param (
		[System.Collections.Generic.List[string]]$errors,
		[switch]$single,
		[switch]$clear,
		[string]$module,
		$objContextMenu,
		$objNotifyIcon
	)
	if (!$clear)
	{
		# Logic for the GUI to react to errors
		if ($errors.Count -gt 0)
		{
			$errors = $errors -join [Environment]::NewLine			
			if (!$single)
			{
				$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_yellow.ico"
				$button10.Enabled = $true
				if ($show_notification -eq 1)
				{
					$objNotifyIcon.BalloonTipText = $errors
					$objNotifyIcon.ShowBalloonTip($Miliseconds)
				}
				$Text = "Profil : $profilename chargé !"
				# Associate errors for each parx module (desktop, printer, share, etc)
				$shortcutErrors = ($errors -split '\r?\n').Trim() | Select-String -Pattern '\[SHORTCUT\]' | Select-Object -ExpandProperty Line
				if ($shortcutErrors.Count -gt 0)
				{
					$loadShortcut.Image = $pngError
					$loadShortcut.ToolTipText = $shortcutErrors -join [Environment]::NewLine
				}
				else
				{
					$loadShortcut.Image = $pngNoError
				}
				
				$shareErrors = ($errors -split '\r?\n').Trim() | Select-String -Pattern '\[SHARES\]' | Select-Object -ExpandProperty Line
				if ($shareErrors.Count -gt 0)
				{
					$loadShares.Image = $pngError
					$loadShares.ToolTipText = $shareErrors -join [Environment]::NewLine
				}
				else
				{
					$loadShares.Image = $pngNoError
				}
				
				$printerErrors = ($errors -split '\r?\n').Trim() | Select-String -Pattern '\[PRINTERS\]' | Select-Object -ExpandProperty Line
				if ($printerErrors.Count -gt 0)
				{
					$loadPrinters.Image = $pngError
					$loadPrinters.ToolTipText = $printerErrors -join [Environment]::NewLine
				}
				else
				{
					$loadPrinters.Image = $pngNoError
				}
				
				$taskbarErrors = ($errors -split '\r?\n').Trim() | Select-String -Pattern '\[TASKBAR\]' | Select-Object -ExpandProperty Line
				if ($taskbarErrors.Count -gt 0)
				{
					$loadtaskbar.Image = $pngError
					$loadtaskbar.ToolTipText = $taskbarErrors -join [Environment]::NewLine
				}
				else
				{
					$loadtaskbar.Image = $pngNoError
				}
			}
			else
			{
				$pattern = "\[$module\]"
				$selectedModule = Get-Variable -Name "load$module" -ValueOnly
				
				$moduleError = ($errors -split '\r?\n').Trim() | Select-String -Pattern $pattern | Select-Object -ExpandProperty Line
				if ($moduleError.Count -gt 0)
				{
					$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_yellow.ico"
					$button10.Enabled = $true
					if ($show_notification -eq 1)
					{
						$objNotifyIcon.BalloonTipText = $moduleError
						$objNotifyIcon.ShowBalloonTip($Miliseconds)
					}
					$objNotifyIcon.Text = "Profil : $profilename chargé !"
					$selectedModule.Image = $pngError
					$selectedModule.ToolTipText = $moduleError -join [Environment]::NewLine
				}
				else
				{
					$selectedModule.Image = $pngNoError
					$button10.Enabled = $false
					if ($isprofileadmin -eq 1)
					{
						$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_blue.ico"
						$objNotifyIcon.Text = "Mode admin de profil : $profilename chargé !"
					}
					elseif ($profileid -eq "9999")
					{
						$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_grey.ico"
						$objNotifyIcon.Text = "Profil: Aucun profil"
					}
					else
					{
						$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_green.ico"
						$objNotifyIcon.Text = "Profil : $profilename chargé !"
					}
				}
			}
			
		}
		else
		{
			$button10.Enabled = $false
			if ($isprofileadmin -eq 1)
			{
				$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_blue.ico"
				$objNotifyIcon.Text = "Mode admin de profil : $profilename chargé !"
			}
			elseif ($profileid -eq "9999")
			{
				$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_grey.ico"
				$objNotifyIcon.Text = "Profil: Aucun profil"
			}
			else
			{
				$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_green.ico"
				$objNotifyIcon.Text = "Profil : $profilename chargé !"
			}
			$loadtaskbar.Image = $pngNoError
			$loadPrinters.Image = $pngNoError
			$loadShares.Image = $pngNoError
			$loadShortcut.Image = $pngNoError
		}
	}
	else
	{
		$loadShortcut.Image = $null
		$loadShortcut.ToolTipText = $null
		
		$loadShares.Image = $null
		$loadShares.ToolTipText = $null
		
		$loadPrinters.Image = $null
		$loadPrinters.ToolTipText = $null
		
		$loadtaskbar.Image = $null
		$loadtaskbar.ToolTipText = $null
		
		$objNotifyIcon.Icon = "$PSScriptRoot\Resources\parxshell_grey.ico"
		$objNotifyIcon.Text = "Profil: Aucun profil"
	}
}

# Monitor event log to get parx errors on printers and shares and shortcuts
function Get-ParxError
{
	[CmdletBinding()]
	Param (
		$eventid,
		[switch]$single,
		[string]$module,
		$objContextMenu,
		$objNotifyIcon,
		[switch]$clear
		
	)
	# Wether runspaces are launched, or single task
	if (!($single))
	{
		$listen = $autoload_shares + $autoload_printers + $autoload_taskbar + $autoload_desktop + $autoload_associations
	}
	else
	{
		$listen = 1
	}
	# Start waiting for profile to be loaded
	$wait = $true
	
	$maxRetries = 30
	$retryCount = 0	
	
	do
	{
		Start-Sleep -Seconds 1
		# We compare the number of runspaces that are done loading, with the number of runspaces that needs to be monitored
		$finished = (Get-WinEvent -FilterHashtable @{ LogName = 'Parxshell'; ProviderName = "Gui"; Id = $eventid; Level = "3" } -ErrorAction SilentlyContinue).Count
		
		if ($finished -lt $listen)
		{
			$wait = $true
			$retryCount++
			
			if ($retryCount -eq $maxRetries)
			{
				$wait = $false
			}
		}
		else
		{
			$wait = $false
		}
	}
	while ($wait -eq $true)
	# Clear is used when Unloading a profile, no need to gets the errors
	if (!$clear)
	{
		# Grab profile errors
		$eventLog = New-Object System.Diagnostics.EventLog("Parxshell")
		$errors = $eventLog.Entries | Where-Object { $_.Source -eq "Gui" -and $_.InstanceId -eq $eventid -and $_.EntryType -eq "Error" } | Select-Object -ExpandProperty Message
		
		if (!($single))
		{
			UpdateSystemTrayIcon -errors $errors -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon
		}
		else
		{
			UpdateSystemTrayIcon -errors $errors -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon -single -module $module
		}
	}
	else
	{
		UpdateSystemTrayIcon -clear -objContextMenu $objContextMenu -objNotifyIcon $objNotifyIcon
		
	}
	
}

