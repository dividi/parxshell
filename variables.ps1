# Initiale state for variable switch_profile (which will be used in case of multi profile, when selecting another profile from GUI)
$switch_profile = $false
$global:noprofile = $false
$global:debugmode = $false
$global:token = Get-Token
# User and last profile cache
$folder_cache = "C:\Users\$userName\appdata\roaming\Parx\"
$last_profile_cache = "C:\Users\$userName\appdata\roaming\Parx\last_profile.txt"
$profiles = get-profiles $env:computername $userName
$global:profileid, $global:profilename, $global:profile_cache = Get-Profileid $profiles

if ($noprofile -eq $false)
{
	# Check if user logs for the first time on host
	if (!(Test-Path -Path $folder_cache))
	{
		$first_logon = $true
	}
	else
	{
		$first_logon = $false
	}
	
	
	# Gets options from profile
	$upgrade_option, $options_cache = Set-ProfileCache $profileid $profile_cache
}
else
{
	# Check if user logs for the first time on host
	if (!(Test-Path -Path $folder_cache))
	{
		$first_logon = $true
	}
	else
	{
		$first_logon = $false
	}
}


