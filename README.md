
## PARXSHELL par DAVID LE VIAVANT ##

Ce logiciel, écrit en Powershell permet d'appliquer les informations envoyées par un serveur Parx (https://gitlab.com/dividi/parx2).
Ce logiciel est sous licence GPLv2. Il est fourni "tel quel" et sans garantie d'aucune sorte.

## INSTALLATION DU CLIENT ##

1. Avec un compte admin, copiez les fichiers du client dans c:\parxshell

2. Ouvrir le fichier config.ps1 (dans le C:\Parxshell) et inscrire l'adresse du serveur Parx

3. Ouvrez une console en mode administrateur (clic droit, lancez en tant qu'admin) et lancez service-install.ps1. Cette opération va installer et démarrer le service parxshell.

Prérequis :

- POWERSHELL 4 (et donc Framework .net 4.5)
