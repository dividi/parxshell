﻿param (
	$username,
	$userMSID,
	$sessionID,
	$logontype,
	[switch]$from_service,
	[switch]$reloadConfig
)
# GENERAL COMMENT
# You'll find many "debug" and "debugpreferences" conditions throughout Parx functions. They will only be executed in debug mode.
# Debug mode will either write to parxgui event log, or directly writes in the debug console (depending wether they are executed from system account, or from user session)
$DebugPreference = 'SilentlyContinue'
$osVersion = [Environment]::OSVersion.Version

# Clean up system tray in case of unlock or switch session
if ($logontype -ne "logon")
{
	Start-Process -FilePath "$PSScriptRoot\Bin\SystemTrayRefresh.exe" -WindowStyle Hidden
}



# Import required functions
. $PSScriptRoot\class-c.ps1
. $PSScriptRoot\libs.ps1
. $PSScriptRoot\config.ps1

log2 "[CLIENT]Version $version" "info" "First"

# try to contact server, setting a maxAttempts (Clearpass)
$testserver = $null
$maxAttempts = 30
$attempt = 0
$global:ServerError = $false
log2 "[CLIENT] Connecting with Parx server on address: $address" "info"
while ($testserver -eq $null -and $attempt -le $maxAttempts)
{
	$testserver = test-server
	if ($testserver -eq $null)
	{
		Start-Sleep -Seconds 1
		$attempt++
		log2 "[CLIENT] Problem while connecting to server, retrying : $attempt/$maxAttempts " "warn"
		
	}
}

if ($testserver -eq $null)
{
	log2 "[CLIENT] !! GURU MEDITATION !! Server is not reachable on $address." "error"
	$ServerError = $true
}

# if connection OK

. $PSScriptRoot\variables.ps1
. $PSScriptRoot\class-options.ps1
. $PSScriptRoot\class-printers.ps1
. $PSScriptRoot\class-shares.ps1
. $PSScriptRoot\class-shortcuts.ps1
. $PSScriptRoot\class-taskbar.ps1
. $PSScriptRoot\class-programs.ps1
. $PSScriptRoot\class-gui.ps1

#force full gui and no autoloads for profile admin, ati and pdt.ati
$profiles_admins_list = $profiles_admins.split(",").trim()

$userGroup = Get-ADUserGroups $username
Log2 "[GROUP IS $userGroup]" "error"
	if ($userGroup -Contains "GIC_ATI" -or $userGroup -like "PDT*" -or $profiles_admins_list -Contains $username)
	{
		log2 "[CLIENT] Hi Admin, Parxshell won't run for you !" "info"
		$full_gui = 1
		$autoload_desktop = 0
		$autoload_options = 0
		$autoload_printers = 0
		$autoload_shares = 0
		$autoload_taskbar = 0
		$autoload_associations = 0
		$noneShallPass = 0
		$global:isprofileadmin = 1
	}	


# Get all the variables needed 
$autoloads, $variables = Set-ParxVariables $switch_profile $first_logon $noDesktop
$max_task = ($autoloads.Values | Measure-Object -Sum).Sum

# Autoload only if a profile exists
if ($profiles.code -eq 0 -and $max_task -gt 0)
{
	log2 "[CLIENT] Loading profile $profilename for $env:computername, $username" "info"
	New-Runspace -Threads $max_task -Autoloads $autoloads -Autolaunch $autolaunch -variables $variables -profileid $profileid -verbose
	log2 "[CLIENT] Profile $profilename for $env:computername, $username is loaded" "success"
}
else
{
	if ($noneShallPass -eq "1")
	{
		log2 "[CLIENT] Kickout : No profile detected" "warn"
		#BalloonTip to kick users without profile
		[void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
		$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon
		$objNotifyIcon.Icon = "C:\parxshell\parxshell.ico"
		$objNotifyIcon.BalloonTipIcon = "Error"
		$objNotifyIcon.BalloonTipText = "Connexion sans profil interdite."
		$objNotifyIcon.BalloonTipTitle = "PARX : None Shall Pass"
		$objNotifyIcon.Visible = $True
		$objNotifyIcon.ShowBalloonTip(10000)
		Start-Sleep -Seconds 10
		start-process "shutdown.exe" -ArgumentList "-l -f"
	}
	else
	{
		if ($first_logon -eq $false -and $ServerError -eq $false)
		{
			log2 "[CLIENT] No profile detected for $env:COMPUTERNAME, $username. As a previous profile ($profilename) was set for $username, defaulting to Default profile ($profileid), removing everything parx ever done on the computer" "warn"
			UnLoad-All
			
		}
		else
		{
			log2 "[CLIENT] No profile detected for $env:COMPUTERNAME, $username. Parx was never launched for $username, nothing will happen." "warn"
			
		}
		
	}
}

# Launch shortcuts with autolaunch set in profile
Autolaunch $autolaunch
# Launch the GUI
# Wait for the start menu experience host (hence systray) process
# We set a wait variable to 0, that will increment 
$wait = 0
if (($osVersion.Major -eq 6 -and $osVersion.Minor -eq 3) -or ($osVersion.Major -ge 10))
{
	if ($osVersion.Major -ge 10)
	{
		# Windows 10 or newer
		Do
		{
			# We don't want to flood the log file, so we only log one time during the wait
			if ($wait -eq 0)
			{
				$wait++
				log2 "[CLIENT] Waiting for the Start Menu Experience host process to be running before launching parxshell system icon" "info"
			}
			
			Start-Sleep -Milliseconds 100
		}
		While (!(Get-Process -Name StartMenuExperienceHost -ErrorAction SilentlyContinue).SI -eq $sessionID)
	}
	else
	{
		# Windows 8.1
		Do
		{
			# We don't want to flood the log file, so we only log one time during the wait
			if ($wait -eq 0)
			{
				$wait++
				log2 "[CLIENT] Waiting for the explorer process to be running before launching parxshell system icon" "info"
			}
			
			Start-Sleep -Milliseconds 100
		}
		While (!(Get-Process -Name Explorer -ErrorAction SilentlyContinue).SI -eq $sessionID)
	}
}




log2 "[CLIENT] Running GUI" "info"
initIHM $profiles | Out-Null


