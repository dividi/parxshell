# Check Parxshell folder rights and set deny [read, write, delete] for eleve, prof, admin if check fails
function Check-ParxAcl
{
	
	if (!((Get-Acl -Path $PSScriptRoot).Access | Where-Object { $_.IdentityReference -like "*Professeur*" }))
	{
		log2 "[SERVICE] ACL not set on $PSScriptRoot, applying acl rules for professeur, eleve, admin" "warn"
		Set-ParxAcl
	}
	else
	{
		log2 "[SERVICE] ACL already set on $PSScriptRoot" "info"
		
	}
}

# Set ACL that will deny access (Modify) for Eleve, Professeur and Personnels administratifs on Parxshell folder, allowing some exceptions 
function Set-ParxAcl
{
	try
	{
		$rootPath = $PSScriptRoot
		$exclusions = @("C:\parxshell\Bin", "C:\parxshell\Extensions", "C:\parxshell\Logs", "C:\parxshell\Logs\Current", "C:\parxshell\class-c.ps1", "C:\parxshell\class-debug.ps1")
		$Groups = @("SERCOL\Eleve", "SERCOL\Professeur", "SERCOL\Personnels administratifs", "SERCOL\Documentaliste", "SERCOL\GIC_ATC", "SERCOL\ASH", "SERCOL\GIC_Stagiaire")
		
		# Apply deny permission to the root folder, files, and subfolders
		$rootAcl = Get-Acl -Path $rootPath
		
		foreach ($group in $Groups)
		{
			$denyRule = New-Object System.Security.AccessControl.FileSystemAccessRule($group, "Modify", "None", "None", "Deny")
			$rootAcl.AddAccessRule($denyRule)
		}
		
		Set-Acl -Path $rootPath -AclObject $rootAcl
		
		# Apply deny rule to files and folders except for allowed files and folders
		Get-ChildItem -Path $rootPath -Recurse -Force | ForEach-Object {
			$itemPath = $_.FullName
			$parentPath = Split-Path -Path $itemPath -Parent
			# Not excluded files and folders, apply DENY rule (with inheritance for folders)
			if ($parentPath -notin $exclusions -and $itemPath -notin $exclusions)
			{
				$itemAcl = Get-Acl -Path $itemPath
				if ($_.PSIsContainer)
				{
					foreach ($group in $Groups)
					{
						$denyRule = New-Object System.Security.AccessControl.FileSystemAccessRule($group, "Modify", "ContainerInherit, ObjectInherit", "None", "Deny")
						$itemAcl.AddAccessRule($denyRule)
					}
				}
				else
				{
					foreach ($group in $Groups)
					{
						$denyRule = New-Object System.Security.AccessControl.FileSystemAccessRule($group, "Modify", "None", "None", "Deny")
						$itemAcl.AddAccessRule($denyRule)
					}
				}
				
				Set-Acl -Path $itemPath -AclObject $itemAcl
			}
			# Excluded files and folders, apply ALLOW rule (ReadAndExecute, -Write (for Logs folder)-
			else
			{
				$itemAcl = Get-Acl -Path $itemPath
				
				if ($_.PSIsContainer)
				{
					if ($_.FullName -like "*Logs*" -or $_.FullName -like "*Current*")
					{
						$rights = "ReadAndExecute,Write"
					}
					else
					{
						$rights = "ReadAndExecute"
					}
					foreach ($group in $Groups)
					{
						$allowRule = New-Object System.Security.AccessControl.FileSystemAccessRule($group, $rights, "ContainerInherit, ObjectInherit", "None", "Allow")
						$itemAcl.AddAccessRule($allowRule)
					}
				}
				else
				{
					foreach ($group in $Groups)
					{
						$allowRule = New-Object System.Security.AccessControl.FileSystemAccessRule($group, $rights, "None", "None", "Allow")
						$itemAcl.AddAccessRule($allowRule)
					}
				}
				
				Set-Acl -Path $itemPath -AclObject $itemAcl
			}
		}
	}
	catch
	{
		Log2 "[SERVICE] Error while setting parx ACL : $($_.Exception.Message)"
	}
}

# Checks if Parx was just installed
function Check-FreshInstall
{
	$RegistryPath = "HKLM:\SOFTWARE\Parxshell"
	$name = "Initialized"
	$value = "1"
	
	if ((Get-ItemPropertyValue -Path 'HKLM:\SOFTWARE\ParxShell' -Name Initialized) -eq 0)
	{
		New-ItemProperty -Path $RegistryPath -Name $Name -Value $Value -PropertyType DWORD -Force
		return $true
	}
	else
	{
		return $false
	}
	
}

# Launch Parxshell into user session in case of fresh install
function Launch-Parxshell
{
	Try
	{
		# Launch Parxshell under active session if one is found
		$logontype = "Restart"
		$username = ((Get-CimInstance -ClassName Win32_ComputerSystem).Username) -replace '.*?\\(.*)', '$1'
		# Get SID from username
		$sid = (New-Object System.Security.Principal.NTAccount($username)).Translate([System.Security.Principal.SecurityIdentifier]).value
		# Get Session ID from explorer based on active username
		$sessionid = (Get-Process -name "explorer" -IncludeUserName | Select-Object -Property UserName, SI -First 1 | where-object { $_ -like "*$username*" }).SI
		# If an user is already logged in an active session, launch parx into its session using a restart message to Parxshell service
		log2 "[INSTALL] As Parx was just installed, trying to launch Parxgui for user $username with sessionid $sessionid on sid $sid" "Info"		
		$args = @"
		-nowait -session:$sessionid "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype -from_service" 
"@
		Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args		
	}
	catch
	{
		# We don't care, no user session is active, parx will be launched whenever someone logs in		
	}
}
# Gets token for api call
function Get-Token
{
	$regkeypath = "Registry::\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\ParxShell"
	$RegValue = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).To
	$key = (3, 4, 2, 3, 56, 34, 254, 222, 1, 1, 2, 23, 42, 54, 33, 233, 1, 34, 2, 7, 6, 5, 35, 43)
	$Decrypt = $RegValue | ConvertTo-SecureString -Key $key
	$Code = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($Decrypt))
	Return $code
}
# Disable welcome animation screen
function Disable-Welcome
{
	reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v "EnableFirstLogonAnimation" /t "REG_DWORD" /d "0" /f
}

function SpeedUp-Logon ($username)
{
	If (Test-Path "c:\Users\$username\appdata\roaming\parx")
	{
		reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "DelayedDesktopSwitchTimeout" /t "REG_DWORD" /d "7" /f
	}
	else
	{
		
		Remove-ItemProperty -Path "Registry::\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "DelayedDesktopSwitchTimeout" -ErrorAction SilentlyContinue | Out-null
		
	}
}
# Log off locked session
function Logoff-LockedSession
{
	$locked_session = (Get-Process -Name "Lockapp" -ErrorAction SilentlyContinue).SI
	If ($locked_session)
	{
		foreach ($session in $locked_session)
		{
			logoff $session
		}
	}
}

# Run tasks on computer startup and refresh (is set in config file)
function Run-Task
{
	# Get list from server
	$tasks = get-tasksList $hostid
	
	if ($tasks.code -eq 0)
	{
		$list = ConvertFrom-JSON $tasks.message
		
		foreach ($task in $list)
		{
			$name = $task.name
			$schparam = $task.pivot.schedule
			$id = $task.pivot.id
			$args = $task.args
			# Here we go. Switch must take wildcard for update tasks, as their names are Updatexyz123		
			switch -wildcard ($name)
			{
				"update*" {
					$version = $name.Split()[1]
					task-update -version $version
					$restart_service = $true
				}
				"delete-schTask" {
					task-delete $args
				}
				"shutdown" {
					task-shutdown $schparam $task
				}
				"reboot" {
					task-reboot $schparam $task
				}
				"logout" {
					task-logout $schparam $task
				}
				# Service is aware if somebody is logged, only do the Restart task (ie restart Parxui) if someone is logged
				{ $global:is_logged -eq $true -and $_ -eq "restart" }
				{
					Write-Host "Restart because someone is logged on"
					Task-Restart
				}
				# Defaulting to other tasks. Packages, and custom tasks
				default {
					if ($task.exe -eq "Package")
					{
						task-package -json $task -name $task.Name -schparam $schparam
					}
					else
					{
						task-execute $task
					}
				}
			}
			# Clean server tasks
			Clean-ServerTasks $task
		}
		Return $restart_service
	}
}

# Package execution
function Do-Package
{
	try
	{
		$zip = "$name.zip"
		# Download the zip file
		$webroot = $address -replace "/api", "/files/packages/$zip"
		Invoke-WebRequest $webroot -OutFile "C:\parxshell\$zip"
		log2 "[PACKAGE] ZIP file [$zip] download" "task"
		# Create a temp folder
		if (!(Test-Path "C:\parxshell\$name"))
		{
			New-Item -Path "C:\parxshell" -Name $name -ItemType "directory"
		}
		
		# Extract the zip file		
		$shell = new-object -com shell.application
		$zip_file = $shell.NameSpace("c:\parxshell\$zip")
		foreach ($item in $zip_file.items())
		{
			log2 "[PACKAGE] Extract file : $($item.name)" "task"
			$shell.Namespace("C:\parxshell\$name").copyhere($item, 1044)
		}
		# Delete zip file
		Remove-Item "C:\parxshell\$zip" -force
		log2 "[PACKAGE] Processing package task" "task"
		. "C:\parxshell\$name\init.ps1"
		log2 "[PACKAGE] Processing package done" "task"
		#reboot client
		if ($rebootAfterUpdate -eq 1)
		{
			restartClient
		}
		# Delete temp folder
		Remove-Item "C:\parxshell\$name" -recurse -force
		
	}
	catch
	{
		if ($_.Exception.Message -match "(404)")
		{
			log2 "[PACKAGE] No file found on server" "error"
		}
		else
		{
			log2 "[PACKAGE] Error while processing package $name : $($_.Exception.Message)" "error"
		}
	}
}
# Schedule or Run packages
function Task-Package 
{
	[CmdletBinding()]
	Param (
		$json,
		[string]$name,
		[string]$schparam
	)
	
	if (!([string]::IsNullOrWhiteSpace($schparam)))
	{
		$def = (Get-ChildItem "Function:Do-Package").Definition
		$def = $def.Replace("`$address", "`"$($address)`"").Replace("`$name", $name).Replace("log2", "#log2")
		Set-TaskScheduled -zip -json $json -block $def
	}
	else
	{
		Do-Package
	}
	Remove-Variable -Name Name	
}

# Delete tasks from server once client has run it
function Clean-ServerTasks ($task)
{
	
	if ($task.pivot.recurtion -eq 0 -and [string]::IsNullOrWhitespace($task.pivot.schedule))
	{
		$url = "$address/rest_deltask"
		$body = @{
			active_task_id = $task.pivot.id
		}
		restPOST -uri $url -body $body -api_token $token
		log2 "[SERVICE] Remove TASK [$id] $name on server" "info"
	}
}

# Enable / disable parxshell launch on login
function Set-ParxLaunch
{
	switch ($enable_parx)
	{
		"0" {
			log2 "[SERVICE] Parxshell is disabled (ati chose to do so from variable enable_parx in config.ps1)" "warn"
			Return $false
		}
		"1" {
			Return $true
		}
	}
}

# Enable security event (only success ) for session lock, session unlock, session switch, etc
function Set-Audit
{
	auditpol /set /Subcategory:"{0CCE921C-69AE-11D9-BED3-505054503030}" /success:enable /failure:disable
}

function Get-UserName ($sid)
{
	# Get username from registry 
	Try
	{
		# We have to wait a bit for the reg key to exists, since login event 7001 is fired before its creation
		$started = $false
		do
		{
			$status = Test-Path -Path "registry::HKEY_USERS\$sid\Volatile Environment"
			if (!($status)) { Start-Sleep -Milliseconds 100 }
			else { $started = $true }
		}
		until ($started)
		$userName = (Get-Itemproperty -LiteralPath "Registry::\HKEY_USERS\$sid\Volatile Environment").USERNAME
		Return $username
	}
	# IF domain isn't reachable (well, first of, do your work !! :p) or host is not part of a domain, get username from registry
	Catch
	{
		log2 "[SERVICE] Error while retrieving username : $($_.Exception.Message)"
	}
	
}

# Remove domainname\ from domainname\username
function Strip-Domain ($user)
{
	if ($user.IndexOf('\') -gt 0)
	{
		return $user.Substring(($user.IndexOf('\') + 1))
	}
	else
	{
		return $user
	}
}

# Runs a timer triggered if test-server returns $null (Clearpass)
function TryService
{
	$timerInterval = 10000 # Timer interval in milliseconds (e.g., 5000 = 5 seconds)
	# Create a timer event
	$timer = New-Object System.Timers.Timer
	$timer.Interval = $timerInterval
	
	# Register an event that will be triggered when the timer elapses
	$event = Register-ObjectEvent -InputObject $timer -EventName Elapsed -Action {
		
			# Access script and functions using the $using scope modifier
			Write-Host $using:myVariable
			
			# Perform the action you want when the condition is met
			# Add your code here
		
	}	
	# Start the timer
	$timer.Start()
}

# Create Parx log 
function Set-ParxLog
{
	If (!(Test-Path -Path "C:\Windows\System32\winevt\Logs\Parxshell.evtx"))
	{
		New-EventLog -Log ParxShell -Source Gui -ErrorAction SilentlyContinue
	}
}

# If server is not reachable, and host is a tablette, try and get the new numser if available
function Check-Numser
{
	$isTablette = (Get-Item "HKLM:\software\microsoft\windows\currentversion\group policy\state\machine" -ErrorAction SilentlyContinue).GetValue("Distinguished-Name") -like "*Tablettes*"
	if ($isTablette)
	{
		Log2 "[SERVICE] As the host is a tablette, will try to find a valid server address" "info"
		$service_Numser_Refresh = 10
		$configfile = "C:\parxshell\config.ps1"
		# Register a timer that will fire every 10 minutes
		Set-ServiceRefresh -ServiceRefresh $service_Numser_Refresh -SourceIdentifier "Check.Numser"
		Register-EngineEvent -SourceIdentifier "Check.Numser" -Action {
			# Get the numser from registered dns server on the active netword card
			$_numser = (Get-NetAdapter -ErrorAction SilentlyContinue | Where-Object { -not $_.Virtual -and $_.Status -eq 'up' }).ifIndex | Get-DnsClientServerAddress -AddressFamily IPv4 -ErrorAction SilentlyContinue | Select-Object -ExpandProperty ServerAddresses -First 1
			if (!([String]::IsNullOrEmpty($_numser)))
			{
				$numser = $_numser.Split(".")[2]
				$new_address = "http://10.231.$numser.110:8000/api"
				# New parx server address found
				if ($new_address -ne $actual_server_ip)
				{
					Log2 "[SERVICE] New address is found, it will be changed from $actual_server_ip to $new_address ! Trying to connect to the new server address." "warn"
					# Parx server address validation
					Try
					{
						(Invoke-RestMethod -uri "$new_address/rest" -Headers @{ Accept = "application/json" } -TimeoutSec 2)
						Log2 "[SERVICE] Parx server is reachable, new address $new_address is valid. Changing config.ps1 and restarting service." "success"
						# Set new server address in config file
						(Get-Content $configfile) -replace '\$address = "(.*)"', "`$address = `"$new_address""" | Set-Content  $configfile
						# Restart Parxshell service
						[System.Environment]::FailFast("Config update completed, restarting service...")
					}
					# New address is not valid
					Catch { Log2 "[SERVICE] Couldn't connect with the new Parx server address, config.ps1 won't be changed" "error" }
				}
			}
		}
		# Wait for the events
		while ($true)
		{
			$event = Wait-Event
			$event | Remove-Event
		}
	}
}

# Register a timer
function Set-ServiceRefresh
{
	param (
		[int]$ServiceRefresh,
		[string]$SourceIdentifier
	)
	
	$Timer = New-Object Timers.Timer
	$objectEventArgs = @{
		InputObject	     = $Timer
		EventName	     = 'Elapsed'
		SourceIdentifier = $SourceIdentifier
	}
	
	Register-ObjectEvent @objectEventArgs
	$interval = $ServiceRefresh
	$Timer.Interval = $interval * 60000
	$Timer.Enabled = $true
}

# Tell the service to listen for these events, and act accordingly
function Register-WinEvents
{
	[CmdletBinding()]
	Param (
		[int]$eventId,
		[string]$logname,
		[string]$SourceIdentifier
	)
	Clear-EventLog -LogName Parxshell -ErrorAction SilentlyContinue
	
	
	switch ($eventId)
	{
		# Log on, launch parxgui
		"7001" {
			$action = {
				Try
				{ 
					
					if ($event.MessageData -eq $true)
					{
						$logontype = "logon"
						$sessionid = $eventArgs.EventRecord.properties[0].Value
						$sid = $eventArgs.EventRecord.properties[1].Value
						$username = Get-UserName $sid
						log2 "[SERVICE] Log on for $username" "Info"
						# Speed up logon process if user session already exists
						#SpeedUp-Logon $username
						$args = @"
		-nowait -session:$sessionid "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype" 
"@
						Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args
						
						while (!(get-process powershell -ErrorAction SilentlyContinue | Where-Object { $_.SI -eq $sessionid }))
						{
							Write-Host "waiting for parxgui"
							Start-Sleep -Milliseconds 50
						}
						#(get-process powershell | Where-Object { $_.SI -eq $sessionid }).PriorityClass = [System.Diagnostics.ProcessPriorityClass]::AboveNormal
						$global:is_logged = $true
						# Speed up logon process if user session already exists
						#SpeedUp-Logon $username
					}
				}
				Catch
				{
					Write-Host $_.Exception.message
				}
			}
		}
		# Log off
		"7002" {
			$action = {
				log2 "[SERVICE] Log off" "Info"
				Clear-CurrentLog
				$global:is_logged = $false
				$global:switch = $false
			}
		}
		# Lock session, kill parxgui for the session id
		"4800" {
			$action = {
				$global:switch = $false
				Clear-CurrentLog
				Clear-EventLog -LogName Parxshell -ErrorAction SilentlyContinue
				if ($event.MessageData -eq $true)
				{
					log2 "[SERVICE] $($eventArgs.EventRecord.properties[1].value) has locked his session $($eventArgs.EventRecord.properties[4].value), killing parxgui" "info"
					$sessionid = $eventArgs.EventRecord.properties[4].value
					#(Get-Process -Name powershell -ErrorAction SilentlyContinue | Where-Object { $_.SI -eq $eventArgs.EventRecord.properties[4].value }) | ForEach-Object { Stop-Process -ProcessName $_.ProcessName -Force }
					Get-CimInstance Win32_Process -Filter "name = 'powershell.exe'" -ErrorAction SilentlyContinue | Select-Object  ProcessId, SessionID, CommandLine | Where-Object { $_.SessionId -eq $sessionid -and $_.CommandLine -notlike "*service.ps1*" } | ForEach-Object { Stop-Process -Id $_.ProcessId -force }
					$global:is_logged = $false
				}
			}
		}
		# Unlock session, relaunch parxgui for the session
		"4801" {
			$action = {
				Get-Config
				if ($event.MessageData -eq $true)
				{
					log2 "[SERVICE] $($eventArgs.EventRecord.properties[1].value) has unlocked his session, launching parxgui" "info"
					# Get user info from event
					$logontype = "unlock"
					$username = $eventArgs.EventRecord.properties[1].value
					$user = New-Object System.Security.Principal.NTAccount($username)
					$sessionid = $eventArgs.EventRecord.properties[4].value
					$sid = $user.Translate([System.Security.Principal.SecurityIdentifier])
					# Speed up logon process if user session already exists
					#SpeedUp-Logon $username
					$args = @"
		-nowait -session:$($eventArgs.EventRecord.properties[4].value) "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype -from_service" 
"@
					# check if switch back event has not been already launched on user switch by checking if a powershell parxgui instance is already running (which includes username in it)
					$is_launched = Get-CimInstance Win32_Process -Filter "name = 'powershell.exe'" -ErrorAction SilentlyContinue | Select-Object SessionID, CommandLine | Where-Object { $_.SessionId -eq $sessionid }
					
					if ([System.String]::IsNullOrEmpty($is_launched))
					{
						Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args
						# Setting parxshell priority to above normal when parxgui is launched
						<#while (!(get-process powershell -ErrorAction SilentlyContinue | Where-Object { $_.SI -eq $sessionid }))
						{
							Start-Sleep -Milliseconds 100
						}#>
						#(get-process powershell | Where-Object { $_.SI -eq $sessionid }).PriorityClass = [System.Diagnostics.ProcessPriorityClass]::AboveNormal
						$global:is_logged = $true
						# Speed up logon process if user session already exists
						#SpeedUp-Logon $username
					}
					
				}
			}
		}
		# User switched back, relaunch parxgui for the session
		"4778" {
			# We must set this variable to true, as windows fire two events on user switch (event switch, event unlock) to prevent parxgui to be launched twice
			$action = {
				Get-config
				if ($event.MessageData -eq $true)
				{
					log2 "[SERVICE] $($eventArgs.EventRecord.properties[0].value) has switched back to his session, launching parxgui" "info"
					# Get user info from event
					$logontype = "switchback"
					$username = $eventArgs.EventRecord.properties[0].value
					$user = New-Object System.Security.Principal.NTAccount($username)
					$sid = $user.Translate([System.Security.Principal.SecurityIdentifier])
					$sessionid = quser $username | select -skip 1 | %{ $_.SubString(41, 5).Trim() }
					# Speed up logon process if user session already exists
					#SpeedUp-Logon $username
					$args = @"
		-nowait -session:$sessionid "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype -from_service" 
"@
					Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args
					# Setting parxshell priority to above normal when parxgui is launched
					<#while (!(get-process powershell -ErrorAction SilentlyContinue | Where-Object { $_.SI -eq $sessionid }))
					{						
						Start-Sleep -Milliseconds 100
					}#>
				#(get-process powershell | Where-Object { $_.SI -eq $sessionid }).PriorityClass = [System.Diagnostics.ProcessPriorityClass]::AboveNormal
					$global:is_logged = $true
					# Speed up logon process if user session already exists
					#SpeedUp-Logon $username
				}
			}
		}
		# Domain policy deletes previous audits that are set by parxshell. Temporary fix until the plateforme change their settings accordingly.
		"4719" {
			$action = {
				Set-Audit
			}
		}
		# Restart parxgui (from GUI)
		"9999"
		{
			$action = {
				Clear-EventLog -LogName Parxshell -ErrorAction SilentlyContinue
				Clear-CurrentLog
				log2 "[SERVICE] Restarting Parxgui from user choice GUI" "Info"
				$event = $eventArgs.EventRecord.properties[0].Value
				$logontype = "Restart"
				$event = $event.Split("|")
				$sessionid = $event[0]
				$username = $event[1]
				$sid = $event[2]
				# Speed up logon process if user session already exists
				#SpeedUp-Logon $username
				Get-CimInstance Win32_Process -Filter "name = 'powershell.exe'" -ErrorAction SilentlyContinue | Select-Object  ProcessId, SessionID, CommandLine | Where-Object { $_.SessionId -eq $sessionid -and $_.CommandLine -notlike "*service.ps1*" } | ForEach-Object { Stop-Process -Id $_.ProcessId -force -ErrorAction SilentlyContinue }
				$args = @"
		-nowait -session:$sessionid "c:\Windows\System32\WindowsPowershell\v1.0\powershell.exe" -noprofile -Executionpolicy bypass -nologo -Noexit  -WindowStyle Hidden -file "c:\parxshell\parxgui.ps1 -username $username -userMSID $sid -sessionid $sessionid -logontype $logontype -from_service" 
"@
				Start-Process -WindowStyle hidden -FilePath c:\parxshell\bin\ServiceUI.exe -ArgumentList $args
				while (!(get-process powershell -ErrorAction SilentlyContinue | Where-Object { $_.SI -eq $sessionid }))
				{
					Write-Host "waiting for parxgui"
					Start-Sleep -Milliseconds 50
				}
				#(get-process powershell | Where-Object { $_.SI -eq $sessionid }).PriorityClass = [System.Diagnostics.ProcessPriorityClass]::AboveNormal
				log2 "[SERVICE] Parxgui restarted" "Info"
				# Speed up logon process if user session already exists
				#SpeedUp-Logon $username
			}
			
		}
		
	}
	# Forging the watcher
	$select = "*[System[(EventID=$eventId)]]"
	$query = [System.Diagnostics.Eventing.Reader.EventLogQuery]::new($logName, [System.Diagnostics.Eventing.Reader.PathType]::LogName, $select)
	$watcher = [System.Diagnostics.Eventing.Reader.EventLogWatcher]::new($query)
	$watcher.Enabled = $true
	Register-ObjectEvent -InputObject $watcher -EventName 'EventRecordWritten' -Action $action -SourceIdentifier $SourceIdentifier -MessageData $parx_launch
}

# Create a Parx folder in scheduled task 
function Set-ParxFolder
{
	$scheduleObject = New-Object -ComObject schedule.service
	$scheduleObject.connect()
	Try
	{
		$scheduleObject.GetFolder("Parx") | out-Null
	}
	Catch
	{
		$rootFolder = $scheduleObject.GetFolder("\")
		$rootFolder.CreateFolder("Parx") | out-Null
	}
}

# Put a task in Parx scheduled task library
function Set-TaskScheduled
{
	[CmdletBinding()]
	Param (
		$json,
		[string]$exe,
		[string]$params,
		[switch]$custom,
		[switch]$zip,
		$block,
		[boolean]$StartWhenAvailable
	)
	
	$id = $json.pivot.id
	$taskName = "parx-run$id"
	$schparam = $json.pivot.schedule
	# Check wether task is already scheduled
	if (!(Get-ScheduledTask -TaskName $taskName -TaskPath \Parx\ -ErrorAction SilentlyContinue))
	{
		log2 "[SERVICE] Scheduled task $($json.Name), $($json.Description) not set, setting it" "info"
		
		# If task isn't part of the server set (ie shutdown, restart, update, etc), grab task parameters from json, else use specified params from calling function
		# Custom Tasks
		if ($custom)
		{
			$exe = $json.exe
			$params = $json.args
			$command = "$exe $params"
		}
		# Update or package task
		elseif ($zip)
		{
			$command = $block
		}
		# Server set tasks
		else
		{
			$command = "$exe $params"
		}
		
		# Forge the trigger schedule
		$sch = $schparam.Split(";")
		# Trigger variations. $sch[0] is the type (weekly, once, atlogon, atstarttup), $sch[1] is the hour, $sch[2] are the days
		switch ($sch[0])
		{
			"-Weekly" {
				[dayofweek[]]$days = @()
				# We must forge an array of $days if multiple days are set
				If ($sch[2] -like "*,*")
				{
					$sch[2].Split(",") | ForEach-Object { $days += $_ }
				}
				# Single day
				else
				{
					$days += $sch[2]
				}
				$trigger = New-ScheduledTaskTrigger -Weekly -DaysOfWeek $days -At $sch[1]
			}
			"-Once" {
				$when = [string]::Join(" ", $sch[1], $sch[2])
				$at = [datetime]::ParseExact($when, "hh:mm yyyy-MM-dd", $null)
				$trigger = New-ScheduledTaskTrigger -Once -At $at
			}
			"-Atlogon" {
				$trigger = New-ScheduledTaskTrigger -AtLogOn
			}
			"-AtStartup" {
				$trigger = New-ScheduledTaskTrigger -AtStartup
			}
		}
		# We ensure that the scheduled task won't show sensitive information if a custom task is set 
		$encodedcommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($command))
		$cmd = "-EncodedCommand $($encodedcommand)"
		# Forging the task
		$action = New-ScheduledTaskAction -Execute "PowerShell" -Argument $cmd
		$principal = New-ScheduledTaskPrincipal -UserId "SYSTEM" -LogonType ServiceAccount
		# Settings for the task, essentially allowing power mode execution, and executing the task when available (if computer was off, except for shutdown, reboot, logout)
		$settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -StartWhenAvailable:$StartWhenAvailable
		$task = New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger -Settings $settings -Description "- $($json.Name) - $($json.Description)"
		Try
		{
			Register-ScheduledTask Parx\$taskName -InputObject $task
			log2 "[SERVICE] Scheduled task $($json.Name), $($json.Description) set" "success"
		}
		Catch
		{
			log2 "[SERVICE] Error $($_.Exception.Message) for scheduled task $($json.Name), $($json.Description)" "error"
			
		}
	}
	Else
	{
		log2 "[SERVICE] Scheduled task $($json.Name), $($json.Description) already set" "info"
	}
	
}

# Validation for the config file from server. Each parameter expected is validated.
Function Validate-Config
{
	Param (
		[string[]]$profiles_admins,
		[ValidateLength(8, 8)]
		[ValidateNotNullOrEmpty()]
		[string]$version = $(throw "Version is mandatory, please provide a value."),
		[ValidateRange(0, 1440)]
		[ValidateNotNullOrEmpty()]
		[string]$service_refresh = $(throw "Service Refresh is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$enable_parx = $(throw "Enable Parx is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$noneShallPass = $(throw "NoneShallPass is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$full_gui = $(throw "Full_gui is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$global:autoload_desktop = $(throw "Autoload_desktop is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$global:autoload_options = $(throw "Autoload_options is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$global:autoload_printers = $(throw "Autoload_printers is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[ValidateNotNullOrEmpty()]
		[string]$global:autoload_shares = $(throw "Autoload_shares is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[string]$global:autoload_taskbar = $(throw "Autoload_taskbar is mandatory, please provide a value."),
		[ValidateSet('0', '1')]
		[string]$global:autoload_associations = $(throw "Autoload_associations is mandatory, please provide a value."),
		[ValidateScript({
				Try { (Invoke-RestMethod -uri "$_/rest" -Headers @{ Accept = "application/json" } -TimeoutSec 1) }
				Catch { Throw [System.Management.Automation.ValidationMetadataException]"Bad Ip server in config file" }
			})]
		[ValidateNotNullOrEmpty()]
		[string]$address = $(throw "Address is mandatory, please provide a value."),
		[string]$actual_address_ip,
		[string]$webproxy
		
	)
	
	Process
	{
		# Check of address changes, for the service to restart if needed
		if ($actual_address_ip -ne $address)
		{
			Return $true, $true
		}
		else
		{
			Return $true, $false
		}
	}
}

# Check for newer config file on the server
function Get-Config ($actual_address_ip)
{
	log2 "[SERVICE] Checking for new config file" "info"
	Try
	{
		$webroot = $address -replace "/api", "/files/config/$env:computername.config"
		$date_server = [datetime](Invoke-WebRequest $webroot -UseBasicParsing).headers['last-modified']
		$date_local = [datetime](Get-item -Path "$PSScriptRoot\config.ps1").LastWriteTime
		# If newer file is found, validate and set is no error are found
		if ($date_local -lt $date_server)
		{
			# Get the content of the config file from the server for validation
			$splat = (((Invoke-WebRequest $webroot -UseBasicParsing).ToString()).Replace("$", "").Replace("`"", "") | ConvertFrom-StringData)
			#Validation success
			Try
			{
				$conf, $restart = Validate-config @splat -actual_address_ip $actual_address_ip
				log2 "[SERVICE] Newer config file is found, applying " "info"
				Set-Config
				Return $conf, $restart
			}
			# Validation failure
			Catch
			{
				log2 "[SERVICE] Newer config file is found but there is a problem : $($_.exception.message)." "error"
				Return $false, $false
			}
		}
		# No changes
		else
		{
			log2 "[SERVICE] Config file is already up to date" "info"
			Return $false, $false
			
		}
	}
	# No config file for the host
	Catch
	{
		log2 "[SERVICE] No config file has been set for $env:computername" "info"
		Return $false, $false
		
	}
}

# Apply config file if validation is ok
function Set-Config
{
	Try
	{
		$webroot = $address -replace "/api", "/files/config/$env:computername.config"
		$destination = "$PSScriptRoot\config.ps1"
		Start-BitsTransfer -Source $webroot -Destination $destination
		
		log2 "[SERVICE] Config file set" "success"
	}
	Catch
	{
		log2 "[SERVICE] Config file not set" "error"
	}
	
	
}


# add host to the db
Function add-host ([string]$env:computername, [string]$hostOS, [string]$version)
{
	
	$url = "$address/rest_host"
	
	$body = @{
		hostName = $env:computername
		hostOs   = $hostOS
		version  = $version
	}
	
	return restPOST -uri $url -body $body -api_token $token
	
}


# update host in the db
Function update-host ([string]$env:computername, [string]$hostOS, [string]$version)
{
	#
	$url = "$address/rest_hostupdate"
	$date = Get-Date
	$formattedDate = $date.ToString("yyyy-MM-dd HH:mm:ss")
	$body = @{
		hostName = $env:computername
		hostOs   = $hostOS
		version  = $version
		last_connection = $formattedDate
	}
	
	return restPOST -uri $url -body $body -api_token $token
	
}



# update client version in db
Function update-version ([string]$env:computername, [string]$version)
{
	
	$url = "$address/rest_version"
	
	$body = @{
		hostName = $env:computername
		version  = $version
	}
	
	return restPOST -uri $url -body $body -api_token $token
	
}

# Register host and update its info in server db
function Register-Host ($hostid, $hostOS)
{
	try
	{
		$json = add-host $env:computername $hostOS $version
		
		if ($json.code -eq 1)
		{
			log2 "[SERVICE] Host already in database" "warn"
		}
		
		elseif ($json.code -eq 0)
		{
			log2 "[SERVICE] Add $env:computername in database" "success"
		}
		else
		{
			log2 "[SERVICE] Server response if $json, not normal" "warn"
		}
		update-version $env:computername $version
		update-host $env:computername $hostOS $version
	}
	catch
	{
		log2 "[SERVICE] Error while updating host in database" "error"
	}
	
}

# Retrieve host information needed for register host
Function Get-HostInfo
{
	$hostId = (get-hostid $env:computername).message
	$wmiOS = Get-WmiObject -ComputerName "." -Class Win32_OperatingSystem;
	if ($wmiOS.ServicePackMajorVersion -eq 0) { $sp = "" }
	else { $sp = " SP" + $wmiOS.ServicePackMajorVersion }
	$hostOS = $wmiOS.Caption + $sp
	Return $hostId, $hostOS
}



# DELETE a scheduled task
function task-delete ($id)
{
	
	$now = Get-Date -UFormat "%Y%m%d@%H%M%S"
	
	if ($id -ne "")
	{
		$args = "/delete /tn Parx\parx-run$id /f"
		$ps = start-process "schtasks.exe" -ArgumentList $args -PassThru
		
		Write-Host "$now [TASK ] Delete task parx-run$id"
	}
	else
	{
		Write-Host "$now [ERROR] No task ID"
	}
}

# shutdown task
function task-shutdown ($when, $task)
{
	if (!([string]::IsNullOrWhiteSpace($when)))
	{
		Set-TaskScheduled -json $task -exe "shutdown" -params "/s /t 10" -StartWhenAvailable $false
	}
	else
	{
		shutdown /s /t 10
	}
}


# Reboot task
function task-reboot ($id, $schparam)
{
	if (!([string]::IsNullOrWhiteSpace($schparam)))
	{
		Set-TaskScheduled -json $task -exe "shutdown" -params "/r /t 10" -StartWhenAvailable $false
		
	}
	else
	{
		shutdown /r /t 10
	}
}


# execute (or schedule if set so) a task 
function task-execute ($json)
{
	
	$id = $json.pivot.id
	$exe = $json.exe
	$params = $json.args
	$schparam = $json.pivot.schedule	
	
	# scheduled task
		if ($schparam -ne "")
		{
			Set-TaskScheduled -json $json -custom -StartWhenAvailable $true
		}
		# non scheduled task
		else
		{
			if ($params)
			{
				start-process $exe -ArgumentList $params
			}
			else
			{
				start-process $exe
			}			
		}	
}

# Log out all users
Function task-logout ($when, $task)
{
	$action = @"
`$users = quser;foreach (`$user in `$users){logoff ((`$user.Trim()) -split '\s+')[2]}
"@
	if (!([string]::IsNullOrWhiteSpace($when)))
	{
		Set-TaskScheduled -json $task -exe $action -StartWhenAvailable $false
	}
	else
	{
		log2 "[SERVICE] Logging out users" "warn"
		$users = quser 2>$null
		if ($users)
		{
			foreach ($user in $users)
			{
				$username = (($user.Trim()) -split '\s+')[0]
				$id = (($user.Trim()) -split '\s+')[2]
				log2 "[SERVICE] Logging out $username id $id" "warn"
				logoff $id
			}
		}
	}
	
}

# If server update needs to change config.ps1 file (for the client to apply new options, etc), merge existing config.ps1 with changes needed
function Merge-Config
{
	#Paths
	$actual_config_path = "$PSScriptRoot\config.ps1"
	$new_config_path = "$PSScriptRoot\add-config.ps1"
	
	#Add new parameters if found (regex #Add -- #Remove)
	$new_config = Get-content $new_config_path -Delimiter "`n" -Encoding oem
	$new_config | Out-String | ForEach-Object { [Regex]::Matches($_, "(?<=#Add)((.|\n)*?)(?=#Remove)") } |
	ForEach-Object {
		$_.Value | Add-Content $actual_config_path -Encoding oem
	}
	
	#Remove parameters from config.ps1 if found (regex #Remove -- #End)
	$actual_config = Get-Content $actual_config_path -Encoding oem
	$new_config | Out-String | ForEach-Object { [Regex]::Matches($_, "(?<=#Remove)((.|\n)*?)(?=#End)") } |
	ForEach-Object {
		$_.Value.Replace("`r`n", "").Trim().Split() |
		Foreach-Object {
			$rem = $_
			$actual_config = $actual_config | Where-Object { !($_.Contains("$rem")) }
		}
	}
	$actual_config | Set-Content $actual_config_path -Encoding oem
}

# execute a client update
Function Task-Update
{
	[CmdletBinding()]
	Param (
		[string]$version		
	)
	log2 "[UPDATE] Client updater START" "task"
	
	# DL the zip file 
	$webroot = $address -replace "/api", ""
	$source = $webroot + "/files/UPD_$version.zip"
	$destination = "UPD_$version.zip"
	
	try
	{
		Invoke-WebRequest $source -OutFile $destination
		log2 "[UPDATE] New parx version is found. Beginning update...file download and extract" "task"
		# Extract the zip file		
		$shell = new-object -com shell.application
		$zip = $shell.NameSpace("C:\parxshell\UPD_$version.zip")
		foreach ($item in $zip.items())
		{
			$name = $item.name
			log2 "[UPDATE] Extract file : $name" "task"
			$shell.Namespace("C:\parxshell").copyhere($item, 1044)
			# If the update needs to change the config file, merge actual config with new parameters required from the server
			if ($name -eq "add-config")
			{
				log2 "[UPDATE] Config file needs some change, updating config.ps1" "task"
				Merge-Config
				Remove-Item "C:\parxshell\add-config.ps1"
				log2 "[UPDATE] Config file updated" "task"
			}
		}
		
		Remove-Item "C:\parxshell\UPD_$version.zip" -force
		log2 "[UPDATE] Client Updater END" "task"
	}
	catch
	{
		if ($_.Exception.Message -match "(404)")
		{
			log2 "[UPDATE] Client Updater : No file found on server" "error"
		}
	}
	
}
# get all tasks for the hostname
Function get-tasksList($hostid)
{
	return restGET "$address/rest_tasks/$hostid"
}
