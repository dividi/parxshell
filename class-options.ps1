################################################# REST

# Set options from json message
function set-options-cache
{
	[CmdletBinding()]
	param
	(
		$options_cache,
		[boolean]$switch_profile,
		[boolean]$first_logon,
		[switch]$wall_only,
		[string]$debugpref,
		[switch]$unloadProfile
	)
	
	if ($debugpref -eq 'Continue')
	{
		$DebugPreference = 'Continue'
		$fromrunspace = $true
	}
	else
	{
		$fromrunspace = $false
	}
	# Initialise Firefox policy
	initFirefoxFiles
	If (!(Test-Path "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"))
	{
		New-Item -Path "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" | Out-Null
	}
	
	# Set restart flag to 0, will be set to 1 if an option has changed and require a refresh explorer to be applied
	$global:restart_flag = 0
	# Set all options that are found
	if (!($wall_only))
	{
		$hash_options = foreach ($option in $options_cache.GetEnumerator())
		{
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION FOUND] Name : $($option.Name) with value : $($option.Value)" "start"
			}
			# Client tags an option Done to 0 when it applies it. Only applies options for which flag is not 0; or if a profile is chosen from GUI list ($switch_profile)
			if ($option.Done -eq "0" -or $switch_profile -eq $true)
			{
				if ($DebugPreference -eq 'Continue')
				{
					log2 "[OPTION NOT SET] SETTING Name : $($option.Name) with value : $($option.Value)" "info"
				}
				# Let's go !
				# Every options functions have internal checks, we set the done flag to 1. disabledesktop will return its own value.
				$done = "1"
				switch ($option.name)
				{
					# disableDesktop is processed within the shortcuts runspace, we only flag the done part wether desktop is synced with onedrive or not
					"disableDesktop" { if (checkOnedrive) { $done = "0" }
						else { $done = "1" } }
					"disableTaskManager" { disabletaskmanager $option.Value }
					"hiddenDrives" { hideDrives $option.Value; $restart_flag = 1 }
					"hideRunMenu" { disableRunMenu $option.Value; $restart_flag = 1 }
					"hideControlPanel" { hideControlPanel $option.Value; $restart_flag = 1 }
					"disableRegistry" { disableRegistry $option.Value }
					"disallowRun" { disableApps $option.Value; $restart_flag = 1 }
					"changeHomePageIE" { changeIeHomepage $option.Value }
					"changeHomePageFF" { changeFirefoxHomepage $option.Value }
					"monitorSleep" { setMonitorSleep $option.Value }
					"systemSleep" { setSystemSleep $option.Value }
                    "lockTimer" { setLockTimer $option.Value }
					"bindHomeFolder" { bindHomeFolder $option.Value $first_logon; $restart_flag = 1 }
					"shutdownTime" { scheduledShutdown $option.Value }
					"proxyUrlIE" { setSystemProxyUrl $option.Value; $restart_flag = 1 }
					"proxyBypassIE" { setSystemProxyBypass $option.Value; $restart_flag = 1 }
					"proxyScriptIE" { setSystemProxyScript $option.Value; $restart_flag = 1 }
					"proxyTypeFF" { setFirefoxProxyType $option.Value }
					"proxyUrlFF" { setFirefoxProxyUrl $option.Value }
					"proxyScriptFF" { setFirefoxProxyScript $option.Value }
					"proxyBypassFF" { setFirefoxProxyBypass $option.Value }
					"AutoEndTasks" { AutoEndTasks $option.Value; $restart_flag = 1 }
					"disableCMD" { disableCMD $option.Value; $restart_flag = 1 }
				}
			}
			else
			{
				if ($DebugPreference -eq 'Continue')
				{
					log2 "[OPTION ALREADY SET, SKIPPING] [INFO] Name : $($option.Name) with value : $($option.Value)" "warn"
				}
			}
			[pscustomobject]::New(@{ Name = $option.name; Value = $option.Value; Done = "$done" })
		}
		# Re set the cache with done set to 1 only if we're not unloading a profile
		if (!$unloadProfile)
		{			
			Set-Cache $hash_options $profile_cache
		}
		
	}
	# Else, only check for new wallpaper and set it 
	$wall_set = setWallPaper
	if ($wall_set -eq $false)
	{
		# Adds custom c# class to refresh wallpaper, if type doesn't exist
		if (!("Wallpaper.Setter" -as [type]))
		{
			Add-Type -TypeDefinition $wp
		}
		[void][Wallpaper.Setter]::SetWallpaper($null);
	}
	
	# Adds custom c# class to refresh policy and explorer, if type doesn't exist
	if (!("Parxgui.Refresh_policy" -as [type]) -or !("Parxgui.Refresh_Explorer" -as [type]))
	{
		Add-Type -Namespace Parxgui -Name Refresh_policy -MemberDefinition @"
  [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
  public static extern IntPtr SendMessageTimeout(IntPtr hWnd, uint Msg, uint wParam, string lParam, uint fuFlags, uint uTimeout, out UIntPtr lpdwResult);
"@
		Add-Type -Namespace Parxgui -Name Refresh_Explorer -MemberDefinition $refresh
	}
	# Send system the policy refresh message
	Send-SettingChange
	# Only refresh explorer (if needed)
	# If restart_flag is set to 1 from the previous pass, refresh explorer
	if ($restart_flag -eq 1)
	{
		# We wait for logonui process to be done
		log2 "[OPTIONS] New param for which refreshing explorer is required found, refreshing explorer" "info"
		Refresh-Explorer
		log2 "[OPTIONS] Explorer refreshed" "info"
	}
	else
	{
		log2 "[OPTIONS] No need to refresh explorer" "info"
	}
	if ($DebugPreference -eq 'Continue')
	{
		log2 "[OPTIONS] [TASK] All options were processed" "task"
	}
}

################################################# REG LIBS

# create the path and set a registry property
Function setRegProperty ($regKey, [string]$key, $type, $value, $name)
{
	try
	{
		if (-not (Test-Path $regKey)) { $null = New-Item -path $regKey -ItemType $type -Force }
		$null = Set-ItemProperty -path $regKey -name $key -type $type -value $value -force
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[SUCCESS] Option $name : setting key $key with value $value on reg $regkey" "registry"
		}
	}
	catch
	{
		$msg = $_.Exception.Message
		log2 "(Fn setRegProperty)$msg (sur $regKey)" "error"
		if ($DebugPreference -eq 'Continue')
		{
			log2  "[ERROR] Option $name error : $($_.Exception.Message)" "registry"
		}
	}
}

################################################# MISC

# Kill IE
Function killIE ()
{
	$ProcessActive = Get-Process iexplore -ErrorAction SilentlyContinue
	
	if ($ProcessActive -ne $null)
	{
		Stop-Process -Name iexplore -Force
		log2 "[OPTIONS] Kill IE" "warn"
	}
}

# Kill Firefox
Function killFirefox ()
{
	$ProcessActive = Get-Process firefox -ErrorAction SilentlyContinue
	
	if ($ProcessActive -ne $null)
	{
		Stop-Process -Name firefox -Force
		log2 "[OPTIONS] Kill Firefox" "warn"
	}
}

# arg : create a shutdown scheduled task to (ah ah) shutdown the OS
Function scheduledShutdown ([string]$value)
{
	# make a nice task
	if ($value -ne "")
	{
		$Sta = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument '-command stop-computer -force'
		$STPrin = New-ScheduledTaskPrincipal -UserId "SYSTEM" -LogonType ServiceAccount
		$Time = New-ScheduledTaskTrigger -Daily -At $value
		$task = Register-ScheduledTask "PARX Daily Shutdown" -Action $Sta -Principal $STPrin -Trigger $Time -Force
		log2 "[OPTION SET] PARX Daily Shutdown set at $value" "success"
	}
	# kill a bad bad task
	else
	{
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] PARX Daily not set, checking if scheduled task is to be deleted" "info"
		}
		Try
		{
			Unregister-ScheduledTask -TaskName "PARX Daily Shutdown" -Confirm:$false
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] PARX Daily Shutdown deleted" "success"
			}
		}
		Catch
		{
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] [INFO] PARX Daily Shutdown scheduled task not found, skipping deletion." "info"
			}
		}
	}
}

# define duration to session lock
Function setLockTimer ([string]$value)
{
   	$regKey = "Registry::\HKEY_USERS\$userMSID\SOFTWARE\Policies\Microsoft\Windows\Control Panel\Desktop"
    if ([string]::IsNullOrEmpty($value) -or $value -eq 0)
    {
        $null = Remove-ItemProperty -Path $regKey -Name "ScreenSaveActive" -ErrorAction SilentlyContinue
		$null = Remove-ItemProperty -Path $regKey -Name "ScreenSaveTimeOut" -ErrorAction SilentlyContinue
		$null = Remove-ItemProperty -Path $regKey -Name "ScreenSaverIsSecure" -ErrorAction SilentlyContinue
        log2 "[OPTION SET] Session lock timer disabled" "success"
    }
    else
    {
        [int]$minutes = [int]$value
        [int]$seconds = $minutes * 60
        setRegProperty $regKey "ScreenSaveActive" String 1
        setRegProperty $regKey "ScreenSaveTimeOut" String $seconds
        setRegProperty $regKey "ScreenSaverIsSecure" String 1
        log2 "[OPTION SET] Session lock timer enabled: $minutes minutes ($seconds seconds)" "success"
    }

}

# define duration to monitor sleep mode 
Function setMonitorSleep ([string]$value)
{
	$null = powercfg -change -monitor-timeout-ac $value
	$null = powercfg -change -monitor-timeout-dc $value
	log2 "[OPTION SET] Monitor sleep : $value mn" "success"
}

# define duration to system sleep mode 
Function setSystemSleep ([string]$value)
{
	$null = powercfg -change -standby-timeout-ac $value
	$null = powercfg -change -standby-timeout-dc $value
	log2 "[OPTION SET] System sleep : $value mn" "success"
}

# change system proxy 
Function setSystemProxyUrl ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
	if ($value -ne "")
	{
		setRegProperty $regKey "ProxyServer" String $value
		setRegProperty $regKey "ProxyEnable" DWORD 1
		log2 "[OPTION SET] System Proxy enabled : $value" "success"
	}
	else
	{
		setRegProperty $regKey "ProxyServer" String ""
		setRegProperty $regKey "ProxyEnable" DWORD 0
		log2 "[OPTION SET] System Proxy disabled : $value" "success"
	}
}

# change system proxy 
Function setSystemProxyScript ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
	setRegProperty $regKey "AutoConfigURL" String $value
	log2 "[OPTION SET] System Proxy script ($value)" "success"
}

# ie proxy bypass addresses (use <LOCAL> to bypass local addresses)
function setSystemProxyBypass ($value)
{
	# replace , by ;	
	$value = $value -replace ",", ";"
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
	setRegProperty $regKey "ProxyOverride" String $value
	log2 "[OPTION SET] System Proxy Bypass : $value" "success"
}

# Check if desktop is synced with onedrive
function checkOnedrive
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
	$desktop = (Get-ItemProperty $regKey).Desktop
	if (!($desktop -like "*onedrive*"))
	{
		return $false
	}
	else
	{
		log2 "[OPTION] Enable desktop : $username desktop is synced with onedrive, disabledesktop will be applied only if $username stop syncing it." "warn"
		return $true
	}
}

# Prevent user from creating, deleting, etc files on his desktop
function disabledesktop ([string]$Value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
	$desktop = (Get-ItemProperty $regKey).Desktop
	if (!(checkOnedrive))
	{
		if ($Value -eq 1)
		{
			$acl_rights = @{
				CreateFiles				     = "Deny"
				AppendData				     = "Deny"
				DeleteSubDirectoriesAndfiles = "Deny"
			}
			Set_Acl -Folder "c:\Users\$username\desktop" $acl_rights -inherit -User
			log2 "[OPTION SET] Disable desktop : $value" "success"
			
		}
		else
		{
			$acl_rights = @{
				CreateFiles				     = "Allow"
				AppendData				     = "Allow"
				DeleteSubDirectoriesAndfiles = "Allow"
			}
			Set_Acl -Folder "c:\Users\$username\desktop" $acl_rights -inherit -User
			log2 "[OPTION SET] Enable desktop : $value" "success"
		}
		
		
		
	}
	
	
}

# Disable task manager
function DisableTaskManager ([string]$value)
{
	$regkey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\System"
	if ($value -eq 1)
	{
		if (-not (Test-Path $regKey)) { $null = New-Item -path $regKey -ItemType RegistryKey -Force }
		setRegProperty $regKey "DisableTaskMgr" DWORD $value "DisableTaskManager"
		log2 "[OPTION SET] Disable Task Manager : $value" "success"
	}
	else
	{
		$null = Remove-ItemProperty -Path $regKey -Name "DisableTaskMgr" -ErrorAction SilentlyContinue
		log2 "[OPTION SET] Enable Task Manager : $value" "success"
	}
}

# Force process ending when user logs off (to prevent eager users to leave their session open with some processes asking to be ended)
function AutoEndTasks ([string]$value)
{
	$regkey = "Registry::\HKEY_USERS\$userMSID\Control Panel\Desktop"
	if ($value -eq 1)
	{
		setRegProperty $regKey "AutoEndTasks" DWORD $value "AutoEndTasks"
		log2 "[OPTION SET] AutoEndTasks enabled : $value" "success"
		
	}
	else
	{
		$null = Remove-ItemProperty -Path $regKey -Name "AutoEndTasks" -ErrorAction SilentlyContinue
		log2 "[OPTION SET] AutoEndTasks disabled : $value" "success"
	}
}

# Hide control panel (1=hidden, 0=shown)
Function hideControlPanel ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
	$controlsKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl"
	if ($value -eq 1)
	{
		# For settings page, show only essentials
		if (-not (Test-Path $regKey)) { $null = New-Item -path $regKey -ItemType RegistryKey -Force }
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Hiding parameters section" "info"
		}
		$values = "showonly:about;display;windowsupdate;bluetooth;tabletmode;sound;easeofaccess-audio;closedcaptioning;easeofaccess-colorfilter;easeofaccess-display;easeofaccess-eyecontrol;Fonts;easeofaccess-highcontrast;easeofaccess-keyboard;easeofaccess-magnifier;easeofaccess-mouse;easeofaccess-mousepointer;easeofaccess-narrator;easeofaccess-narrator-isautostartenabled;easeofaccess-speechrecognition;easeofaccess-cursor;easeofaccess-visualeffects;printers;Touch;Touchpad;connecteddevices;workplace"
		setRegProperty $regKey "SettingsPageVisibility" String $values "HideControlPanel"
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Hiding control panel section" "info"
		}
		# For control panel, show only essentials
		if (-not (Test-Path $controlsKey)) { $null = New-Item -path $controlsKey -ItemType RegistryKey -Force }
		setRegProperty $regKey "DisallowCpl" Dword $value "DisallowControlPanel"
		$x = 0
		$controls = @("Barre des t�ches et navigation"; "Centre de synchronisation"; "Centre R�seau et Partage"; "Chiffrement de lecteur Bitlocker"; "Clavier"; "Microsoft.UserAccounts"; "Connexions RemoteApp et Bureau � distance"; "Date et heure"; "Espaces de stockage"; "Ex�cution automatique"; "Gestion des couleurs"; "Gestionnaire de p�riph�riques"; "Historique des fichiers"; "Microsoft.IndexingOptions"; "Options de l'Explorateur de Fichiers"; "Options Internet"; "Microsoft.AdministrativeTools"; "Pare-Feu Windows Defender"; "Programmes et fonctionnalit�s"; "R�cup�ration"; "R�solution des probl�mes"; "Sauvegarder et restaurer (Windows 7)"; "S�curit� et maintenance"; "Syst�me"; "T�l�phone et modem")
		foreach ($control in $controls)
		{
			$x++
			setRegProperty $controlsKey "$x" String $control $control
		}
		log2 "[OPTION SET] Hide control Panel enabled ($value)" "success"
	}
	else
	{
		$null = Remove-ItemProperty -Path $regKey -Name "SettingsPageVisibility" -ErrorAction SilentlyContinue
		setRegProperty $regKey "DisallowCpl" Dword $value "DisallowControlPanel"
		log2 "[OPTION SET] Show control Panel ($value)" "success"
	}
}

# Disable the run menu (1=disabled, 0=enabled)
Function disableRunMenu ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
	$classesKey = "Registry::\HKEY_USERS\$userMSID\SOFTWARE\Classes\CLSID\{2559a1f3-21d7-11d4-bdaf-00c04f60b9f0}\Instance\InitPropertyBag"
	
	# Hide the run menu
	if ($value -eq 1)
	{
		#Acl right
		$acl_rights = @{
			ReadAndExecute = "Deny"
		}
		
		#Disable win+
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Disabling Win+R key (since it's the only thing that need the explorer to restart, and we don't want that, it will be applied only for next logon" "info"
		}
		setRegProperty $regKey "DisabledHotkeys" ExpandString "R" "DisableRunMenu"
		
		#Disable %windir%\explorer.exe shell:::{2559a1f3-21d7-11d4-bdaf-00c04f60b9f0} command to be executed (because I'm a bourrin !)
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Disabling %windir%\explorer.exe shell:::{2559a1f3-21d7-11d4-bdaf-00c04f60b9f0} command execution " "info"
		}
		
		"CLSID", "command", "method" | ForEach-Object { setRegProperty $classesKey $_ String "" "DisableRunMenu" }
		#Hide task manager and run when right clicking start menu, and prevent execute from folder (one never knows !)
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Hiding task manager, and hiding run menu from start menu and prevent its execution from WinX Folder" "info"
		}
		$path = "C:\Users\$username\AppData\Local\Microsoft\Windows\WinX\Group2"
		Get-ChildItem $path -Force | ForEach-Object {
			If ($_.Name -like "*Run.lnk*" -or $_.Name -like "*Task Manager.lnk*")
			{
				Set_Acl -Folder "$path\$($_.Name)" $acl_rights -Type "File"
				$_.Attributes = $_.Attributes -bor "Hidden"
			}
		}
		
		#Hide powershell when right clicking start menu, and prevent execute from folder (one never knows !)
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Hide powershell from start menu and prevent its execution from WinX Folder" "info"
		}
		$path = "C:\Users\$username\AppData\Local\Microsoft\Windows\WinX\Group3"
		Get-ChildItem $path -Force | ForEach-Object {
			If ($_.Name -like "*Windows Powershell.lnk*")
			{
				Set_Acl -Folder "$path\$($_.Name)" $acl_rights -Type "File"
				$_.Attributes = $_.Attributes -bor "Hidden"
			}
		}
		log2 "[OPTION SET] Hide Run Menu : $value" "success"
	}
	
	# show the menu
	else
	{
		#Acl right
		$acl_rights = @{
			ReadAndExecute = "Allow"
		}
		#Enable win+R	
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Enabling Win+R (since it's the only thing that need the explorer to restart, and we don't want that, it will be applied only for next logon" "info"
		}
		$null = Remove-ItemProperty -Path $regKey -Name "DisabledHotkeys" -ErrorAction SilentlyContinue
		#Show task manager and run when right clicking start menu, and delete acl
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Showing task manager, and showing run menu from start menu" "info"
		}
		$path = "C:\Users\$username\AppData\Local\Microsoft\Windows\WinX\Group2"
		If (Test-Path $classesKey)
		{
			Remove-Item -Path $classesKey -Recurse -Force
		}
		Get-ChildItem $path -Force | ForEach-Object {
			If ($_.Name -like "*Run.lnk*" -or $_.Name -like "*Task Manager.lnk*")
			{
				Set_Acl -Folder "$path\$($_.Name)" $acl_rights -Type "File"
				$_.Attributes = $_.Attributes -bxor "Hidden"
			}
		}
		#Show powershell when right clicking start menu, and delete acl
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Showing powerhsell from start menu" "info"
		}
		$path = "C:\Users\$username\AppData\Local\Microsoft\Windows\WinX\Group3"
		Get-ChildItem $path -Force | ForEach-Object {
			If ($_.Name -like "*Windows Powershell.lnk")
			{
				Set_Acl -Folder "$path\$($_.Name)" $acl_rights -Type "File"
				$_.Attributes = $_.Attributes -bxor "Hidden"
			}
		}
		#Remove disabling  %windir%\explorer.exe shell:::{2559a1f3-21d7-11d4-bdaf-00c04f60b9f0} command to be executed
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[OPTION] Enabling  %windir%\explorer.exe shell:::{2559a1f3-21d7-11d4-bdaf-00c04f60b9f0} execution" "info"
		}
		
		log2 "[OPTION SET] Show Run Menu : $value" "success"
	}
}

# disable registry access (1=disabled, 0=enabled)
Function disableRegistry ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\System"
	setRegProperty $regKey "DisableRegistryTools" DWORD $value "DisableRegistry"
	if ($value -eq 0)
	{
		log2 "[OPTION SET] Enable Registry : $value" "success"
	}
	else
	{
		log2 "[OPTION SET] Disable Registry : $value" "success"
	}
}

# Change IE homepage
Function changeIeHomepage ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Internet Explorer\Main"
	setRegProperty $regKey "Start Page" String $value "Change IE HomePage"
	log2 "[OPTION SET] IE HomePage : $value" "success"
}

# Decimal value for hidden drives (comma separated, empty to show all drives)
Function giveHiddenDrivesValue([string]$value)
{
	# A: 1, B: 2, C: 4, D: 8, E: 16, F: 32, G: 64, H: 128, I: 256, J: 512, K: 1024, 
	# L: 2048, M: 4096, N: 8192, O: 16384, P: 32768, Q: 65536, R: 131072, S: 262144, 
	# T: 524288, U: 1048576, V: 2097152, W: 4194304, X: 8388608, Y: 16777216, Z: 33554432
	
	[int]$sum = 0;
	$value.Split(',') | ForEach-Object {
		switch ($_)
		{
			"A" { $sum += 1 }
			"B" { $sum += 2 }
			"C" { $sum += 4 }
			"D" { $sum += 8 }
			"E" { $sum += 16 }
			"F" { $sum += 32 }
			"G" { $sum += 64 }
			"H" { $sum += 128 }
			"I" { $sum += 256 }
			"J" { $sum += 512 }
			"K" { $sum += 1024 }
			"L" { $sum += 2048 }
			"M" { $sum += 4096 }
			"N" { $sum += 8192 }
			"O" { $sum += 16384 }
			"P" { $sum += 32768 }
			"Q" { $sum += 65536 }
			"R" { $sum += 131072 }
			"S" { $sum += 262144 }
			"T" { $sum += 524288 }
			"U" { $sum += 1048576 }
			"V" { $sum += 2097152 }
			"W" { $sum += 4194304 }
			"X" { $sum += 8388608 }
			"Y" { $sum += 16777216 }
			"Z" { $sum += 33554432 }
			default { $sum += 0 }
		}
	}
	return $sum
}

# Hide drives in Computer
Function hideDrives ([string]$value)
{
	[int]$decValue = giveHiddenDrivesValue $value
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
	
	# Hide no drives
	if ($value -eq "")
	{
		# Delete existing key
		$exists = (Get-ItemProperty $regkey -ErrorAction SilentlyContinue).NoDrives -eq $null
		if ($exists -eq $False)
		{
			$null = Remove-ItemProperty -Path $regKey -Name "NoDrives" -ErrorAction SilentlyContinue
		}
		# Show explorer address bar
		#$null = Stop-Process -Name hidebar -Force -ErrorAction SilentlyContinue
		log2 "[OPTION SET] Hide drives disabled ($value)" "success"
	}
	else
	{
		# Create the keys if missing 
		setRegProperty $regKey "NoDrives" DWORD $decValue "Hide Drives"
		# Hide explorer address bar to prevent navigation from it
		$block = @"
           Start-Process -FilePath "$scriptpath\Bin\hidebar.exe"
"@
		$scriptblock = [scriptblock]::Create($block)
		#$null = invoke-ascurrent_user -UseWindowsPowerShell -scriptblock $scriptblock -nowait
		log2 "[OPTION SET] Hide drives enabled ($value)" "success"
	}
}

# Disable cmd prompt (but leaves .cmd and .bat allowed)
function disableCMD ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Microsoft\Windows\System"
	
	if ($value -eq 0)
	{
		$null = Remove-ItemProperty -Path $regKey -Name "DisableCMD" -ErrorAction SilentlyContinue
		log2 "[OPTION SET] Enable Cmd : $value" "success"
	}
	else
	{
		setRegProperty $regKey "DisableCMD" DWORD 2 "Disable CMD"
		log2 "[OPTION SET] Disable Cmd : $value" "success"
	}
}

# Disable selected apps execution
Function disableApps ([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
	setRegProperty $regKey "DisallowRun" DWORD 1 "Disallow Run"
	# make DisallowRun key if it not exists or clear it
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun"
	if (-not (Test-Path $regKey))
	{
		$null = New-Item -path $regKey -ItemType RegistryKey -Force
	}
	else
	{
		$null = Remove-Item -Path $regKey
		$null = New-Item -path $regKey -ItemType RegistryKey -Force
	}
	
	# apps to disable
	$keyName = 1
	$value.Split(',') | ForEach-Object {
		if ($_ -ne "")
		{
			$null = Set-ItemProperty -path $regKey $keyName -value $_.trim()
			$keyName++
		}
	}
	if ($value -eq "")
	{
		log2 "[OPTION SET] Disabled apps disabled" "success"
		
	}
	else
	{
		log2 "[OPTION SET] Disable app enabled ($value)" "success"
	}
}

# set the wallpaper
function setWallPaper ()
{
	$wall_address = $address.Replace("api", "wallpapers")
	$wallregKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\System"
	Try
	{
		# Save the wallpaper in windows folder and set it to current user only if length is not the same (or file doesn't exist)
		If ((Invoke-WebRequest -UseBasicParsing -Uri "$wall_address/$profileid.jpg").RawContentLength -ne (Get-ChildItem -Path "C:\Windows\Web\Wallpaper\Windows\$profileid.jpg" -ErrorAction SilentlyContinue).Length)
		{
			Invoke-WebRequest -Uri "$wall_address/$profileid.jpg" -OutFile "C:\Windows\Web\Wallpaper\Windows\$profileid.jpg"
			log2 "[OPTIONS] New Wallpaper found" "info"
			#Return $false
		}
		else
		{
			log2 "[OPTIONS] No new Wallpaper found" "info"
		}
		# Apply registry change to set wallpaper
		If ((Test-RegistryValue $wallregKey "Wallpaper") -eq $false -or (Test-RegistryValue $wallregKey "Wallpaper" -getvalue $true) -ne "C:\Windows\web\wallpaper\Windows\$profileid.jpg")
		{
			$null = reg add "HKU\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v Wallpaper /t REG_SZ /d C:\Windows\web\wallpaper\Windows\$profileid.jpg /f
			$null = reg add "HKU\$userMSID\Control Panel\Desktop" /v Wallpaper /t REG_SZ /d C:\Windows\web\wallpaper\Windows\$profileid.jpg /f
			log2 "[OPTION SET] Wallpaper set" "success"
			Return $false
		}
	}
	Catch
	{
		# If no wallpaper is set, set the windows default one		
		$null = reg add "HKU\$userMSID\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v Wallpaper /t REG_SZ /d C:\Windows\web\wallpaper\Windows\img0.jpg /f
		$null = reg add "HKU\$userMSID\Control Panel\Desktop" /v Wallpaper /t REG_SZ /d C:\Windows\web\wallpaper\Windows\img0.jpg /f
		log2 "[OPTIONS] No Wallpaper set on profile, resetting to windows default one" "info"
		Return $false
	}
}

# Get and test if homedrive exists. 3 outcomes : User has a homedrive and is reachable (user from site) / User has a homedrive and is not reachable (user from other site) / User has no homedrive (ex ATI,...)
function Test-Homedrive
{
	param (
		[boolean]$first_logon
	)
	
	# Reg key created when user logs on for the first time, with its profile properties
	$environment = "Registry::\HKEY_USERS\$userMSID\Volatile Environment"
	Do
	{
		# We have to wait for the homedrive reg key to exists
		
		if ((Test-RegistryValue -RegKeyPath $environment -Value "HOMEDRIVE") -eq $false)
		{
			$wait = $true
			Start-Sleep -Milliseconds 100
		}
		else
		{
			$wait = $false
		}
	}
	while ($wait -eq $true)
	# Check if homedrive is bound to an other drive than C: (from AD), else return false
	$homeletter = Test-RegistryValue -RegKeyPath $environment -Value "HOMEDRIVE" -getvalue $true
	if ($homeletter -ne "C:")
	{
		# If first logon (or first launch since parx big update) we have to wait for some reg keys to exists, and test the share path from the current user session
		If ($first_logon -eq $true)
		{
			# File that will get the test-path result from current user
			$check_file = "c:\Users\$username\appdata\roaming\parx\homeshare.txt"
			# We have to wait for the homeshare reg key to exists
			Do
			{
				if ((Test-RegistryValue -RegKeyPath $environment -Value "HOMESHARE") -eq $false)
				{
					$wait = $true
					Start-Sleep -Milliseconds 100
				}
				else
				{
					$wait = $false
				}
			}
			while ($wait -eq $true)
			
			#Homeshare value (ie \\server\share)
			$homeshare = Test-RegistryValue -RegKeyPath $environment -Value "HOMESHARE" -getvalue $true

			# Block that will be injected in the invoke-ascurrent_user function			
			$scriptblock = [scriptblock]::Create("Test-Path $homeshare")
			
			log2 "[OPTION] First logon for $username, waiting for session to be mounted to check homedrive" "Task"
			# We have to wait for the user session to be mounted for the invoke-ascurrent_user function to find the correct user session ID
			Do
			{
				$null = quser $username 2>&1 3>&1
				if ($LASTEXITCODE -eq 1)
				{
					$wait = $true
					Start-Sleep -Milliseconds 100
				}
				else
				{
					$wait = $false
				}
			}
			while ($wait -eq $true)
			log2 "[OPTION] Session for $username found, checking if homedrive is reachable " "Task"
			# Test if homedrive is mounted for current user
			$test_homedir = invoke-ascurrent_user -UseWindowsPowerShell -scriptblock $scriptblock -CaptureOutput
			
			# Get the test-path result from output, converting string result to boolean
			if ([System.Convert]::ToBoolean($test_homedir))
			{
				Return $homeletter, $true, $homeshare
			}
			else
			{
				Return $homeletter, $false, $homeshare
			}
		}
		# Else, if not first logon, testing for previous mounted points in registry
		else
		{
			$homedir_mounted = "Registry::\HKEY_USERS\$userMSID\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2"
            $homeshare = Test-RegistryValue -RegKeyPath $environment -Value "HOMESHARE" -getvalue $true
            $homedir = $homeshare.Replace("\","#")

			if (Test-Path "$homedir_mounted\$homedir")
			{
				Return $homeletter, $true, $homeshare
			}
			else
			{
				Return $homeletter, $false, $homeshare
			}
		}
	}
	# Homedrive value is set to C: (user has no homedrive bound from AD)
	else
	{
		Return $homeletter, $false, $homeshare
	}
}

# bind a drive letter with pics, vids, musics and docs folders, hide useless stuff from Ce PC and bind the drive letter to the user library
Function bindHomeFolder ([string]$letter, $first_logon)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
	$hide_icon = "Registry::\HKEY_USERS\$userMSID\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideMyComputerIcons"
	$quick_access = "Registry::\HKEY_USERS\$userMSID\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
	
	# Set "Ce PC" as default view when opening explorer.exe
	if ($DebugPreference -eq 'Continue')
	{
		log2 "[OPTION] Setting -CE PC- as default view when opening explorer" "info"
	}
	$null = New-ItemProperty -path $quick_access -name "LaunchTo" -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue
	$homeletter, $homedrive, $homeshare = Test-Homedrive $first_logon
	
	# Test whether user Homedrive is present on fileshare
	if ($homedrive -eq $true)
	{
		log2 "[OPTION] User $username homedrive was found, binding drive : $letter" "task"
		if (-not (Test-Path $regKey)) { $null = New-Item -path $regKey -ItemType RegistryKey -Force }
		if ($letter -ne "")
		{
			# User environment variables
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Forwarding user environment variables to ${letter}: " "info"
			}
			
			setRegProperty $regKey "Cookies" String "%USERPROFILE%\AppData\Local\Microsoft\Windows\INetCookies"
			setRegProperty $regKey "Favorites" String "%USERPROFILE%\Favorites"
			setRegProperty $regKey "History" String "%USERPROFILE%\AppData\Local\Microsoft\Windows\History"
			setRegProperty $regKey "My Music" String "%USERPROFILE%\Music"
			setRegProperty $regKey "My Pictures" String "%USERPROFILE%\Pictures"
			setRegProperty $regKey "My Video" String "%USERPROFILE%\Videos"
			setRegProperty $regKey "Personal" String "${letter}:\"
			
			# User libraries
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Forwarding user libaries to ${letter}: " "info"
			}
			setRegProperty $regKey "{374DE290-123F-4565-9164-39C4925E467B}" String "%USERPROFILE%\Downloads"
			setRegProperty $regKey "{0DDD015D-B06C-45D5-8C4C-F59713854639}" String "%USERPROFILE%\Pictures"
			setRegProperty $regKey "{F42EE2D3-909F-4907-8871-4C22FC0BF756}" String "${letter}:\"
			setRegProperty $regKey "{A0C69A99-21C8-4671-8703-7934162FCF1D}" String "%USERPROFILE%\Music"
			setRegProperty $regKey "{7D83EE9B-2244-4E70-B1F5-5393042AF1E4}" String "%USERPROFILE%\Downloads"
			setRegProperty $regKey "{35286A68-3C57-41A1-BBB1-0EAE73D76C95}" String "%USERPROFILE%\Video"
			
			# Hiding stuff from explorer
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Hiding some useless stuff from explorer " "info"
			}
			if ((Test-Path -path $hide_icon) -ne $true) { $null = New-Item $hide_icon -force -ErrorAction SilentlyContinue };
			$null = New-ItemProperty -path $hide_icon -Name '{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
			$null = New-ItemProperty -path $hide_icon -Name '{3DFDF296-DBEC-4FB4-81D1-6A3438BCF4DE}' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
			$null = New-ItemProperty -path $hide_icon -Name '{F86FA3AB-70D2-4FC7-9C99-FCBF05467F3A}' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
			$null = New-ItemProperty -path $hide_icon -Name '{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
			$null = New-ItemProperty -LiteralPath "$hide_icon\NewStartPanel" -Name '{645FF040-5081-101B-9F08-00AA002F954E}' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
			
			# Cleaning history and setting quick access value
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Cleaning recent destination history and setting quick access to ${letter}:" "info"
			}
			$null = robocopy /Z /S /NP /NDL /NFL /NJH /NJS "$PSScriptRoot\links" "C:\Users\$username\Links"
			$null = Get-ChildItem -Path "C:\Users\$username\AppData\Roaming\Microsoft\Windows\Recent\AutomaticDestinations" -Include * -File -Recurse -ErrorAction SilentlyContinue | ForEach-Object { $null = $_.Delete() }
		}
		else
		{
			# User environment variables
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Resetting user environment variables to default value : %USERPROFILE% " "info"
			}
			setRegProperty $regKey "Cookies" String "%USERPROFILE%\AppData\Local\Microsoft\Windows\INetCookies"
			setRegProperty $regKey "Favorites" String "%USERPROFILE%\Favorites"
			setRegProperty $regKey "History" String "%USERPROFILE%\AppData\Local\Microsoft\Windows\History"
			setRegProperty $regKey "My Music" String "%USERPROFILE%\Music"
			setRegProperty $regKey "My Pictures" String "%USERPROFILE%\Pictures"
			setRegProperty $regKey "My Video" String "%USERPROFILE%\Videos"
			setRegProperty $regKey "Personal" String "%USERPROFILE%\Documents"
			
			# User libraries
			setRegProperty $regKey "{374DE290-123F-4565-9164-39C4925E467B}" String "%USERPROFILE%\Downloads"
			setRegProperty $regKey "{0DDD015D-B06C-45D5-8C4C-F59713854639}" String "%USERPROFILE%\Pictures"
			setRegProperty $regKey "{F42EE2D3-909F-4907-8871-4C22FC0BF756}" String "%USERPROFILE%\Documents"
			setRegProperty $regKey "{A0C69A99-21C8-4671-8703-7934162FCF1D}" String "%USERPROFILE%\Music"
			setRegProperty $regKey "{7D83EE9B-2244-4E70-B1F5-5393042AF1E4}" String "%USERPROFILE%\Downloads"
			setRegProperty $regKey "{35286A68-3C57-41A1-BBB1-0EAE73D76C95}" String "%USERPROFILE%\Video"
			
			# Showing stuff from explorer
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Showing useless stuff from explorer" "info"
			}
			$null = Remove-Item -Path $hide_icon -Force
			
			# Cleaning history and setting quick access value
			if ($DebugPreference -eq 'Continue')
			{
				log2 "[OPTION] Cleaning recent destination history and setting back quick access to default value : %USERPROFILE%" "info"
			}
			$null = Get-ChildItem -Path "C:\Users\$username\Links" -Include * -File -Recurse | ForEach-Object { $null = $_.Delete() }
			$null = Get-ChildItem -Path "C:\Users\$username\AppData\Roaming\Microsoft\Windows\Recent\AutomaticDestinations" -Include * -File -Recurse | ForEach-Object { $null = $_.Delete() }
		}
		log2 "[OPTION SET] Drive binded : $letter" "Success"
	}
	else
	{
		if ($homeletter -eq "C:")
		{
			log2 "[OPTION NOT SET] User $username has no shared homedrive - Homedrive is $homeletter" "Warn"
		}
		else
		{
			log2 "[OPTION NOT SET] User $username homedrive ($homeletter) is not reachable (User Homedrive not in current AD site) " "Warn"
		}
	}
}

# Disable the windows 7/8 libraries (1=disabled, 0=enabled)
Function removeLibraries([string]$value)
{
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Classes\CLSID\{031E4825-7B94-4dc3-B131-E946B44C8DD5}"
	setRegProperty $regKey "System.IsPinnedToNameSpaceTree" DWORD $value
	log2 "[OPTION SET] Disable Libraries : $value" "success"
}

################################################# FIREFOX

# make firefox config files (local-setting.js + mozilla.cfg)
Function initFirefoxFiles()
{
	# delete mozilla.cfg and local-settings.js
	$installFolder = (Get-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe" -name "path").Path
	$prefFile = $installFolder + "\defaults\pref\local-settings.js"
	$cfgFile = $installFolder + "\mozilla.cfg"
	
	if (Test-Path $prefFile) { $null = Remove-Item -Path $prefFile }
	if (Test-Path $cfgFile) { $null = Remove-Item -Path $cfgFile }
	
	# test if Mozilla/Firefox keys exists and create them
	$MozillaregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla"
	$MozillaFirefoxregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox"
	
	# Mozilla key do not exist : create mozilla AND firefox keys
	if (-not (Test-Path $MozillaregKey))
	{
		$null = New-Item -path $MozillaregKey -ItemType RegistryKey -Force
		$null = New-Item -path $MozillaFirefoxregKey -ItemType RegistryKey -Force
	}
	# Mozilla key exists : create firefox keys if it do not exist
	else
	{
		if (-not (Test-Path $MozillaFirefoxregKey))
		{
			$null = New-Item -path $MozillaFirefoxregKey -ItemType RegistryKey -Force
		}
	}
	# Delete existing key PROXY (quicker to clear )
	$MozillaFirefoxProxyregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Proxy"
	if (Test-Path $MozillaFirefoxProxyregKey) { $null = Remove-Item -Path $MozillaFirefoxProxyregKey }
	$null = New-Item -path $MozillaFirefoxProxyregKey -ItemType RegistryKey -Force
	# lock settings in proxy
	$null = Set-ItemProperty -path $MozillaFirefoxProxyregKey -name "Locked" -value 1 -force
	if ($DebugPreference -eq 'Continue')
	{
		log2 "[OPTIONS SET] Firefox config initialised" "success"
	}
}

# Change firefox proxy type (1=manual, 2=proxy autoconfig, 4=autodetect, 5=system proxy, 0=none)
Function setFirefoxProxyType ([string]$proxyType)
{
	$MozillaFirefoxProxyregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Proxy"
	switch ($proxyType)
	{
		# no proxy
		"0" {
			setRegProperty $MozillaFirefoxProxyregKey "Mode" String "none"
			log2 "[OPTION SET] Firefox Proxy type : NONE" "success"
			break
		}
		# manual
		"1" {
			setRegProperty $MozillaFirefoxProxyregKey "Mode" String "manual"
			log2 "[OPTION SET] Firefox Proxy type : MANUAL" "success"
			break
		}
		# auto config with a URL/script
		"2" {
			setRegProperty $MozillaFirefoxProxyregKey "Mode" String "autoConfig"
			log2 "[OPTION SET] Firefox Proxy type : AUTOCONFIG" "success"
			break
		}
		# auto detect
		"4" {
			setRegProperty $MozillaFirefoxProxyregKey "Mode" String "autoDetect"
			log2 "[OPTION SET] Firefox Proxy type : AUTODETECT" "success"
			break
		}
		# system
		"5" {
			setRegProperty $MozillaFirefoxProxyregKey "Mode" String "system"
			log2 "[OPTION SET] Firefox Proxy type : SYSTEM" "success"
			break
		}
		default {
			break
		}
	}
}

# Change firefox proxy url
Function setFirefoxProxyUrl ([string]$proxyURL)
{
	$MozillaFirefoxProxyregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Proxy"
	# use for all protocols
	setRegProperty $MozillaFirefoxProxyregKey "UseHTTPProxyForAllProtocols" DWORD 1
	# proxy url + port
	setRegProperty $MozillaFirefoxProxyregKey "HTTPProxy" String $proxyURL
	log2 "[OPTION SET] Firefox Proxy URL : $proxyURL" "success"
}

# set Firefox proxy bypass
Function setFirefoxProxyBypass ($proxyBypass)
{
	$MozillaFirefoxProxyregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Proxy"
	setRegProperty $MozillaFirefoxProxyregKey "Passthrough" String $proxyBypass
	log2 "[OPTION SET] Firefox Proxy BYPASS for : $proxyBypass" "success"
}

# set Firefox proxy script (PAC file)
Function setFirefoxProxyScript ($proxyAutoConfigURL)
{
	$MozillaFirefoxProxyregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Proxy"
	setRegProperty $MozillaFirefoxProxyregKey "AutoConfigURL" String $proxyAutoConfigURL
	log2 "[OPTION SET] Firefox Proxy AUTOCONFIG URL: $proxyAutoConfigURL" "success"
}

# Change firefox homepage
Function changeFirefoxHomepage ([string]$url)
{
	# Delete existing key PROXY (quicker to clear )
	$MozillaFirefoxHomepageregKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Homepage"
	if (Test-Path $MozillaFirefoxHomepageregKey) { $null = Remove-Item -Path $MozillaFirefoxHomepageregKey }
	$regKey = "Registry::\HKEY_USERS\$userMSID\Software\Policies\Mozilla\Firefox\Homepage"
	if ($url -ne "")
	{
		#clean url
		if (($url.substring(0, 4).ToLower()) -ne "http") { $url = "http://$url" }
		setRegProperty $regKey "URL" String $url
		log2 "[OPTION SET] Firefox HomePage : $url" "success"
	}
	else
	{
		setRegProperty $regKey "URL" String "about:blank"
		log2 "[OPTION SET] Firefox HomePage is BLANK" "success"
	}
}

# Test if registry key exists and return its value (if asked)
Function Test-RegistryValue
{
	param ([string]$RegKeyPath,
		[string]$Value,
		[boolean]$getvalue)
	
	if ($getvalue -eq $false)
	{
		$ValueExist = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).$Value -ne $null
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[REGISTRY] Registry value $Value on registry key $RegKeyPath exists" "registry"
		}
		Return $ValueExist
	}
	else
	{
		$RegValue = (Get-ItemProperty $RegKeyPath -ErrorAction SilentlyContinue -ErrorVariable Reg_not_found).$Value
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[REGISTRY] Registry value on registry key $RegKeyPath is $RegValue" "registry"
		}
		
		Return $RegValue
	}
	If ($Reg_not_found)
	{
		if ($DebugPreference -eq 'Continue')
		{
			log2 "[REGISTRY] Registry value $Value on key $RegKeyPath doesn't exist" "registry"
		}
		Return $false
	}
}

# Sends system a refresh policy message
function Send-SettingChange
{
	$HWND_BROADCAST = [IntPtr] 0xffff;
	$WM_SETTINGCHANGE = 0x1a;
	$result = [UIntPtr]::Zero
	
	[void]([Parxgui.Refresh_policy]::SendMessageTimeout($HWND_BROADCAST, $WM_SETTINGCHANGE, 0, "Policy", 0, 1000, [ref]$result))
}

# Sends system an explorer refresh message
Function Refresh-Explorer
{
	[void][Parxgui.Refresh_Explorer]::RefreshWindowsExplorer()
}